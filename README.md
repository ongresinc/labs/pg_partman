# PG_Partman Lab setup

## Settig up

To start this laboratory you only need the project cloned, so if you are reading this file you are ok.
Now, run the `setup` command to create the `python venv` and his dependencies.

```
make setup
```

## Start PostgreSQL Servers

k8s credentials should be in environment, but you are against minikube, no auth is required.
Although, you can execute `setup` rule for setting up this.

```
make deploy
```

This will create the default deploy (postgres) using `postgres` role that executes k8s API calls,
and create instances with PostgreSQL servers versions from 9.4 to 12.

## When you are done with your test, you can clean all this mess running

```
make clean
```

## Useful k8s commands

### How to list `pods`, `services`, `deployments`, `confimaps`

```
$ kubectl get configmaps --namespace=partman 
NAME                   DATA   AGE
postgres-10.1-config   7      10m
postgres-11.5-config   7      10m
postgres-9.4-config    7      10m
postgres-9.5-config    7      10m
postgres-9.6-config    7      10m
$ kubectl get pods --namespace=partman
NAME                                 READY   STATUS    RESTARTS   AGE
postgres10-1-dply-5dfcdc5586-h44jj   1/1     Running   0          10m
postgres11-5-dply-fdf76957b-plwkv    1/1     Running   0          10m
postgres9-4-dply-6ccccbbd8b-xkfb4    1/1     Running   0          10m
postgres9-5-dply-64f9597cb8-6xk84    1/1     Running   0          10m
postgres9-6-dply-6bd47fc594-nlm7m    1/1     Running   0          10m
$ kubectl get service --namespace=partman
NAME               TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
postgres10-1-svc   NodePort   10.108.21.249   <none>        5432:31880/TCP   10m
postgres11-5-svc   NodePort   10.99.115.249   <none>        5432:31156/TCP   10m
postgres9-4-svc    NodePort   10.104.88.88    <none>        5432:31935/TCP   10m
postgres9-5-svc    NodePort   10.100.85.43    <none>        5432:32132/TCP   10m
postgres9-6-svc    NodePort   10.96.241.76    <none>        5432:31428/TCP   10m
$ kubectl get deployments --namespace=partman
NAME                READY   UP-TO-DATE   AVAILABLE   AGE
postgres10-1-dply   1/1     1            1           11m
postgres11-5-dply   1/1     1            1           11m
postgres9-4-dply    1/1     1            1           11m
postgres9-5-dply    1/1     1            1           11m
postgres9-6-dply    1/1     1            1           11m
```
```
cepxio@cepxio-carbon:~/gitlab/pg_partman (ansible)$ kubectl get services --namespace=partman -o custom-columns=PORT:.spec
PORT
map[clusterIP:10.101.110.179 externalTrafficPolicy:Cluster ports:[map[name:postgres nodePort:32349 port:5432 protocol:TCP targetPort:5432]] selector:map[app:postgres10-1] sessionAffinity:None type:NodePort]
map[clusterIP:10.97.11.147 externalTrafficPolicy:Cluster ports:[map[name:postgres nodePort:30877 port:5432 protocol:TCP targetPort:5432]] selector:map[app:postgres11-5] sessionAffinity:None type:NodePort]
map[clusterIP:10.107.214.49 externalTrafficPolicy:Cluster ports:[map[name:postgres nodePort:32372 port:5432 protocol:TCP targetPort:5432]] selector:map[app:postgres9-4] sessionAffinity:None type:NodePort]
map[clusterIP:10.98.235.248 externalTrafficPolicy:Cluster ports:[map[name:postgres nodePort:32607 port:5432 protocol:TCP targetPort:5432]] selector:map[app:postgres9-5] sessionAffinity:None type:NodePort]
map[clusterIP:10.102.93.105 externalTrafficPolicy:Cluster ports:[map[name:postgres nodePort:30957 port:5432 protocol:TCP targetPort:5432]] selector:map[app:postgres9-6] sessionAffinity:None type:NodePort]
cepxio@cepxio-carbon:~/gitlab/pg_partman (ansible)$ kubectl get services --namespace=partman -o custom-columns=IP:.spec.clusterIP,PORT:.spec.ports.*
IP               PORT
10.101.110.179   map[name:postgres nodePort:32349 port:5432 protocol:TCP targetPort:5432]
10.97.11.147     map[name:postgres nodePort:30877 port:5432 protocol:TCP targetPort:5432]
10.107.214.49    map[name:postgres nodePort:32372 port:5432 protocol:TCP targetPort:5432]
10.98.235.248    map[name:postgres nodePort:32607 port:5432 protocol:TCP targetPort:5432]
10.102.93.105    map[name:postgres nodePort:30957 port:5432 protocol:TCP targetPort:5432]
cepxio@cepxio-carbon:~/gitlab/pg_partman (ansible)$ kubectl get services --namespace=partman -o custom-columns=IP:.spec.clusterIP,PORT:.spec.ports.*.nodePort
IP               PORT
10.101.110.179   32349
10.97.11.147     30877
10.107.214.49    32372
10.98.235.248    32607
10.102.93.105    30957
cepxio@cepxio-carbon:~/gitlab/pg_partman (ansible)$ 
```
### Connect to instance into k8s
```
$ kubectl exec -it postgres10-1-dply-5dfcdc5586-h44jj --namespace=partman bash
root@postgres10-1-dply-5dfcdc5586-h44jj:/# 
root@postgres10-1-dply-5dfcdc5586-h44jj:/# su - postgres
No directory, logging in with HOME=/
$ psql
psql (10.1)
Type "help" for help.

postgres=# 
postgres=# 
postgres=# 
postgres=# l
postgres-# ^C
postgres=# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
-----------+----------+----------+------------+------------+-----------------------
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
(3 rows)

postgres=# 
```

### Connect to postgres through k8s cluster

To connect to the postgresql server into k8s cluster, you have to search for Cluster IP Address with the next command:

```
$ kubectl cluster-info --namespace=partman
Kubernetes master is running at https://192.168.99.100:8443

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

> Tip: Don't forget include the namespace where are running the instances if any.

Using the cluster IP and the port mapped you can connect to:

```
$ psql -h 192.168.99.100 -p 31935 -U postgres
psql (12.1 (Ubuntu 12.1-1.pgdg18.04+1), server 9.4.23)
Type "help" for help.

postgres=# \q
```

Enjoy@

 
## The Law

How versioning in Pg changed:

https://www.postgresql.org/support/versioning/


Using them to manipule user params (yamls replacements, etc):
https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#kubernetes-filters


## Shitty dependencies

```
ERROR: k8s 0.11.0 has requirement requests==2.13.0, but you'll have requests 2.22.0 which is incompatible.
```
