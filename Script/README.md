**pg_partman use**
==========================

Table of contents
----------  
 * [Install](#id1)
 * [Config](#id2)
 * [Use cases](#id3)
    * [pg_partman from zero](#id31)
    * [pg_partman from table with data](#id31)
    
    
    
 
 
 
 
##Install <a name="id1"></a>
--------------------------
**From repo:**
Centos:
-yum install pg_partman11 (repositorio)
Ubuntu:
-apt-get install postgresql-11-partman

**From source code:**
Install PG  devel package

Centos:
-postgresql11-devel

Ubuntu:
-postgresql-server-dev-11

Download code from: https://github.com/pgpartman/pg_partman

Install:
make install

##Config <a name="id2"></a>
--------------------------
If want use the background worker config the postgresql.conf file the following variables (require restart):

shared_preload_libraries = 'pg_partman_bgw'
pg_partman_bgw.interval = 3600
pg_partman_bgw.role = 'postgres'
pg_partman_bgw.dbname = 'test_partman'

                                  
                                  
**Inside the database for partition:**

```
CREATE SCHEMA partman;
CREATE EXTENSION pg_partman SCHEMA partman;


CREATE ROLE partman WITH LOGIN;
GRANT ALL ON SCHEMA partman TO partman;
GRANT ALL ON ALL TABLES IN SCHEMA partman TO partman;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA partman TO partman;
GRANT EXECUTE ON ALL PROCEDURES IN SCHEMA partman TO partman;  -- PG11+ only
GRANT ALL ON SCHEMA my_partition_schema TO partman;
GRANT CREATE ON DATABASE test_partman TO partman;

```

## Use cases <a name="id3"></a>
--------------------------
### pg_partman from zero <a name="id31"></a>

Create partition with date/timestamp range

```
CREATE TABLE public.acct_v9 (
    tag bigint DEFAULT 0 NOT NULL,
    class_id character varying(16) DEFAULT ' '::character varying NOT NULL,
    mac_src macaddr DEFAULT '00:00:00:00:00:00'::macaddr NOT NULL,
    mac_dst macaddr DEFAULT '00:00:00:00:00:00'::macaddr NOT NULL,
    vlan integer DEFAULT 0 NOT NULL,
    as_src bigint DEFAULT 0 NOT NULL,
    as_dst bigint DEFAULT 0 NOT NULL,
    ip_src inet DEFAULT '0.0.0.0'::inet NOT NULL,
    ip_dst inet DEFAULT '0.0.0.0'::inet NOT NULL,
    port_src integer DEFAULT 0 NOT NULL,
    port_dst integer DEFAULT 0 NOT NULL,
    tcp_flags smallint DEFAULT 0 NOT NULL,
    ip_proto smallint DEFAULT 0 NOT NULL,
    tos integer DEFAULT 0 NOT NULL,
    packets integer NOT NULL,
    bytes bigint NOT NULL,
    flows integer DEFAULT 0 NOT NULL,
    stamp_inserted timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    stamp_updated timestamp without time zone
)partition by range (stamp_inserted) ;



BEGIN;


SELECT partman.create_parent('public.acct_v9', 'stamp_inserted', 'native', 'daily'); --create partition childs

--_pYYYY                - Yearly (and any custom time greater than this)
--_pYYYY"q"Q            - Quarterly (double quotes required to add another string value inside a date/time format string)
--_pYYYY_MM             - Monthly (and all custom time intervals between yearly and monthly)
--_pIYYY"w"IW           - Weekly (ISO Year and ISO Week)
--_pYYYY_MM_DD          - Daily (and all custom time intervals between monthly and daily)
--_pYYYY_MM_DD_HH24MI   - Hourly, Half-Hourly, Quarter-Hourly (and all custom time intervals between daily and hourly)
--_pYYYY_MM_DD_HH24MISS - Only used with custom time if interval is less than 1 minute (cannot be less than 1 second)
--_p#####               - Serial/ID partition has a suffix that is the value of the lowest possible entry in that table (Ex: _p10, _p20000, etc)
COMMIT;

select * from partman.show_partitions('public.acct_v9'); --get childs tables
select * from partman.show_partitions(p_parent_table:='public.acct_v9',p_include_default:=true);  --get childs tables include default if PG 11+


--inserting data

INSERT INTO public.acct_v9( tag, class_id,  packets,   bytes, flows, stamp_inserted, stamp_updated)
   select (random()*10)::int, md5(random()::character varying(16))::character varying(16) ,(random()*1000)::int,(random()*100000)::int,(random()*10)::int, dd,dd
FROM generate_series  ( '2019-12-09'::timestamp without time zone  '2019-12-20'::timestamp  without time zone , '1 hour'::interval) dd 


 EXPLAIN ANALYZE select * from public.acct_v9 --checking plan use partition 
 
 
 
 --maintenance(create new childs and delete old tables (retention) if is configured)
 
 select partman.run_maintenance();
 
-- Retention, can configurate in part_config table from pg_partman


create schema retencion ;

UPDATE partman.part_config SET retention = '3 days', retention_schema = 'retencion' WHERE parent_table = 'public.acct_v9'; --move tables with more than 3 days to retencion schema

select partman.run_maintenance();
select * from partman.show_partitions('public.acct_v9');


select * from pg_tables where schemaname ='retencion';
```

Create partition with serial/int/bigint range

```

create table id_master (i int primary key, j text) 
partition by range (i);

SELECT partman.create_parent('public.id_master', 'i', 'native', '1000' ,p_premake:=2); --premake = child tables future to create default is 4



select * from partman.show_partitions('public.id_master');

---sub partition 
SELECT partman.create_sub_parent('public.id_master', 'i', 'native', '200', p_native_check:='yes'); --must check if the table have data, because the data will delete




select * from partman.show_partitions('public.id_master');
select * from partman.show_partitions('public.id_master_p0');

--insert data
insert into id_master 
select i, md5(i::text) from generate_series (1,1000) as  i;




EXPLAIN ANALYZE select * from id_master  ; --checking plan use partition 

--maintenance
select partman.run_maintenance();

--insert more data
insert into id_master 
select i, md5(i::text) from generate_series (1001,1900) as  i;


--config retention
UPDATE partman.part_config SET retention = '500', retention_schema = 'retencion' WHERE parent_table = 'public.id_master'; --move tables with more than 300 old


select partman.run_maintenance();

--check the child tables
select * from partman.show_partitions('public.id_master');    
select * from partman.show_partitions('public.id_master_p1000');
select * from partman.show_partitions('public.id_master_p2000');


```


### pg_partman from table with data <a name="id32"></a>

This example use the script from dell database: [db dell script](Script/structure_dellstore2-normal-1.0_schema_dell.sql "db dell script")
 and function [trigger_to_declarative_range](Script/function_trigger_to_declarative_range.sql "trigger_to_declarative_range") , please load these script before execute following SQL commands

```


BEGIN;
--update data more recent
update dell.orderlines set orderdate=orderdate+'15 year'::interval;

SELECT partman.create_parent(p_parent_table:='dell.orderlines', p_control:='orderdate', p_type:='partman', p_interval:='monthly',p_start_partition:='2018-12-01');






select * from partman.show_partitions('dell.orderlines');
EXPLAIN ANALYZE select * from dell.orderlines; --check the plan use the partition  


select partman.partition_data_time('dell.orderlines'); --move data to child tables, it must be run as many times each child partitions, 17 in this case
EXPLAIN ANALYZE select * from dell.orderlines;--check the plan use the partition  , and data in child tables

delete from partman.part_config where parent_table='dell.orderlines'; --destroy partman metadata


select public.trigger_to_declarative_range ('dell','orderlines','orderdate','%_partition_check%','date'); --convert to  declarative partition

SELECT partman.create_parent('dell.orderlines', 'orderdate', 'native', 'monthly'); --create partman metadata and structure for declarative partition 
select * from partman.show_partitions('dell.orderlines'); 
EXPLAIN ANALYZE select * from dell.orderlines; --check the plan use the partition  
COMMIT;

```
