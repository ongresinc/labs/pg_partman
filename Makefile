SHELL=/bin/bash
py3_path=$(shell which python3)
VENV_ON=source venv/bin/activate
REPONAME=$(shell basename $$PWD)
MACRO_ANS_INTER=REPONAME=$(REPONAME) ansible_python_interpreter=$(py3_path) 
DEFAULT_APP=postgres
DEFAULT_VERSION=11.4
DEFAULT_DB=test
API_VERSION=bench-operator.3manuek.com/v1alpha1


ifndef VERSION
	VERSION=$(DEFAULT_VERSION)
endif

ifndef APP
	APP=$(DEFAULT_APP)
endif

ifdef DEBUG_ANS
	ansible_debug=-vvv
endif

ifdef DEBUG
	DEBUG=true
endif

ifndef DEBUG
	DEBUG=false
endif

ifndef DB
	DB=$(DEFAULT_DB)
endif

.PHONY: setup venv-install clean-% deploy-%

venv-install:
	python3 -m venv venv

setup: venv-install
	$(VENV_ON) && python3 -m pip install --upgrade pip && $(py3_path) -m pip install -r requirements.txt

tag:
	git tag -a v$(TAG) -m 'v$(TAG)' &&\
	git push origin --tags

k8s: 
	$(VENV_ON) &&\
	cd ansible && ANSIBLE_CONFIG="$(APP).cfg" ansible-playbook $(ansible_debug) \
		-i inventories/$(APP) --extra-vars "DEBUG=$(DEBUG) ansible_state=present $(MACRO_ANS_INTER) app=$(APP) version=$(VERSION)" \
		$(APP).yml && \
	 cd .. && deactivate

deploy: k8s 
	$(VENV_ON) &&\
	cd ansible && ANSIBLE_CONFIG="$(APP).cfg" ansible-playbook $(ansible_debug) \
		-e "pg_db_create=$(DB)" \
		partman_partition.yml &&\
	 cd .. && deactivate

clean: 
	$(VENV_ON) &&\
	cd ansible && ANSIBLE_CONFIG="$(APP).cfg" ansible-playbook $(ansible_debug) \
		-i inventories/$(APP) --extra-vars "DEBUG=$(DEBUG) ansible_state=absent $(MACRO_ANS_INTER) app=$(APP) version=$(VERSION)" \
		$(APP).yml &&\
	cd .. && deactivate
