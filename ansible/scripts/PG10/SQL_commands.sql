
--create partition table in trigger partition
BEGIN;
SELECT _pgpartman.create_parent('public.crypto_trades_completed', 'created_at', 'partman', 'monthly');
COMMIT;

--move row from main to child tables, must be executed for each child who has the main table
SELECT _pgpartman.partition_data_time('public.crypto_trades_completed')

--show metadata from partition configuration
SELECT * from _pgpartman.show_partitions('public.crypto_trades_completed');
SELECT * from _pgpartman.part_config;

--delete partition configuration
DELETE from _pgpartman.part_config where parent_table='public.crypto_trades_completed';

--migrate from trigger to declarative partition
SELECT public.trigger_to_declarative_range ('public','crypto_trades_completed','created_at','%_partition_check%','date');
--create partition table in declarative partition
SELECT _pgpartman.create_parent('public.crypto_trades_completed', 'created_at', 'native', 'monthly');

--show metadata from partition configuration
SELECT * from _pgpartman.show_partitions('public.crypto_trades_completed');



--to create index al constraint in new tables after maintenace execute 

--for indexes
DO
$$
DECLARE
t text;
men text;
men_det text;
sqlerror text;
BEGIN 
for t in 
select replace ( replace (b.def,'crypto_trades_completed_old_convert',partition_tablename),'CREATE INDEX ', 'CREATE INDEX '||substring (partition_tablename from position ('_p201' in partition_tablename) for length(partition_tablename)) )from _pgpartman.show_partitions('public.crypto_trades_completed'),
(

SELECT 
   
    indexdef||';' as def 
FROM
    pg_indexes
WHERE
    schemaname = 'public' and tablename='crypto_trades_completed_old_convert' and indexdef not like 'CREATE UNIQUE INDEX %' ) as b
where partition_tablename not in (
SELECT 
 distinct 
   tablename
FROM
    pg_indexes
WHERE
    schemaname = 'public' and tablename like 'crypto_trades_completed'||'_p%' )
    
LOOP
 BEGIN
 --raise notice 't:%',t;
  EXECUTE t;
  
  EXCEPTION
  WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS men = message_text, men_det =pg_exception_detail, sqlerror = returned_sqlstate;
  RAISE NOTICE 'Error in : %, %, %', sqlerror, men,men_det;
 END; 
END LOOP;
END;
$$


--for constraint
DO
$$
DECLARE
t text;
men text;
men_det text;
sqlerror text;
BEGIN 
for t in 
select 'alter table '|| partition_tablename ||' add  '||b.def from _pgpartman.show_partitions('public.crypto_trades_completed'),
(
SELECT 
       pg_get_constraintdef(c.oid,true)||';' as def 
FROM   pg_constraint c
JOIN   pg_namespace n ON n.oid = c.connamespace
WHERE  contype IN ('f', 'p ')
and (conrelid::regclass)::text ='crypto_trades_completed_old_convert' ) as b
where partition_tablename not in (
    SELECT  distinct (conrelid::regclass)::text
FROM   pg_constraint c
JOIN   pg_namespace n ON n.oid = c.connamespace
WHERE  contype IN ('f', 'p ')
and (conrelid::regclass)::text like 'crypto_trades_completed'||'_p%' 
) 
LOOP
 BEGIN
  EXECUTE t;
  EXCEPTION
  WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS men = message_text, men_det =pg_exception_detail, sqlerror = returned_sqlstate;
  RAISE NOTICE 'Error in : %, %, %', sqlerror, men,men_det;
 END; 
END LOOP;
END;
$$


