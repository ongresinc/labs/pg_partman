The following steps describe how to partition the public.crypto_trades_completed table into the paxful database, reproducible to test with PG 11+


*   Create database  px

*   Load partman script modified for RDS from file  [pg_partman--4.2.2_rds_pg10.sql](scripts/pg_partman--4.2.2_rds_pg10.sql)  into px dabatase 

*   Load  paxful structure into px dabatase script  from file [structure_px.sql](scripts/structure_px.sql) into px dabatase 

*   Load function to migrate partition format [trigger_to_declarative_range.sql](scripts/trigger_to_declarative_range.sql) into px dabatase 


*   Run the SQL command in file [SQL_commands.sql](scripts/SQL_commands.sql) into px dabatase 