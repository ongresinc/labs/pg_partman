CREATE OR REPLACE FUNCTION public.trigger_to_declarative_range(sch text, ta text, col text, cons_pat text DEFAULT '%%'::text, ptype text DEFAULT 'num'::text)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
    DECLARE
    --ptype num, date
    tab text := $1||'.'||$2;
    qtable_new text;
    table_child record;
    chk record;
    qtable_child_rm text;
    qtable_child_attach text;
    qtable_temp text;
    qtable_rm_const text;
    qtable_default text;
    qtable_miss text;
    range_value text;
    lastday date;
    chk_tmp text;
    chk_left text;
    chk_rigth text;
    clean_tmp text;
    clean_left text;
    clean_rigth text;
    clean_value text;
    range_left text; 
    range_rigth text;
    i int; 
    men text;
    men_det text;
    sqlerror text;
    _tablename text;
    BEGIN
    --the new table
    qtable_new := 'CREATE TABLE '||tab||'_2_convert ( like '||tab||' including constraints) PARTITION BY RANGE ('||$3||')';
    raise notice 'creating new table';
    EXECUTE qtable_new;
    --removing 
    raise notice 'remove/add child-tables';
    drop table if exists _t_name;
    create temp  TABLE _t_name (name_sch text, name_table date);
    FOR table_child in   SELECT   es_p.nspname tab_sch,    h.relname  AS Tab_son
                    FROM pg_inherits i
		    JOIN pg_class p        ON i.inhparent = p.oid
		    JOIN pg_class h         ON i.inhrelid   = h.oid
		    JOIN pg_namespace es_p   ON es_p.oid  = p.relnamespace
		    JOIN pg_namespace es_h    ON es_h.oid   = h.relnamespace
		    where es_p.nspname ||'.'||p.relname=tab
		    LOOP
		    
                    --rm
		    qtable_child_rm := 'ALTER TABLE '||table_child.tab_sch||'.'||table_child.Tab_son||' NO INHERIT '||tab;
		    raise notice 'remove child: %',table_child.tab_sch||'.'||table_child.Tab_son;
		    raise notice '%',qtable_child_rm;
		    EXECUTE qtable_child_rm;
                    --attach
                    ---range logic
                     select conname, pg_get_constraintdef(pg_constraint.oid,true) consrc  into chk  from pg_constraint join pg_class on  (pg_constraint.conrelid=pg_class.oid) 
                      join pg_namespace on (pg_namespace.oid=pg_class.relnamespace)
                         where contype='c' and conname like $4 and  pg_namespace.nspname  ||'.'||pg_class.relname=table_child.tab_sch||'.'||table_child.Tab_son;
                         IF chk.consrc ilike '%between %' THEN
                         continue ;
                         ELSIF chk.consrc ilike '% and %' THEN
                          chk_tmp:= trim( replace (chk.consrc,'CHECK ',''));
                          chk_left:=substring(chk_tmp from 2 for position ('and' in lower(chk_tmp))-3 );
                          chk_rigth:=trim(substring(chk_tmp from position ('and' in lower(chk_tmp))+4 for length (chk_tmp)));
                          chk_rigth:=substring(chk_rigth from 1 for length(chk_rigth)-1);
                         raise notice 'ORGINAL: chk_left : %, chk_rigth : %',chk_left,chk_rigth;
                         --get clean value 
                         clean_left := trim(replace(replace (replace( replace (chk_left,$3||'',''),'>',''),'<',''),'=',''));
                         clean_rigth := trim(replace(replace (replace( replace (chk_rigth,$3||'',''),'>',''),'<',''),'=',''));
                           --interchange values if is necessary
                           IF clean_left>clean_rigth THEN
                            clean_tmp:=clean_left;
                            chk_tmp:=chk_left;
                            clean_left:=clean_rigth;
                            chk_left:=chk_rigth;
                            clean_rigth:=clean_tmp;
                            chk_rigth:=chk_tmp;
                           END IF;    
                         ELSE
                           raise notice 'else: %',chk.consrc;
                           RAISE EXCEPTION 'not support MAXVALUE or MINVALUE YET';
                           
                              
                         END IF;
                         --for type on rangue partition
                         --NUMERIC
                         IF $5='num' THEN
                           IF position('=' in chk_left) =0 THEN
                             range_left :=(clean_left::numeric +1)::text;
                           ELSE
                            range_left :=clean_left;  
                           END IF;
                           IF position('=' in chk_rigth) <>0 THEN
                             range_rigth :=(clean_rigth::numeric +1)::text;
                           ELSE
                              range_rigth :=clean_rigth;
                           END IF;
                           qtable_child_attach := 'ALTER TABLE '||tab||'_2_convert'|| ' ATTACH PARTITION '||table_child.tab_sch||'.'||table_child.Tab_son||' FOR VALUES FROM ('||range_left||') TO ('||range_rigth||')';
                           raise notice 'qtable_child_attach: %',qtable_child_attach;
                           EXECUTE qtable_child_attach;
                         END IF;


                         --date
                         IF $5='date' THEN
                           IF position('=' in chk_left) =0 THEN
                             range_left :=(clean_left::date +1)::text;
                           ELSE
                            range_left :=clean_left;  
                           END IF;
                           IF position('=' in chk_rigth) <>0 THEN
                             range_rigth :=(clean_rigth::date +1)::text;
                           ELSE
                              range_rigth :=clean_rigth;
                           END IF;
                           qtable_child_attach := 'ALTER TABLE '||tab||'_2_convert'|| ' ATTACH PARTITION '||table_child.tab_sch||'.'||table_child.Tab_son||' FOR VALUES FROM ('||substring(range_left from 1 for position('::' in range_left)-1)||') TO ('||substring(range_rigth from 1 for position('::' in range_rigth)-1)||')';
                           raise notice 'qtable_child_attach: %',qtable_child_attach;
                           EXECUTE qtable_child_attach;
                         END IF;
                         
                         
                  
                    
		    --rm child constraint
		    qtable_rm_const:= ' ALTER TABLE ' || table_child.tab_sch||'.'||table_child.Tab_son|| ' DROP CONSTRAINT  '|| (    select conname::text from pg_constraint  where conrelid = (
							select pg_class.oid  from pg_class join pg_namespace on (pg_class.relnamespace=pg_namespace.oid) 
							where relname=table_child.Tab_son and pg_namespace.nspname=table_child.tab_sch) and conname like cons_pat) ;
		     raise notice '%',qtable_rm_const;
		    EXECUTE qtable_rm_const;



    END LOOP;
    IF  current_setting('server_version_num')::int >=110000 THEN

     raise notice 'add default partition';
     qtable_default:= 'CREATE TABLE '||$1||'.'||$2|| '_default PARTITION OF '||tab||'_2_convert DEFAULT';
     EXECUTE qtable_default;

    END IF;
   
    raise notice 'change tables names';

    --change tables name
    qtable_temp:= 'ALTER TABLE '||tab||' RENAME TO '||$2||'_old_convert';
    EXECUTE qtable_temp;
    qtable_temp:= 'ALTER TABLE '||tab||'_2_convert RENAME TO '||$2;
    EXECUTE qtable_temp;

    EXCEPTION 
     WHEN OTHERS THEN
	GET STACKED DIAGNOSTICS men = message_text, men_det =pg_exception_detail, sqlerror = returned_sqlstate;
	RAISE EXCEPTION 'Otro error: %, %, %', sqlerror, men,men_det;
		    

    END;
    $function$
;

