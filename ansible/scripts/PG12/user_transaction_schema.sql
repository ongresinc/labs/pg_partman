create table public.merchants(id integer);
alter table public.merchants add primary key (id);
--data for merchants
insert into public.merchants values (1),(2),(3),(4);



CREATE TABLE public.user_transactions (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    type character varying(50) NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    crypto_amount bigint NOT NULL,
    crypto_add_to_balance bigint NOT NULL,
    new_crypto_balance bigint,
    sent_to_address character varying(100),
    transaction_hash character varying(255),
    confirms integer DEFAULT 0 NOT NULL,
    note text,
    crypto_current_rate_usd numeric(25,2) NOT NULL,
    host character varying(50),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fiat_value numeric(25,2),
    trade_id bigint,
    merchant_id integer,
    crypto_amount_no_fee bigint,
    source character varying(64),
    priority character varying(64),
    autosend_type character varying(64),
    "position" smallint DEFAULT 0,
    linked_user_transaction_id bigint,
    crypto_current_rate_fiat numeric(25,2),
    new_crypto_balance_old bigint,
    user_note character varying(255),
    merchant_name character varying(64),
    track_id character varying(64)
);

CREATE SEQUENCE public.user_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ONLY public.user_transactions ALTER COLUMN id SET DEFAULT nextval('public.user_transactions_id_seq'::regclass);

--constaints
ALTER TABLE ONLY public.user_transactions  ADD CONSTRAINT user_transactions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.user_transactions  ADD CONSTRAINT user_transactions_merchant_id_foreign FOREIGN KEY (merchant_id) REFERENCES public.merchants(id);

--indexes
CREATE INDEX user_transactions_jj2_idx ON public.user_transactions USING btree (trade_id, user_account_id);
CREATE INDEX user_transactions_jj_idx ON public.user_transactions USING btree (user_account_id, trade_id);


create table otra (id integer, u_tras bigint  );
    ALTER TABLE ONLY public.otra
    ADD CONSTRAINT user_transactions_id_foreign FOREIGN KEY (u_tras) REFERENCES public.user_transactions(id);


--data for user_transactions
insert into user_transactions (
user_account_id ,
    type ,
    crypto_currency_id ,
    crypto_amount ,
    
    crypto_add_to_balance ,
    new_crypto_balance ,
    sent_to_address ,
    transaction_hash ,
    
    confirms  ,
    crypto_current_rate_usd,
    created_at ,
    updated_at ,
    merchant_id
     )
    select (random()*1000)::int, md5(random()::character varying(50))::character varying(50) ,(random()*10)::int,(random()*100000)::int,
    (random()*1000)::int, (random()*1000)::int,md5(random()::character varying(16))::character varying(100),md5(random()::character varying(16))::character varying(255),
    (random()*10)::int, (random()*1000)::numeric(25,2),
     dd,dd+'1 day'::interval, (random()*3)::int+1

FROM generate_series
        ( '2019-11-09'::timestamp without time zone
        , '2019-12-20'::timestamp  without time zone
        , '2 hour'::interval) dd ;


--data for otra
insert into  public.otra  select 1, id  from public.user_transactions limit 1;
