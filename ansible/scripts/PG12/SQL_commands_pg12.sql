EXPLAIN ANALYZE SELECT * FROM public.user_transactions; 
ALTER TABLE public.otra DROP CONSTRAINT user_transactions_id_foreign;

BEGIN;

SELECT _pgpartman.create_parent('public.user_transactions', 'user_account_id', 'partman', '200'); 
SELECT * FROM _pgpartman.show_partitions('public.user_transactions');

SELECT _pgpartman.partition_data_id('public.user_transactions');
SELECT _pgpartman.partition_data_id('public.user_transactions');
SELECT _pgpartman.partition_data_id('public.user_transactions');
SELECT _pgpartman.partition_data_id('public.user_transactions');
SELECT _pgpartman.partition_data_id('public.user_transactions');
SELECT _pgpartman.partition_data_id('public.user_transactions');
SELECT _pgpartman.partition_data_id('public.user_transactions');
SELECT _pgpartman.partition_data_id('public.user_transactions');

EXPLAIN ANALYZE SELECT * FROM public.user_transactions;
DELETE FROM _pgpartman.part_config WHERE parent_table='public.user_transactions'; 
SELECT public.trigger_to_declarative_range ('public','user_transactions','user_account_id','%_partition_check%','num'); 
SELECT _pgpartman.create_parent('public.user_transactions', 'user_account_id', 'native', '200'); 

COMMIT;