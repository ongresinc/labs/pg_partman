
--create partition table in trigger partition
BEGIN;
SELECT _pgpartman.create_parent('public.crypto_trades_completed', 'created_at', 'partman', 'monthly');
COMMIT;

--move row from main to child tables, must be executed for each child who has the main table
SELECT _pgpartman.partition_data_time('public.crypto_trades_completed')

--show metadata from partition configuration
SELECT * from _pgpartman.show_partitions('public.crypto_trades_completed');
SELECT * from _pgpartman.part_config;

--delete partition configuration
DELETE from _pgpartman.part_config where parent_table='public.crypto_trades_completed';

--migrate from trigger to declarative partition
SELECT public.trigger_to_declarative_range ('public','crypto_trades_completed','created_at','%_partition_check%','date');
--create partition table in declarative partition
SELECT _pgpartman.create_parent('public.crypto_trades_completed', 'created_at', 'native', 'monthly');

--show metadata from partition configuration
SELECT * from _pgpartman.show_partitions('public.crypto_trades_completed');


--get index definition for the futures tables 
SELECT 
   
   replace ( indexdef,'_old_convert ',' ')||';'
FROM
    pg_indexes
WHERE
    schemaname = 'public' and tablename='crypto_trades_completed_old_convert';

--CREATE UNIQUE INDEX crypto_trades_completed_pkey ON public.crypto_trades_completed USING btree (id);
--CREATE INDEX crypto_trades_completed_affiliate_id_index ON public.crypto_trades_completed USING btree (affiliate_id);
--CREATE INDEX crypto_trades_completed_buyer_id_index ON public.crypto_trades_completed USING btree (buyer_id);
--CREATE INDEX crypto_trades_completed_completed_at_index ON public.crypto_trades_completed USING btree (completed_at);
--CREATE INDEX crypto_trades_completed_created_at_index ON public.crypto_trades_completed USING btree (created_at);
--CREATE INDEX crypto_trades_completed_created_at_seller_id_idx ON public.crypto_trades_completed USING btree (created_at, seller_id);
--CREATE INDEX crypto_trades_completed_crypto_trade_status_id_index ON public.crypto_trades_completed USING btree (crypto_trade_status_id);
--CREATE INDEX crypto_trades_completed_fiat_currency_id_index ON public.crypto_trades_completed USING btree (fiat_currency_id);
--CREATE INDEX crypto_trades_completed_id_created_at_completed_at_idx ON public.crypto_trades_completed USING btree (id, created_at, completed_at);
--CREATE INDEX crypto_trades_completed_invoice_uuid_index ON public.crypto_trades_completed USING btree (invoice_uuid) WHERE (invoice_uuid IS NOT NULL);
--CREATE INDEX crypto_trades_completed_jj_idx ON public.crypto_trades_completed USING btree (payment_method_id, seller_id);
--CREATE INDEX crypto_trades_completed_offer_id_index ON public.crypto_trades_completed USING btree (offer_id);
--CREATE INDEX crypto_trades_completed_offer_owner_id_index ON public.crypto_trades_completed USING btree (offer_owner_id);
--CREATE INDEX crypto_trades_completed_offer_responder_id_index ON public.crypto_trades_completed USING btree (offer_responder_id);
--CREATE INDEX crypto_trades_completed_offer_type_index ON public.crypto_trades_completed USING btree (offer_type);
--CREATE INDEX crypto_trades_completed_payment_method_id_idx ON public.crypto_trades_completed USING btree (payment_method_id);
--CREATE INDEX crypto_trades_completed_seller_id_index ON public.crypto_trades_completed USING btree (seller_id);
--CREATE INDEX crypto_trades_completed_seller_payment_method_idx ON public.crypto_trades_completed USING btree (seller_id, payment_method_id);
--CREATE INDEX crypto_trades_completed_source_id_index ON public.crypto_trades_completed USING btree (source_id);
--CREATE INDEX crypto_trades_completed_updated_at_index ON public.crypto_trades_completed USING btree (updated_at);
--CREATE INDEX idx_tree_columns ON public.crypto_trades_completed USING btree (created_at, offer_type, crypto_trade_status_id);


--get constraint definition for the futures tables 
    SELECT 
      'alter table public.crypto_trades_completed add  '|| pg_get_constraintdef(c.oid,true)||';'
FROM   pg_constraint c
JOIN   pg_namespace n ON n.oid = c.connamespace
WHERE  contype IN ('f', 'p ')
and (conrelid::regclass)::text ='crypto_trades_completed_old_convert' 
ORDER  BY conrelid::regclass::text, contype DESC;

--alter table public.crypto_trades_completed add  PRIMARY KEY (id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (affiliate_id) REFERENCES affiliates(id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (buyer_id) REFERENCES user_accounts(id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (offer_id) REFERENCES offers(id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (offer_owner_id) REFERENCES user_accounts(id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (offer_responder_id) REFERENCES user_accounts(id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (owner_country_id_logged_in) REFERENCES countries(id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (payment_method_id) REFERENCES payment_methods(id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (responder_country_id_logged_in) REFERENCES countries(id);
--alter table public.crypto_trades_completed add  FOREIGN KEY (seller_id) REFERENCES user_accounts(id);



