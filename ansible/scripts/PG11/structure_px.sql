--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: okmeter; Type: SCHEMA; Schema: -; Owner: webadmin
--

CREATE SCHEMA okmeter;


ALTER SCHEMA okmeter OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';





--
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA public;


--
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_stat_statements IS 'track execution statistics of all SQL statements executed';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: pg_stats(text); Type: FUNCTION; Schema: okmeter; Owner: webadmin
--

CREATE FUNCTION okmeter.pg_stats(text) RETURNS SETOF record
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE r record;
BEGIN
    FOR r IN EXECUTE 'SELECT r FROM pg_' || $1 || ' r' LOOP RETURN NEXT r;  -- To get pg_settings, pg_stat_activity etc.
    END loop;
    RETURN;
END
$_$;


ALTER FUNCTION okmeter.pg_stats(text) OWNER TO postgres;

--
-- Name: get_location(inet); Type: FUNCTION; Schema: public; Owner: webadmin
--

CREATE FUNCTION public.get_location(inet) RETURNS integer
    LANGUAGE sql STABLE
    AS $_$
  SELECT id FROM blocks
  WHERE ip_range >>= ip4($1)
$_$;


ALTER FUNCTION public.get_location(inet) OWNER TO postgres;

--
-- Name: similarity_v2(text, text); Type: FUNCTION; Schema: public; Owner: webadmin
--

CREATE FUNCTION public.similarity_v2(hash_1 text, hash_2 text) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$
    SELECT 
        1.0 - (LENGTH(REPLACE(
                        (('x' || hash_1)::bit(64) # ('x' || hash_2)::bit(64))::text,'0', '')
                    ) / 64.0)::double precision;
$$;


ALTER FUNCTION public.similarity_v2(hash_1 text, hash_2 text) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: user_accounts; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_accounts (
    id bigint NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(64) NOT NULL,
    remember_token character varying(100),
    ip_address character varying(60) NOT NULL,
    email_active boolean DEFAULT false NOT NULL,
    phone_active_deprecated boolean DEFAULT false NOT NULL,
    suspend boolean DEFAULT false NOT NULL,
    country_id integer DEFAULT 227 NOT NULL,
    active_fiat_currency_id_deprecated integer DEFAULT 144 NOT NULL,
    activation_token character varying(40),
    login_times integer DEFAULT 0 NOT NULL,
    date_last_login timestamp without time zone DEFAULT '2001-09-28 01:00:00'::timestamp without time zone NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    password_changed boolean DEFAULT false NOT NULL,
    timezone character varying(255) DEFAULT 'America/New_York'::character varying NOT NULL,
    last_transaction bigint DEFAULT (0)::bigint NOT NULL,
    referral_code character varying(255),
    referred_by_user_id_deprecated bigint,
    email_normalized character varying(255),
    marketplace_can_post boolean DEFAULT false NOT NULL,
    username character varying(100),
    avatar_url character varying(255) DEFAULT '/2/images/avatar.png'::character varying NOT NULL,
    free_trades integer DEFAULT 0 NOT NULL,
    sound_notifications boolean DEFAULT true NOT NULL,
    frozen boolean DEFAULT false NOT NULL,
    notify_not_email_messages boolean DEFAULT false NOT NULL,
    banned boolean DEFAULT false NOT NULL,
    entry_point character varying(40),
    is_affiliate boolean DEFAULT false NOT NULL,
    compromised boolean DEFAULT false NOT NULL,
    is_merchant boolean DEFAULT false NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    is_vendor boolean,
    dashboard character varying(30),
    entry_point_related_object_type character varying(255),
    entry_point_related_object_id integer,
    last_seen timestamp without time zone,
    privacy_policy_confirmed boolean,
    is_deleted boolean,
    locale character varying(12),
    country_assign_reason integer,
    id_verification_required boolean,
    id_verification_required_admin_id bigint,
    id_verification_required_requested_at timestamp(0) without time zone,
    document_verification_required boolean,
    document_verification_required_admin_id bigint,
    document_verification_required_requested_at timestamp(0) without time zone,
    id_verification_required_trading_volume integer,
    free_internal_sendout boolean DEFAULT false NOT NULL,
    oauth_provider character varying(32),
    tainted boolean DEFAULT false
);


ALTER TABLE public.user_accounts OWNER TO postgres;

--
-- Name: user_crypto_balances; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_crypto_balances (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    balance bigint DEFAULT (0)::bigint NOT NULL,
    total_received bigint DEFAULT (0)::bigint NOT NULL,
    num_transactions integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    total_bonus_amount bigint DEFAULT (0)::bigint NOT NULL,
    incoming_amount bigint DEFAULT (0)::bigint NOT NULL,
    balance_escrow bigint DEFAULT (0)::bigint NOT NULL,
    bond integer DEFAULT 0 NOT NULL,
    CONSTRAINT amounts_positive CHECK (((incoming_amount >= 0) AND (balance_escrow >= 0))),
    CONSTRAINT balance_positive_minus_network_fee CHECK ((balance >= '-100000'::integer)),
    CONSTRAINT bond_positive CHECK ((bond >= 0))
);


ALTER TABLE public.user_crypto_balances OWNER TO postgres;

--
-- Name: user_stats; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_stats (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    feedback_positive integer DEFAULT 0 NOT NULL,
    feedback_negative integer DEFAULT 0 NOT NULL,
    feedback_total integer DEFAULT 0 NOT NULL,
    total_partners integer DEFAULT 0 NOT NULL,
    total_btc numeric(25,8) DEFAULT 0.00000000 NOT NULL,
    total_trades integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    feedback_neutral integer DEFAULT 0 NOT NULL,
    trusted_count integer DEFAULT 0 NOT NULL,
    blocked_count integer DEFAULT 0 NOT NULL,
    buy_trade_started_count integer,
    sell_trade_started_count integer,
    trade_bought_volume_usd numeric(50,2),
    trade_sold_volume_usd numeric(50,2),
    wallet_sendout_external_volume_usd numeric(50,2),
    wallet_sendout_internal_volume_usd numeric(50,2),
    wallet_receive_external_volume_usd numeric(50,2),
    wallet_receive_internal_volume_usd numeric(50,2)
);


ALTER TABLE public.user_stats OWNER TO postgres;

--
-- Name: active_traders; Type: VIEW; Schema: public; Owner: webadmin
--

CREATE VIEW public.active_traders AS
 SELECT ua.id,
    ua.email,
    ua.username,
    ((cb.balance)::numeric / (100000000)::numeric) AS btc_balance,
    ((cb.total_received)::numeric / (100000000)::numeric) AS btc_total_received,
    cb.num_transactions,
    cb.incoming_amount,
    cb.balance_escrow,
    us.total_btc AS btc_trade_volume,
    us.total_trades,
    us.feedback_total,
    us.feedback_positive,
    us.feedback_negative,
    us.feedback_neutral
   FROM ((public.user_crypto_balances cb
     LEFT JOIN public.user_accounts ua ON ((ua.id = cb.user_account_id)))
     LEFT JOIN public.user_stats us ON ((ua.id = us.user_account_id)))
  WHERE ((cb.balance > 0) OR (cb.num_transactions > 0))
  ORDER BY cb.balance DESC;


ALTER TABLE public.active_traders OWNER TO postgres;

--
-- Name: addresses_promo_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.addresses_promo_id_seq
    START WITH 1851
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.addresses_promo_id_seq OWNER TO postgres;

--
-- Name: admin_logs; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.admin_logs (
    id integer NOT NULL,
    action character varying(255) NOT NULL,
    note text NOT NULL,
    moderator_id integer NOT NULL,
    target_user_id integer,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.admin_logs OWNER TO postgres;

--
-- Name: admin_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.admin_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_logs_id_seq OWNER TO postgres;

--
-- Name: admin_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.admin_logs_id_seq OWNED BY public.admin_logs.id;


--
-- Name: affiliate_balances; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.affiliate_balances (
    id integer NOT NULL,
    affiliate_id bigint NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    crypto_balance bigint DEFAULT (0)::bigint NOT NULL,
    total_received bigint DEFAULT (0)::bigint NOT NULL,
    num_transactions integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    CONSTRAINT affiliate_balances_crypto_balance_positive CHECK ((crypto_balance >= 0)),
    CONSTRAINT affiliate_balances_total_received_positive CHECK ((total_received >= 0))
);


ALTER TABLE public.affiliate_balances OWNER TO postgres;

--
-- Name: affiliate_balances_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.affiliate_balances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affiliate_balances_id_seq OWNER TO postgres;

--
-- Name: affiliate_balances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.affiliate_balances_id_seq OWNED BY public.affiliate_balances.id;


--
-- Name: affiliate_skins; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.affiliate_skins (
    id integer NOT NULL,
    affiliate_id integer NOT NULL,
    template character varying(30) NOT NULL,
    skin character varying(30) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.affiliate_skins OWNER TO postgres;

--
-- Name: affiliate_skins_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.affiliate_skins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affiliate_skins_id_seq OWNER TO postgres;

--
-- Name: affiliate_skins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.affiliate_skins_id_seq OWNED BY public.affiliate_skins.id;


--
-- Name: affiliate_transactions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.affiliate_transactions (
    id bigint NOT NULL,
    affiliate_id bigint NOT NULL,
    affiliate_widget_id bigint,
    trade_id bigint,
    status_id smallint DEFAULT (1)::smallint NOT NULL,
    fiat_currency_id integer NOT NULL,
    fiat_amount numeric(25,2) NOT NULL,
    crypto_amount bigint NOT NULL,
    crypto_current_rate_usd numeric(15,2) NOT NULL,
    affiliate_fee_percentage numeric(5,2),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    type_id integer DEFAULT 1 NOT NULL,
    new_crypto_balance bigint,
    trade_fiat_amount_requested numeric(25,2) DEFAULT (0)::numeric NOT NULL,
    trade_crypto_amount_requested bigint DEFAULT (0)::bigint NOT NULL,
    track_id character varying(64),
    merchant_fee_percentage numeric(10,2),
    fiat_fee_amount numeric(16,2),
    crypto_fee_amount bigint,
    fiat_net_amount numeric(16,2),
    crypto_net_amount bigint
);


ALTER TABLE public.affiliate_transactions OWNER TO postgres;

--
-- Name: affiliate_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.affiliate_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affiliate_transactions_id_seq OWNER TO postgres;

--
-- Name: affiliate_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.affiliate_transactions_id_seq OWNED BY public.affiliate_transactions.id;


--
-- Name: affiliate_widgets; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.affiliate_widgets (
    id bigint NOT NULL,
    affiliate_id bigint NOT NULL,
    name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone
);


ALTER TABLE public.affiliate_widgets OWNER TO postgres;

--
-- Name: affiliate_widgets_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.affiliate_widgets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affiliate_widgets_id_seq OWNER TO postgres;

--
-- Name: affiliate_widgets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.affiliate_widgets_id_seq OWNED BY public.affiliate_widgets.id;


--
-- Name: affiliates; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.affiliates (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    name character varying(255),
    api_key character varying(64),
    secret_encrypted text,
    fee_percentage numeric(5,2) DEFAULT (2)::numeric NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone,
    is_merchant boolean DEFAULT false NOT NULL,
    user_registered_callback_url text,
    purchase_completed_callback_url text,
    purchase_completed_redirect_url text,
    trade_completed_callback_url text,
    is_trusted boolean DEFAULT false,
    trusted_at timestamp(0) without time zone,
    featured_offers jsonb,
    payment_method_groups_disabled jsonb,
    is_no_fee_on_sendouts boolean
);


ALTER TABLE public.affiliates OWNER TO postgres;

--
-- Name: affiliates_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.affiliates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affiliates_id_seq OWNER TO postgres;

--
-- Name: affiliates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.affiliates_id_seq OWNED BY public.affiliates.id;


--
-- Name: auth_logs; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.auth_logs (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    user_agent text,
    ip character varying(255),
    challenge_password_status smallint DEFAULT (0)::smallint NOT NULL,
    challenge_password_retries integer DEFAULT 0 NOT NULL,
    challenge_browser_status smallint,
    challenge_browser_retries integer DEFAULT 0 NOT NULL,
    challenge_sms_2_factor_status smallint,
    challenge_sms_2_factor_retries integer DEFAULT 0 NOT NULL,
    two_factor_type character varying(255),
    log_type smallint NOT NULL,
    passed boolean DEFAULT false NOT NULL,
    location_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    login_type smallint
);


ALTER TABLE public.auth_logs OWNER TO postgres;

--
-- Name: auth_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.auth_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_logs_id_seq OWNER TO postgres;

--
-- Name: auth_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.auth_logs_id_seq OWNED BY public.auth_logs.id;


--
-- Name: bank_account_bank_networks; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.bank_account_bank_networks (
    id integer NOT NULL,
    bank_account_id integer NOT NULL,
    bank_network_id integer NOT NULL,
    details jsonb NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.bank_account_bank_networks OWNER TO postgres;

--
-- Name: bank_account_bank_networks_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.bank_account_bank_networks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_account_bank_networks_id_seq OWNER TO postgres;

--
-- Name: bank_account_bank_networks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.bank_account_bank_networks_id_seq OWNED BY public.bank_account_bank_networks.id;


--
-- Name: bank_accounts; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.bank_accounts (
    id integer NOT NULL,
    bank_id integer NOT NULL,
    user_account_id bigint NOT NULL,
    holder_name character varying(255) NOT NULL,
    account_number character varying(255) NOT NULL,
    fiat_currency_id integer NOT NULL,
    funding boolean DEFAULT false NOT NULL,
    is_personal boolean DEFAULT true NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    country_id integer
);


ALTER TABLE public.bank_accounts OWNER TO postgres;

--
-- Name: bank_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.bank_accounts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_accounts_id_seq OWNER TO postgres;

--
-- Name: bank_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.bank_accounts_id_seq OWNED BY public.bank_accounts.id;


--
-- Name: bank_networks; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.bank_networks (
    id integer NOT NULL,
    slug character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.bank_networks OWNER TO postgres;

--
-- Name: bank_networks_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.bank_networks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_networks_id_seq OWNER TO postgres;

--
-- Name: bank_networks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.bank_networks_id_seq OWNED BY public.bank_networks.id;


--
-- Name: banks; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.banks (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    alias character varying(255),
    branch character varying(255),
    swift character varying(11),
    country_id integer NOT NULL,
    city character varying(255),
    zip character varying(50),
    zip_extended character varying(50),
    address character varying(255),
    status integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    routing_number character varying(9),
    state character varying(50)
);


ALTER TABLE public.banks OWNER TO postgres;

--
-- Name: banks_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.banks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banks_id_seq OWNER TO postgres;

--
-- Name: banks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.banks_id_seq OWNED BY public.banks.id;


--
-- Name: callbacks; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.callbacks (
    id bigint NOT NULL,
    type character varying(64) NOT NULL,
    url character varying(1024) NOT NULL,
    status character varying(64) NOT NULL,
    partner_type character varying(64) NOT NULL,
    partner_id bigint NOT NULL,
    track_id character varying(64) NOT NULL,
    data text NOT NULL,
    retries integer NOT NULL,
    last_retry_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.callbacks OWNER TO postgres;

--
-- Name: callbacks_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.callbacks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.callbacks_id_seq OWNER TO postgres;

--
-- Name: callbacks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.callbacks_id_seq OWNED BY public.callbacks.id;


--
-- Name: chainalysis_requests; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.chainalysis_requests (
    id bigint NOT NULL,
    transaction_id bigint NOT NULL,
    type smallint NOT NULL,
    risk smallint,
    cluster_name character varying(150),
    cluster_category character varying(150),
    response_success boolean,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.chainalysis_requests OWNER TO postgres;

--
-- Name: chainalysis_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.chainalysis_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chainalysis_requests_id_seq OWNER TO postgres;

--
-- Name: chainalysis_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.chainalysis_requests_id_seq OWNED BY public.chainalysis_requests.id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.cities (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    slug character varying(128) NOT NULL,
    country_id integer NOT NULL,
    country_iso character varying(5) NOT NULL,
    lat numeric(8,2) NOT NULL,
    lon numeric(8,2) NOT NULL
);


ALTER TABLE public.cities OWNER TO postgres;

--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.cities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cities_id_seq OWNER TO postgres;

--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.cities_id_seq OWNED BY public.cities.id;


--
-- Name: complyadvantage_searches; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.complyadvantage_searches (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    search_id bigint,
    ref character varying(96),
    first_name character varying(255),
    last_name character varying(255),
    middle_name character varying(255),
    match_status character varying(255),
    is_success boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.complyadvantage_searches OWNER TO postgres;

--
-- Name: complyadvantage_searches_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.complyadvantage_searches_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.complyadvantage_searches_id_seq OWNER TO postgres;

--
-- Name: complyadvantage_searches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.complyadvantage_searches_id_seq OWNED BY public.complyadvantage_searches.id;


--
-- Name: complyadvantage_tm_requests; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.complyadvantage_tm_requests (
    id bigint NOT NULL,
    transaction_id bigint NOT NULL,
    type smallint NOT NULL,
    response_status boolean,
    tries smallint DEFAULT '0'::smallint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.complyadvantage_tm_requests OWNER TO postgres;

--
-- Name: complyadvantage_tm_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.complyadvantage_tm_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.complyadvantage_tm_requests_id_seq OWNER TO postgres;

--
-- Name: complyadvantage_tm_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.complyadvantage_tm_requests_id_seq OWNED BY public.complyadvantage_tm_requests.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.countries (
    id integer NOT NULL,
    region_id smallint DEFAULT (0)::smallint NOT NULL,
    name character varying(250) NOT NULL,
    shortname character varying(4),
    code smallint DEFAULT (0)::smallint NOT NULL,
    lat numeric NOT NULL,
    lon numeric NOT NULL,
    zoom smallint DEFAULT (0)::smallint NOT NULL,
    sort_id smallint DEFAULT (0)::smallint NOT NULL,
    currency_id integer DEFAULT 0 NOT NULL,
    iso character varying(5),
    iso3 character varying(7),
    slug character varying(128) NOT NULL
);


ALTER TABLE public.countries OWNER TO postgres;

--
-- Name: country_city_landings_offers; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.country_city_landings_offers (
    id integer NOT NULL,
    offer_type smallint NOT NULL,
    fiat_currency_id integer,
    payment_method_id integer,
    country_id integer,
    offers character varying[]
);


ALTER TABLE public.country_city_landings_offers OWNER TO postgres;

--
-- Name: country_city_landings_offers_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.country_city_landings_offers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_city_landings_offers_id_seq OWNER TO postgres;

--
-- Name: country_city_landings_offers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.country_city_landings_offers_id_seq OWNED BY public.country_city_landings_offers.id;


--
-- Name: country_landings; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.country_landings (
    id bigint NOT NULL,
    country_id bigint NOT NULL,
    h1 text,
    h2 text,
    h3 text,
    description text,
    title text,
    meta_description text,
    meta_keywords text,
    main_image_url character varying(512),
    popular_payment_methods_deprecated character varying(64),
    manually_added_payment_methods_deprecated character varying(64),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone,
    use_manual_buy_popular_for_sell boolean DEFAULT false NOT NULL
);


ALTER TABLE public.country_landings OWNER TO postgres;

--
-- Name: country_landings_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.country_landings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_landings_id_seq OWNER TO postgres;

--
-- Name: country_landings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.country_landings_id_seq OWNED BY public.country_landings.id;


--
-- Name: cron_job; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.cron_job (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    return text NOT NULL,
    runtime real NOT NULL,
    cron_manager_id integer NOT NULL
);


ALTER TABLE public.cron_job OWNER TO postgres;

--
-- Name: cron_job_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.cron_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cron_job_id_seq OWNER TO postgres;

--
-- Name: cron_job_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.cron_job_id_seq OWNED BY public.cron_job.id;


--
-- Name: cron_manager; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.cron_manager (
    id integer NOT NULL,
    rundate timestamp without time zone NOT NULL,
    runtime real NOT NULL
);


ALTER TABLE public.cron_manager OWNER TO postgres;

--
-- Name: cron_manager_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.cron_manager_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cron_manager_id_seq OWNER TO postgres;

--
-- Name: cron_manager_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.cron_manager_id_seq OWNED BY public.cron_manager.id;


--
-- Name: crypto_blacklist; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_blacklist (
    address character varying(35) NOT NULL,
    description text,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.crypto_blacklist OWNER TO postgres;

--
-- Name: crypto_currencies; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_currencies (
    id integer NOT NULL,
    name character varying(64),
    code character(5),
    "rate_USD" numeric(10,4),
    "rate_BTC" numeric(16,8),
    date integer DEFAULT 0 NOT NULL,
    sort_id smallint DEFAULT (0)::smallint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.crypto_currencies OWNER TO postgres;

--
-- Name: crypto_currencies_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.crypto_currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crypto_currencies_id_seq OWNER TO postgres;

--
-- Name: crypto_currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.crypto_currencies_id_seq OWNED BY public.crypto_currencies.id;


--
-- Name: crypto_currency_forks; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_currency_forks (
    id bigint NOT NULL,
    original_crypto_currency_id integer NOT NULL,
    forked_crypto_currency_id integer NOT NULL,
    forked_crypto_currency_amount numeric(50,0) NOT NULL,
    forked_at timestamp without time zone NOT NULL,
    payout_crypto_currency_id integer NOT NULL,
    total_payout_amount numeric(50,0) NOT NULL,
    payout_rate numeric(20,8) NOT NULL,
    recipients_total_balance_before_fork numeric(50,0),
    recipients_count bigint,
    recipients_payout_amount numeric(50,0),
    processed_recipients_count bigint,
    processed_recipients_payout_amount numeric(50,0),
    status smallint NOT NULL,
    approved_by bigint,
    approved_at timestamp without time zone,
    confirmation_by bigint,
    confirmation_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.crypto_currency_forks OWNER TO postgres;

--
-- Name: crypto_currency_forks_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.crypto_currency_forks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crypto_currency_forks_id_seq OWNER TO postgres;

--
-- Name: crypto_currency_forks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.crypto_currency_forks_id_seq OWNED BY public.crypto_currency_forks.id;


--
-- Name: crypto_currency_forks_user_payouts; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_currency_forks_user_payouts (
    id bigint NOT NULL,
    crypto_currency_fork_id integer NOT NULL,
    user_account_id bigint NOT NULL,
    last_user_transaction_id bigint NOT NULL,
    balance_before_fork numeric(50,0) NOT NULL,
    payout_amount numeric(50,0),
    payout_user_transaction_id bigint,
    status smallint NOT NULL,
    paid_at timestamp without time zone,
    banned boolean NOT NULL,
    suspend boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.crypto_currency_forks_user_payouts OWNER TO postgres;

--
-- Name: crypto_currency_forks_user_payouts_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.crypto_currency_forks_user_payouts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crypto_currency_forks_user_payouts_id_seq OWNER TO postgres;

--
-- Name: crypto_currency_forks_user_payouts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.crypto_currency_forks_user_payouts_id_seq OWNED BY public.crypto_currency_forks_user_payouts.id;


--
-- Name: crypto_trade_notes; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trade_notes (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    author_id integer NOT NULL,
    trade_id integer NOT NULL,
    target_id character varying(255),
    message text NOT NULL,
    attachments jsonb,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.crypto_trade_notes OWNER TO postgres;

--
-- Name: crypto_trade_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.crypto_trade_notes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crypto_trade_notes_id_seq OWNER TO postgres;

--
-- Name: crypto_trade_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.crypto_trade_notes_id_seq OWNED BY public.crypto_trade_notes.id;


--
-- Name: crypto_trade_relations; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trade_relations (
    id integer NOT NULL,
    parent_id bigint NOT NULL,
    child_id bigint NOT NULL,
    type_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.crypto_trade_relations OWNER TO postgres;

--
-- Name: crypto_trade_relations_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.crypto_trade_relations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crypto_trade_relations_id_seq OWNER TO postgres;

--
-- Name: crypto_trade_relations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.crypto_trade_relations_id_seq OWNED BY public.crypto_trade_relations.id;


--
-- Name: crypto_trade_statuses; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trade_statuses (
    status_id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.crypto_trade_statuses OWNER TO postgres;

--
-- Name: crypto_trade_surveys; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trade_surveys (
    trade_id integer NOT NULL,
    question_id integer,
    other text,
    user_account_id integer NOT NULL,
    offer_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.crypto_trade_surveys OWNER TO postgres;

--
-- Name: crypto_trades_active; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trades_active (
    id bigint NOT NULL,
    id_hashed character varying(20),
    crypto_trade_status_id integer NOT NULL,
    offer_responder_id bigint NOT NULL,
    offer_owner_id bigint NOT NULL,
    offer_id bigint NOT NULL,
    payment_method_id integer NOT NULL,
    payment_window integer DEFAULT 0 NOT NULL,
    margin numeric(10,2) DEFAULT (0)::numeric NOT NULL,
    fiat_currency_id integer DEFAULT 144 NOT NULL,
    fiat_amount_requested numeric(25,2) DEFAULT (0)::numeric NOT NULL,
    fiat_price_per_btc numeric(20,2) DEFAULT (0)::numeric NOT NULL,
    crypto_amount_requested bigint DEFAULT (0)::bigint NOT NULL,
    crypto_amount_total bigint DEFAULT (0)::bigint NOT NULL,
    fee_percentage numeric(10,2) DEFAULT (1)::numeric NOT NULL,
    fee_crypto_amount bigint DEFAULT (0)::bigint NOT NULL,
    offer_terms text,
    trade_details text,
    offer_type smallint DEFAULT (1)::smallint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    seller_id bigint,
    buyer_id bigint,
    owner_country_id_logged_in bigint,
    responder_country_id_logged_in bigint,
    reopened boolean DEFAULT false NOT NULL,
    instant_funding boolean DEFAULT false NOT NULL,
    funding_started_at timestamp without time zone,
    funding_completed_at timestamp without time zone,
    escrow_funded_at timestamp without time zone,
    paid_at timestamp without time zone,
    cancelled_at timestamp without time zone,
    completed_at timestamp without time zone,
    job_id_payment_window character varying(100),
    job_id_trade_not_funded character varying(100),
    dispute_started_at timestamp without time zone,
    dispute_ended_at timestamp without time zone,
    responder_ip character varying(100),
    owner_ip character varying(100),
    crypto_current_rate_usd numeric(20,2) DEFAULT (0)::numeric NOT NULL,
    source_id smallint DEFAULT (1)::smallint NOT NULL,
    affiliate_id bigint,
    seller_fee_crypto_amount bigint DEFAULT (0)::bigint NOT NULL,
    buyer_fee_crypto_amount bigint DEFAULT (0)::bigint NOT NULL,
    seller_fee_percentage numeric(5,2) DEFAULT (0)::numeric NOT NULL,
    buyer_fee_percentage numeric(5,2) DEFAULT (0)::numeric NOT NULL,
    external_description character varying(100),
    external_crypto_address character varying(48),
    owner_responded_at timestamp without time zone,
    flow_type integer,
    CONSTRAINT check_financial_values CHECK (((payment_window > 0) AND (fiat_amount_requested > (0)::numeric) AND (crypto_amount_requested > 0) AND (crypto_amount_total > 0) AND (fiat_price_per_btc > (0)::numeric))),
    CONSTRAINT check_minimum_margin CHECK ((margin >= ('-99'::integer)::numeric)),
    CONSTRAINT check_offer_type_buy_or_sell CHECK (((offer_type = 1) OR (offer_type = 2)))
)
WITH (fillfactor='70', autovacuum_enabled='on');


ALTER TABLE public.crypto_trades_active OWNER TO postgres;

--
-- Name: crypto_trades_active_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.crypto_trades_active_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crypto_trades_active_id_seq OWNER TO postgres;

--
-- Name: crypto_trades_active_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.crypto_trades_active_id_seq OWNED BY public.crypto_trades_active.id;


--
-- Name: crypto_trades_completed; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trades_completed (
    id_hashed character varying(20),
    crypto_trade_status_id integer NOT NULL,
    offer_responder_id bigint NOT NULL,
    offer_owner_id bigint NOT NULL,
    offer_id bigint NOT NULL,
    payment_method_id integer NOT NULL,
    payment_window integer DEFAULT 0 NOT NULL,
    margin numeric(10,2) DEFAULT (0)::numeric NOT NULL,
    fiat_currency_id integer DEFAULT 144 NOT NULL,
    fiat_amount_requested numeric(25,2) DEFAULT (0)::numeric NOT NULL,
    fiat_price_per_btc numeric(20,2) DEFAULT (0)::numeric NOT NULL,
    crypto_amount_requested bigint DEFAULT (0)::bigint NOT NULL,
    crypto_amount_total bigint DEFAULT (0)::bigint NOT NULL,
    fee_percentage numeric(10,2) DEFAULT (1)::numeric NOT NULL,
    fee_crypto_amount bigint DEFAULT (0)::bigint NOT NULL,
    offer_terms text,
    trade_details text,
    offer_type smallint DEFAULT (1)::smallint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    seller_id bigint,
    buyer_id bigint,
    owner_country_id_logged_in bigint,
    responder_country_id_logged_in bigint,
    reopened boolean DEFAULT false NOT NULL,
    instant_funding boolean DEFAULT false NOT NULL,
    funding_started_at timestamp without time zone,
    funding_completed_at timestamp without time zone,
    escrow_funded_at timestamp without time zone,
    paid_at timestamp without time zone,
    cancelled_at timestamp without time zone,
    completed_at timestamp without time zone,
    trade_started_at timestamp without time zone,
    id bigint NOT NULL,
    dispute_started_at timestamp without time zone,
    dispute_ended_at timestamp without time zone,
    responder_ip character varying(100),
    owner_ip character varying(100),
    crypto_current_rate_usd numeric(20,2) DEFAULT (0)::numeric NOT NULL,
    invoice_uuid character varying(100),
    source_id smallint DEFAULT (1)::smallint NOT NULL,
    affiliate_id bigint,
    seller_fee_crypto_amount bigint DEFAULT (0)::bigint NOT NULL,
    buyer_fee_crypto_amount bigint DEFAULT (0)::bigint NOT NULL,
    seller_fee_percentage numeric(5,2) DEFAULT (0)::numeric NOT NULL,
    buyer_fee_percentage numeric(5,2) DEFAULT (0)::numeric NOT NULL,
    external_description character varying(100),
    external_crypto_address character varying(48),
    owner_responded_at timestamp without time zone,
    crypto_marketprice_usd_at_trade_end numeric(50,2),
    flow_type integer
);


ALTER TABLE public.crypto_trades_completed OWNER TO postgres;

--
-- Name: crypto_trades_details; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trades_details (
    id integer NOT NULL,
    trade_id bigint NOT NULL,
    type smallint NOT NULL,
    user_account_id bigint NOT NULL,
    partner_account_id bigint NOT NULL,
    moderator_opened_id bigint,
    partner_opened_at timestamp without time zone,
    moderator_opened_at timestamp without time zone,
    trade_created_at timestamp without time zone,
    value character varying(255),
    phash_original text,
    phash_watermark text,
    created_at timestamp without time zone NOT NULL,
    requirement integer,
    related_trades jsonb,
    related_trades_checked_at timestamp(0) without time zone
);


ALTER TABLE public.crypto_trades_details OWNER TO postgres;

--
-- Name: crypto_trades_details_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.crypto_trades_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crypto_trades_details_id_seq OWNER TO postgres;

--
-- Name: crypto_trades_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.crypto_trades_details_id_seq OWNED BY public.crypto_trades_details.id;


--
-- Name: crypto_trades_disputed; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trades_disputed (
    id bigint NOT NULL,
    crypto_trade_id bigint NOT NULL,
    dispute_assigned_user_account_id bigint,
    created_at timestamp without time zone NOT NULL,
    dispute_reason text,
    updated_at timestamp without time zone DEFAULT '2017-01-31 12:17:51'::timestamp without time zone NOT NULL,
    dispute_started_by bigint,
    last_moderator_replied_account_id bigint,
    last_moderator_replied_at timestamp without time zone,
    dispute_reason_id integer,
    dispute_opened_by_mod_at timestamp without time zone,
    tags jsonb,
    dispute_award_reason_id bigint
);


ALTER TABLE public.crypto_trades_disputed OWNER TO postgres;

--
-- Name: crypto_trades_disputed_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.crypto_trades_disputed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crypto_trades_disputed_id_seq OWNER TO postgres;

--
-- Name: crypto_trades_disputed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.crypto_trades_disputed_id_seq OWNED BY public.crypto_trades_disputed.id;


--
-- Name: crypto_trades_escrow; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trades_escrow (
    trade_id bigint NOT NULL,
    amount_without_fee bigint NOT NULL,
    amount_with_fee bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    CONSTRAINT amounts_positive CHECK (((amount_without_fee >= 0) AND (amount_with_fee >= 0)))
);


ALTER TABLE public.crypto_trades_escrow OWNER TO postgres;

--
-- Name: crypto_trades_payment_details; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trades_payment_details (
    trade_id integer NOT NULL,
    field_key character varying(255) NOT NULL,
    field_value text NOT NULL
);


ALTER TABLE public.crypto_trades_payment_details OWNER TO postgres;

--
-- Name: crypto_trades_payment_requirements; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.crypto_trades_payment_requirements (
    trade_id integer NOT NULL,
    requirement smallint NOT NULL,
    status smallint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    optional boolean DEFAULT false NOT NULL
);


ALTER TABLE public.crypto_trades_payment_requirements OWNER TO postgres;

--
-- Name: dispute_award_reasons; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.dispute_award_reasons (
    id bigint NOT NULL,
    "group" character varying(50),
    label character varying(255) NOT NULL,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public.dispute_award_reasons OWNER TO postgres;

--
-- Name: dispute_award_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.dispute_award_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispute_award_reasons_id_seq OWNER TO postgres;

--
-- Name: dispute_award_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.dispute_award_reasons_id_seq OWNED BY public.dispute_award_reasons.id;


--
-- Name: dispute_logs; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.dispute_logs (
    id integer NOT NULL,
    moderator_id integer NOT NULL,
    trade_id integer NOT NULL,
    offer_id integer NOT NULL,
    awarded_at timestamp without time zone NOT NULL,
    awarded_user_id integer NOT NULL
);


ALTER TABLE public.dispute_logs OWNER TO postgres;

--
-- Name: dispute_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.dispute_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispute_logs_id_seq OWNER TO postgres;

--
-- Name: dispute_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.dispute_logs_id_seq OWNED BY public.dispute_logs.id;


--
-- Name: dispute_reasons; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.dispute_reasons (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    translation_key character varying(255) NOT NULL,
    entity smallint NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.dispute_reasons OWNER TO postgres;

--
-- Name: dispute_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.dispute_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dispute_reasons_id_seq OWNER TO postgres;

--
-- Name: dispute_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.dispute_reasons_id_seq OWNED BY public.dispute_reasons.id;


--
-- Name: email_actions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.email_actions (
    token character varying(255) NOT NULL,
    action character varying(255) NOT NULL,
    user_account_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    used_at timestamp without time zone,
    expires_at timestamp without time zone
);


ALTER TABLE public.email_actions OWNER TO postgres;

--
-- Name: export_reports; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.export_reports (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    started_at timestamp without time zone NOT NULL,
    completed_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status smallint DEFAULT (1)::smallint NOT NULL,
    type smallint DEFAULT (1)::smallint NOT NULL,
    format smallint DEFAULT (1)::smallint NOT NULL,
    params character varying(500),
    admin_account_id bigint,
    disk character varying(255) DEFAULT 'local_export_reports'::character varying
);


ALTER TABLE public.export_reports OWNER TO postgres;

--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.failed_jobs (
    id integer NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    failed_at timestamp without time zone NOT NULL,
    exception text
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: feedback; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.feedback (
    id bigint NOT NULL,
    feedback_leaver_id bigint NOT NULL,
    feedback_receiver_id bigint NOT NULL,
    comment text,
    rating smallint DEFAULT (0)::smallint NOT NULL,
    trade_id bigint NOT NULL,
    reply_message text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone,
    is_replied boolean,
    offer_id integer
);


ALTER TABLE public.feedback OWNER TO postgres;

--
-- Name: feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.feedback_id_seq OWNER TO postgres;

--
-- Name: feedback_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.feedback_id_seq OWNED BY public.feedback.id;


--
-- Name: fiat_currencies; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.fiat_currencies (
    id integer NOT NULL,
    name character varying(64),
    code character varying(20),
    "rate_USD" numeric(25,4),
    "rate_BTC" numeric(25,8),
    sort_id smallint DEFAULT (0)::smallint NOT NULL,
    country_id_deprecated integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone,
    is_visible boolean DEFAULT false NOT NULL,
    copy_from_fiat_currency_id integer,
    default_amount integer
);


ALTER TABLE public.fiat_currencies OWNER TO postgres;

--
-- Name: fiat_currencies_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.fiat_currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fiat_currencies_id_seq OWNER TO postgres;

--
-- Name: fiat_currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.fiat_currencies_id_seq OWNED BY public.fiat_currencies.id;


--
-- Name: invoices; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.invoices (
    id bigint NOT NULL,
    participating_object_id integer NOT NULL,
    crypto_amount_requested bigint NOT NULL,
    fiat_currency_id integer NOT NULL,
    fiat_currency_rate integer NOT NULL,
    fiat_currency_amount integer NOT NULL,
    token character varying(100),
    email character varying(255) NOT NULL,
    message character varying(255) NOT NULL,
    address character varying(255),
    crypto_amount_paid bigint NOT NULL,
    fiat_currency_rate_paid bigint NOT NULL,
    date_paid character varying(255) NOT NULL,
    user_account_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.invoices OWNER TO postgres;

--
-- Name: invoices_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.invoices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invoices_id_seq OWNER TO postgres;

--
-- Name: invoices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.invoices_id_seq OWNED BY public.invoices.id;


--
-- Name: jumio_verifications; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.jumio_verifications (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    type smallint NOT NULL,
    document_type character varying(32),
    ip_address character varying(255),
    internal_reference character varying(255),
    reference character varying(255),
    is_success boolean,
    reject_reason character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    status smallint,
    verification_id integer,
    tries smallint,
    txn_status character varying(255),
    verification_status character varying(255),
    id_similarity character varying(255),
    id_validity boolean,
    error_code integer,
    document_number_hash character varying(64),
    image_retrieval_status smallint,
    available_images jsonb
);


ALTER TABLE public.jumio_verifications OWNER TO postgres;

--
-- Name: jumio_verifications_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.jumio_verifications_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jumio_verifications_id_seq OWNER TO postgres;

--
-- Name: jumio_verifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.jumio_verifications_id_seq OWNED BY public.jumio_verifications.id;


--
-- Name: kyc_verifications; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.kyc_verifications (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    type smallint NOT NULL,
    first_name character varying(255),
    last_name text,
    date_of_birth date,
    issue_country_id integer,
    document_type text,
    document_number text,
    document_expiration_date text,
    country_id integer,
    region_id integer,
    region_name character varying(255),
    city_id integer,
    city_name character varying(255),
    address_line_1 character varying(255),
    address_line_2 character varying(255),
    zip_code character varying(128),
    status smallint,
    approve_reason character varying(255),
    reject_reason character varying(255),
    admin_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    document_number_hash character varying(64),
    reviewing_comment character varying(255),
    summary jsonb,
    first_name_original character varying(255),
    last_name_original character varying(255),
    middle_name_original character varying(255),
    middle_name character varying(255),
    review_start timestamp(0) without time zone,
    review_end timestamp(0) without time zone,
    source character varying(255)
);


ALTER TABLE public.kyc_verifications OWNER TO postgres;

--
-- Name: kyc_verifications_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.kyc_verifications_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kyc_verifications_id_seq OWNER TO postgres;

--
-- Name: kyc_verifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.kyc_verifications_id_seq OWNED BY public.kyc_verifications.id;


--
-- Name: languages; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.languages (
    code character varying(5) NOT NULL,
    name character varying(255) NOT NULL,
    native character varying(255),
    type character varying(3) DEFAULT 'ltr'::character varying NOT NULL,
    "order" integer DEFAULT 999 NOT NULL,
    supported boolean DEFAULT false NOT NULL
);


ALTER TABLE public.languages OWNER TO postgres;

--
-- Name: locations; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.locations (
    id integer,
    continent_code character varying(255),
    continent_name character varying(255),
    country_iso_code character varying(255),
    country_name character varying(255),
    subdivision_iso_code character varying(255),
    subdivision_name character varying(255),
    city_name character varying(255),
    metro_code character varying(255),
    time_zone character varying(255),
    country_id integer,
    country_iso_code3 character varying(255),
    lat character varying(255),
    lon character varying(255),
    country_location_id integer,
    subdivision_location_id integer
);


ALTER TABLE public.locations OWNER TO postgres;

--
-- Name: lock_monitor; Type: VIEW; Schema: public; Owner: webadmin
--

CREATE VIEW public.lock_monitor AS
 SELECT COALESCE(((blockingl.relation)::regclass)::text, blockingl.locktype) AS locked_item,
    (now() - blockeda.query_start) AS waiting_duration,
    blockeda.pid AS blocked_pid,
    blockeda.query AS blocked_query,
    blockedl.mode AS blocked_mode,
    blockinga.pid AS blocking_pid,
    blockinga.query AS blocking_query,
    blockingl.mode AS blocking_mode
   FROM (((pg_locks blockedl
     JOIN pg_stat_activity blockeda ON ((blockedl.pid = blockeda.pid)))
     JOIN pg_locks blockingl ON ((((blockingl.transactionid = blockedl.transactionid) OR ((blockingl.relation = blockedl.relation) AND (blockingl.locktype = blockedl.locktype))) AND (blockedl.pid <> blockingl.pid))))
     JOIN pg_stat_activity blockinga ON (((blockingl.pid = blockinga.pid) AND (blockinga.datid = blockeda.datid))))
  WHERE ((NOT blockedl.granted) AND (blockinga.datname = current_database()));


ALTER TABLE public.lock_monitor OWNER TO postgres;

--
-- Name: merchant_deposit_address; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.merchant_deposit_address (
    id bigint NOT NULL,
    bitcoin_address character varying(48),
    user_account_id bigint NOT NULL,
    merchant_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.merchant_deposit_address OWNER TO postgres;

--
-- Name: merchant_deposit_address_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.merchant_deposit_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.merchant_deposit_address_id_seq OWNER TO postgres;

--
-- Name: merchant_deposit_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.merchant_deposit_address_id_seq OWNED BY public.merchant_deposit_address.id;


--
-- Name: merchants; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.merchants (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    secret text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_account_id bigint
);


ALTER TABLE public.merchants OWNER TO postgres;

--
-- Name: merchants_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.merchants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.merchants_id_seq OWNER TO postgres;

--
-- Name: merchants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.merchants_id_seq OWNED BY public.merchants.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.migrations (
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: moderator_checklist_items; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.moderator_checklist_items (
    id integer NOT NULL,
    checklist_key character varying(3) NOT NULL,
    label character varying(255) NOT NULL,
    description text NOT NULL,
    order_id smallint NOT NULL,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public.moderator_checklist_items OWNER TO postgres;

--
-- Name: moderator_checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.moderator_checklist_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.moderator_checklist_item_id_seq OWNER TO postgres;

--
-- Name: moderator_checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.moderator_checklist_item_id_seq OWNED BY public.moderator_checklist_items.id;


--
-- Name: moderator_checklist_logs; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.moderator_checklist_logs (
    id bigint NOT NULL,
    crypto_trades_disputed_id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    moderator_checklist_item_id integer NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.moderator_checklist_logs OWNER TO postgres;

--
-- Name: moderator_checklist_log_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.moderator_checklist_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.moderator_checklist_log_id_seq OWNER TO postgres;

--
-- Name: moderator_checklist_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.moderator_checklist_log_id_seq OWNED BY public.moderator_checklist_logs.id;


--
-- Name: monthly_reports; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.monthly_reports (
    id integer NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    average_bitcoin_price numeric(25,2),
    new_users integer,
    total_volume_crypto bigint,
    affiliate_total_volume_crypto bigint,
    total_trades integer,
    conversion_percentage numeric(5,2),
    total_profit_crypto bigint,
    default_affiliate_profit_crypto bigint,
    escrow_profit_crypto bigint,
    wallet_profit_crypto bigint,
    wallet_transactions integer,
    wallet_volume_crypto bigint,
    tickets_closed integer,
    disputes_closed integer,
    scam_reports integer,
    banned_accounts integer,
    scammer_funds_siezed integer,
    refunds_issued integer,
    new_vendors integer,
    verified_vendors integer,
    new_affiliates integer,
    new_merchants integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    wallet_transactions_by_merchant integer,
    wallet_transactions_fiat_amount_by_merchant numeric(25,2),
    wallet_transactions_by_non_merchant integer,
    wallet_transactions_fiat_amount_by_non_merchant numeric(25,2),
    wallet_transactions_crypto_amount_by_merchant bigint,
    wallet_transactions_crypto_amount_by_non_merchant bigint
);


ALTER TABLE public.monthly_reports OWNER TO postgres;

--
-- Name: monthly_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.monthly_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.monthly_reports_id_seq OWNER TO postgres;

--
-- Name: monthly_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.monthly_reports_id_seq OWNED BY public.monthly_reports.id;


--
-- Name: offer_payment_details; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.offer_payment_details (
    offer_id integer NOT NULL,
    payment_method_id integer NOT NULL,
    field_key character varying(255) NOT NULL,
    field_value text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.offer_payment_details OWNER TO postgres;

--
-- Name: offer_payment_requirements; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.offer_payment_requirements (
    offer_id integer NOT NULL,
    requirement smallint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.offer_payment_requirements OWNER TO postgres;

--
-- Name: offer_performance; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.offer_performance (
    offer_id integer NOT NULL,
    total_trades_count integer DEFAULT 0 NOT NULL,
    released integer DEFAULT 0 NOT NULL,
    cancelled_buyer_count integer DEFAULT 0 NOT NULL,
    cancelled_system_count integer DEFAULT 0 NOT NULL,
    cancelled_seller_count integer DEFAULT 0 NOT NULL,
    dispute_wins_seller_count integer DEFAULT 0 NOT NULL,
    dispute_wins_buyer_count integer DEFAULT 0 NOT NULL,
    last_trade_started_at timestamp without time zone,
    dispute_cancelled_buyer integer,
    disputed_trade_released integer,
    buyer_cancelled_before_paid integer,
    buyer_cancelled_after_paid integer,
    buyer_cancelled_after_uploading integer
);


ALTER TABLE public.offer_performance OWNER TO postgres;

--
-- Name: offer_tags; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.offer_tags (
    offer_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.offer_tags OWNER TO postgres;

--
-- Name: offers; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.offers (
    id bigint NOT NULL,
    id_hashed character varying(20),
    user_account_id bigint NOT NULL,
    active boolean DEFAULT true NOT NULL,
    offer_type smallint DEFAULT (1)::smallint NOT NULL,
    payment_method_id integer,
    payment_window integer DEFAULT 120 NOT NULL,
    margin numeric(10,2) DEFAULT (0)::numeric NOT NULL,
    fiat_currency_id integer DEFAULT 144 NOT NULL,
    fiat_amount_range_min numeric(25,2) NOT NULL,
    fiat_amount_range_max numeric(25,2) NOT NULL,
    crypto_derived_range_min bigint NOT NULL,
    crypto_derived_range_max bigint NOT NULL,
    fiat_price_per_btc numeric(25,2) NOT NULL,
    fee_percentage numeric(5,2) DEFAULT (1)::numeric NOT NULL,
    offer_terms text,
    trade_details text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    release_time integer,
    deleted_at timestamp without time zone,
    location_id integer,
    authorized boolean DEFAULT true NOT NULL,
    "fiat_USD_price_per_btc" numeric(25,2) DEFAULT (0)::numeric NOT NULL,
    country_id integer,
    require_verified_email boolean DEFAULT false NOT NULL,
    require_verified_phone boolean DEFAULT false NOT NULL,
    show_only_trusted_user boolean DEFAULT false NOT NULL,
    show_limit_max_coins boolean DEFAULT false NOT NULL,
    require_min_past_trades integer,
    payment_method_label character varying(255),
    predefined_amount bigint[],
    country_limitation_type integer,
    country_limitation_list character varying[],
    require_offer_currency_match_buyer_country boolean,
    block_anonymizer_users boolean,
    require_verified_id boolean,
    payment_method_country_id integer,
    country_limitation_smart boolean DEFAULT false NOT NULL,
    new_buyer_max_fiat_limit integer,
    price_datasource_key character varying(50),
    price_datapoint_key character varying(50),
    flat_fee integer,
    kiosk_verification_type integer,
    kiosk_require_minimum_trades boolean,
    kiosk_block_anonymizer_users boolean
)
WITH (fillfactor='70', autovacuum_enabled='on');


ALTER TABLE public.offers OWNER TO postgres;

--
-- Name: offers_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.offers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.offers_id_seq OWNER TO postgres;

--
-- Name: offers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.offers_id_seq OWNED BY public.offers.id;


--
-- Name: password_reminders; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.password_reminders (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.password_reminders OWNER TO postgres;

--
-- Name: payment_method_groups; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.payment_method_groups (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    buy_quantity integer DEFAULT 0 NOT NULL,
    sell_quantity integer DEFAULT 0 NOT NULL,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    popular_payment_methods_deprecated jsonb,
    order_by smallint,
    score_feedback_limit integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.payment_method_groups OWNER TO postgres;

--
-- Name: payment_method_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.payment_method_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_method_groups_id_seq OWNER TO postgres;

--
-- Name: payment_method_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.payment_method_groups_id_seq OWNED BY public.payment_method_groups.id;


--
-- Name: payment_method_stats; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.payment_method_stats (
    id bigint NOT NULL,
    fiat_currency_id bigint NOT NULL,
    payment_method_id bigint NOT NULL,
    avg_from numeric(10,2) DEFAULT (0)::numeric NOT NULL,
    avg_to numeric(10,2) DEFAULT (0)::numeric NOT NULL,
    week_trades_amount integer DEFAULT 0 NOT NULL,
    week_volume integer DEFAULT 0 NOT NULL,
    week_avg_profit integer DEFAULT 312 NOT NULL,
    scam_reports integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.payment_method_stats OWNER TO postgres;

--
-- Name: payment_method_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.payment_method_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_method_stats_id_seq OWNER TO postgres;

--
-- Name: payment_method_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.payment_method_stats_id_seq OWNED BY public.payment_method_stats.id;


--
-- Name: payment_methods; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.payment_methods (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    description_short text,
    description_long text,
    payment_window integer DEFAULT 0 NOT NULL,
    instructions text,
    approved boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    release_time integer,
    deleted_at timestamp without time zone,
    sell_quantity integer DEFAULT 0 NOT NULL,
    buy_quantity integer DEFAULT 0 NOT NULL,
    has_icon boolean DEFAULT false NOT NULL,
    payment_method_group_id integer,
    warning_text text,
    icon_url character varying(512),
    tags character varying(64),
    suggested_by_user_id integer,
    is_copyrighted boolean DEFAULT false NOT NULL,
    vendor_escrow_fee_percentage double precision DEFAULT '1'::double precision NOT NULL,
    dispute_start_disclaimer text,
    is_country_specific boolean DEFAULT false NOT NULL,
    merged_to_payment_method_id integer,
    countries jsonb,
    max_payment_window integer DEFAULT 4320 NOT NULL,
    CONSTRAINT vendor_fee_percentage_limit CHECK (((vendor_escrow_fee_percentage >= (0)::double precision) AND (vendor_escrow_fee_percentage <= (100)::double precision)))
)
WITH (autovacuum_vacuum_scale_factor='0', autovacuum_vacuum_threshold='100');


ALTER TABLE public.payment_methods OWNER TO postgres;

--
-- Name: payment_methods_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.payment_methods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_methods_id_seq OWNER TO postgres;

--
-- Name: payment_methods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.payment_methods_id_seq OWNED BY public.payment_methods.id;


--
-- Name: phone_calls; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.phone_calls (
    hash_id character(32) NOT NULL,
    "to" character varying(50) NOT NULL,
    type character varying(255) DEFAULT 'twilio'::character varying NOT NULL,
    call_received boolean DEFAULT false NOT NULL,
    text text NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.phone_calls OWNER TO postgres;

--
-- Name: popular_payment_methods; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.popular_payment_methods (
    id integer NOT NULL,
    type integer DEFAULT 0 NOT NULL,
    offer_type integer NOT NULL,
    country_id integer,
    payment_method_group_id integer,
    payment_method_id integer NOT NULL
);


ALTER TABLE public.popular_payment_methods OWNER TO postgres;

--
-- Name: popular_payment_methods_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.popular_payment_methods_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.popular_payment_methods_id_seq OWNER TO postgres;

--
-- Name: popular_payment_methods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.popular_payment_methods_id_seq OWNED BY public.popular_payment_methods.id;


--
-- Name: price_equations; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.price_equations (
    id integer NOT NULL,
    datasource_key character varying(255) NOT NULL,
    datapoint_key character varying(255) NOT NULL,
    price numeric(20,2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.price_equations OWNER TO postgres;

--
-- Name: price_equations_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.price_equations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.price_equations_id_seq OWNER TO postgres;

--
-- Name: price_equations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.price_equations_id_seq OWNED BY public.price_equations.id;


--
-- Name: pro_traders_list_for_email; Type: VIEW; Schema: public; Owner: webadmin
--

CREATE VIEW public.pro_traders_list_for_email AS
 SELECT string_agg((user_accounts.email)::text, ', '::text) AS string_agg
   FROM public.user_accounts
  WHERE ((user_accounts.marketplace_can_post = true) AND (user_accounts.suspend = false));


ALTER TABLE public.pro_traders_list_for_email OWNER TO postgres;

--
-- Name: referral_balances; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.referral_balances (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    crypto_balance bigint DEFAULT (0)::bigint NOT NULL,
    total_received bigint DEFAULT (0)::bigint NOT NULL,
    num_transactions integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    CONSTRAINT vendor_balances_crypto_balance_positive CHECK ((crypto_balance >= 0)),
    CONSTRAINT vendor_balances_total_received_positive CHECK ((total_received >= 0))
);


ALTER TABLE public.referral_balances OWNER TO postgres;

--
-- Name: referral_payouts; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.referral_payouts (
    id integer NOT NULL,
    trade_id bigint NOT NULL,
    seller_id bigint NOT NULL,
    buyer_id bigint NOT NULL,
    is_failed boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.referral_payouts OWNER TO postgres;

--
-- Name: referral_payouts_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.referral_payouts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.referral_payouts_id_seq OWNER TO postgres;

--
-- Name: referral_payouts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.referral_payouts_id_seq OWNED BY public.referral_payouts.id;


--
-- Name: referral_transactions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.referral_transactions (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    referral_user_account_id bigint,
    type_id integer DEFAULT 1 NOT NULL,
    trade_id bigint,
    status_id smallint DEFAULT (1)::smallint NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    fiat_currency_id integer NOT NULL,
    fiat_amount numeric(25,2) NOT NULL,
    crypto_amount bigint NOT NULL,
    crypto_current_rate_usd numeric(15,2) NOT NULL,
    referral_fee_percentage numeric(5,2),
    trade_fiat_amount_requested numeric(25,2) DEFAULT (0)::numeric NOT NULL,
    trade_crypto_amount_requested bigint DEFAULT (0)::bigint NOT NULL,
    new_crypto_balance bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    track character varying(50),
    tier smallint,
    initial_referral_user_account_id bigint
);


ALTER TABLE public.referral_transactions OWNER TO postgres;

--
-- Name: referrals; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.referrals (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    referral_user_account_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    track character varying(50)
);


ALTER TABLE public.referrals OWNER TO postgres;

--
-- Name: regions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.regions (
    id integer,
    name character varying(255),
    lat numeric,
    lon numeric,
    zoom smallint
);


ALTER TABLE public.regions OWNER TO postgres;

--
-- Name: salesman_lead_connections; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.salesman_lead_connections (
    id bigint NOT NULL,
    salesman_id integer NOT NULL,
    lead_id integer,
    is_active_connection boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    starting_mod_id integer,
    disconnecting_mod_id integer,
    disconnected_as_lead timestamp(0) without time zone,
    currently_connected_users_count integer DEFAULT 0 NOT NULL,
    past_connected_users_count integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.salesman_lead_connections OWNER TO postgres;

--
-- Name: salesman_lead_connections_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.salesman_lead_connections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.salesman_lead_connections_id_seq OWNER TO postgres;

--
-- Name: salesman_lead_connections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.salesman_lead_connections_id_seq OWNED BY public.salesman_lead_connections.id;


--
-- Name: sds_perfomance; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.sds_perfomance (
    id bigint NOT NULL,
    model character varying(50) NOT NULL,
    version character varying(50) NOT NULL,
    score numeric(5,2) NOT NULL,
    resolution character varying(50) NOT NULL,
    created_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.sds_perfomance OWNER TO postgres;

--
-- Name: sds_perfomance_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.sds_perfomance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sds_perfomance_id_seq OWNER TO postgres;

--
-- Name: sds_perfomance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.sds_perfomance_id_seq OWNED BY public.sds_perfomance.id;


--
-- Name: security_questions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.security_questions (
    id bigint NOT NULL,
    key character varying(255) NOT NULL,
    batch integer NOT NULL,
    active boolean DEFAULT false NOT NULL
);


ALTER TABLE public.security_questions OWNER TO postgres;

--
-- Name: security_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.security_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.security_questions_id_seq OWNER TO postgres;

--
-- Name: security_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.security_questions_id_seq OWNED BY public.security_questions.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    key character varying(128) NOT NULL,
    value text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.settings OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_id_seq OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: user_settings; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_settings (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    location_id integer,
    phone character varying(50),
    phone_activation_code integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    score integer DEFAULT 0 NOT NULL,
    forward_bitcoins boolean DEFAULT false NOT NULL,
    forward_address character varying(60),
    username_changed boolean DEFAULT false NOT NULL,
    phone_active boolean DEFAULT false NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    two_factor_withdrawal character varying(255),
    two_factor_login character varying(255),
    notify_email_profile_viewed boolean DEFAULT false NOT NULL,
    notify_email_offer_viewed boolean DEFAULT true NOT NULL,
    notify_profile_viewed boolean DEFAULT false NOT NULL,
    notify_offer_viewed boolean DEFAULT true NOT NULL,
    notify_bitcoins_incoming boolean DEFAULT true NOT NULL,
    notify_email_bitcoins_incoming boolean DEFAULT true NOT NULL,
    notify_bitcoins_confirmed boolean DEFAULT true NOT NULL,
    notify_email_bitcoins_confirmed boolean DEFAULT true NOT NULL,
    notify_sms_bitcoins_confirmed boolean DEFAULT false NOT NULL,
    notify_incoming_trade boolean DEFAULT true NOT NULL,
    notify_email_incoming_trade boolean DEFAULT true NOT NULL,
    notify_sms_incoming_trade boolean DEFAULT true NOT NULL,
    notify_partner_paid boolean DEFAULT true NOT NULL,
    notify_email_partner_paid boolean DEFAULT true NOT NULL,
    notify_sms_partner_paid boolean DEFAULT true NOT NULL,
    notify_email_trade_messages_interval integer DEFAULT 10 NOT NULL,
    notify_trade_cancelled boolean DEFAULT true NOT NULL,
    notify_email_trade_cancelled boolean DEFAULT true NOT NULL,
    notify_bitcoins_sold boolean DEFAULT true NOT NULL,
    notify_email_bitcoins_sold boolean DEFAULT true NOT NULL,
    notify_bitcoins_purchased boolean DEFAULT true NOT NULL,
    notify_email_bitcoins_purchased boolean DEFAULT true NOT NULL,
    bio text,
    email_subscribed boolean DEFAULT true NOT NULL,
    two_factor_secret text,
    two_factor_release_coins character varying(255),
    moderator_notes text,
    fiat_currency_id integer DEFAULT 144 NOT NULL,
    is_verified boolean DEFAULT false NOT NULL,
    bot_count integer DEFAULT 0 NOT NULL,
    max_bot_count integer DEFAULT 0 NOT NULL,
    bot_count_updated_at timestamp without time zone,
    ignore_surveys_deprecated boolean DEFAULT false NOT NULL,
    last_email_notification_sent timestamp without time zone,
    phone_carrier character varying(255),
    phone_type character varying(255),
    fiat_trade_max_limit_usd numeric(25,2),
    phone_lookup_required boolean,
    referral_fee_percentage_tier2 numeric(5,2),
    is_affiliate_banned boolean,
    phone_country_id integer,
    support_platform_notes text,
    is_id_verified boolean,
    id_verified_at timestamp(0) without time zone,
    moderator_country_override boolean,
    is_document_verified boolean,
    document_verified_at timestamp(0) without time zone,
    date_of_birth date,
    citizenship_id integer,
    complyadvantage_fuzziness double precision,
    middle_name character varying(255),
    id_verification_status smallint,
    id_verification_id integer,
    document_verification_status smallint,
    document_verification_id integer,
    blocked_kyc_actions jsonb,
    vendor_verification_allowed boolean,
    is_affiliate_paid_for_sell_side boolean,
    registration_domain character varying(30),
    security_questions_reset boolean
);


ALTER TABLE public.user_settings OWNER TO postgres;

--
-- Name: sign_ups_by_location; Type: VIEW; Schema: public; Owner: webadmin
--

CREATE VIEW public.sign_ups_by_location AS
 SELECT l.country_name,
    count(ua.id) AS sign_ups
   FROM ((public.user_accounts ua
     LEFT JOIN public.user_settings us ON ((us.user_account_id = ua.id)))
     LEFT JOIN public.locations l ON ((l.id = us.location_id)))
  GROUP BY l.country_name
  ORDER BY (count(ua.id)) DESC;


ALTER TABLE public.sign_ups_by_location OWNER TO postgres;

--
-- Name: slow_query_reports; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.slow_query_reports (
    id integer NOT NULL,
    type character varying(20) NOT NULL,
    statistics jsonb NOT NULL,
    batch smallint NOT NULL,
    conditions jsonb,
    is_reset_executed boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.slow_query_reports OWNER TO postgres;

--
-- Name: slow_query_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.slow_query_reports_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.slow_query_reports_id_seq OWNER TO postgres;

--
-- Name: slow_query_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.slow_query_reports_id_seq OWNED BY public.slow_query_reports.id;


--
-- Name: survey_questions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.survey_questions (
    id integer NOT NULL,
    text text NOT NULL,
    active boolean DEFAULT false NOT NULL,
    language_code character varying(255) DEFAULT 'en'::character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.survey_questions OWNER TO postgres;

--
-- Name: survey_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.survey_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.survey_questions_id_seq OWNER TO postgres;

--
-- Name: survey_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.survey_questions_id_seq OWNED BY public.survey_questions.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.tags (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL,
    suggested_by integer,
    suggested_at timestamp without time zone,
    approved_by integer,
    approved_at timestamp without time zone,
    icon_url character varying(255),
    slug character varying(100)
);


ALTER TABLE public.tags OWNER TO postgres;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_id_seq OWNER TO postgres;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.tags_id_seq OWNED BY public.tags.id;


--
-- Name: trade_fees; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.trade_fees (
    id bigint NOT NULL,
    trade_id bigint NOT NULL,
    fee_type_id integer NOT NULL,
    fee_crypto_amount bigint NOT NULL,
    fee_percentage numeric(5,2) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.trade_fees OWNER TO postgres;

--
-- Name: trade_fees_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.trade_fees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trade_fees_id_seq OWNER TO postgres;

--
-- Name: trade_fees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.trade_fees_id_seq OWNED BY public.trade_fees.id;


--
-- Name: trades_completed; Type: VIEW; Schema: public; Owner: webadmin
--

CREATE VIEW public.trades_completed AS
 SELECT t.total_trades,
    t.completed_trades,
    t.cancelled_seller,
    t.cancelled_buyer,
    t.cancelled_system,
    t.fees_earned,
    t.total_trade_volume,
        CASE
            WHEN (t.total_trades = 0) THEN (0)::text
            ELSE ((round((((t.completed_trades)::numeric / (t.total_trades)::numeric) * (100)::numeric), 2))::text || '%'::text)
        END AS conversion,
    t.completed_trade_volume,
    t.disputes_total,
    t.dispute_wins_seller,
    t.dispute_wins_buyer,
        CASE
            WHEN (t.total_trades = 0) THEN (0)::text
            ELSE ((round((((t.cancelled_buyer)::numeric / (t.total_trades)::numeric) * (100)::numeric), 2))::text || '%'::text)
        END AS cancelled_buyer_percentage,
        CASE
            WHEN (t.total_trades = 0) THEN (0)::text
            ELSE ((round((((t.cancelled_system)::numeric / (t.total_trades)::numeric) * (100)::numeric), 2))::text || '%'::text)
        END AS cancelled_system_percentage,
        CASE
            WHEN (t.total_trades = 0) THEN (0)::text
            ELSE ((round((((t.cancelled_seller)::numeric / (t.total_trades)::numeric) * (100)::numeric), 2))::text || '%'::text)
        END AS cancelled_seller_percentage,
        CASE
            WHEN (t.total_trades = 0) THEN (0)::text
            ELSE ((round((((t.dispute_wins_seller)::numeric / (t.total_trades)::numeric) * (100)::numeric), 2))::text || '%'::text)
        END AS dispute_wins_seller_percentage,
        CASE
            WHEN (t.total_trades = 0) THEN (0)::text
            ELSE ((round((((t.dispute_wins_buyer)::numeric / (t.total_trades)::numeric) * (100)::numeric), 2))::text || '%'::text)
        END AS dispute_wins_buyer_percentage
   FROM ( SELECT d.date,
            count(ctp.id) AS total_trades,
            count(
                CASE
                    WHEN (ctp.crypto_trade_status_id = 9) THEN 1
                    ELSE NULL::integer
                END) AS completed_trades,
            count(
                CASE
                    WHEN (ctp.crypto_trade_status_id = 8) THEN 1
                    ELSE NULL::integer
                END) AS cancelled_seller,
            count(
                CASE
                    WHEN (ctp.crypto_trade_status_id = 7) THEN 1
                    ELSE NULL::integer
                END) AS cancelled_buyer,
            count(
                CASE
                    WHEN (ctp.crypto_trade_status_id = 6) THEN 1
                    ELSE NULL::integer
                END) AS cancelled_system,
            (sum(
                CASE
                    WHEN (ctp.crypto_trade_status_id = 9) THEN ctp.fee_crypto_amount
                    ELSE (NULL::integer)::bigint
                END) / (100000000)::numeric) AS fees_earned,
            (sum(ctp.crypto_amount_requested) / (100000000)::numeric) AS total_trade_volume,
            (sum(
                CASE
                    WHEN (ctp.crypto_trade_status_id = 9) THEN ctp.crypto_amount_requested
                    ELSE (NULL::integer)::bigint
                END) / (100000000)::numeric) AS completed_trade_volume,
            count(
                CASE
                    WHEN ((ctp.crypto_trade_status_id = 11) OR (ctp.crypto_trade_status_id = 12)) THEN (1)::bigint
                    ELSE (NULL::integer)::bigint
                END) AS disputes_total,
            count(
                CASE
                    WHEN (ctp.crypto_trade_status_id = 11) THEN (1)::bigint
                    ELSE (NULL::integer)::bigint
                END) AS dispute_wins_seller,
            count(
                CASE
                    WHEN (ctp.crypto_trade_status_id = 12) THEN (1)::bigint
                    ELSE (NULL::integer)::bigint
                END) AS dispute_wins_buyer
           FROM (( SELECT to_char(date_trunc('day'::text, ((('now'::text)::date - offs.offs))::timestamp with time zone), 'YYYY-MM-DD'::text) AS date
                   FROM generate_series(0, 365, 1) offs(offs)) d
             LEFT JOIN public.crypto_trades_completed ctp ON ((d.date = to_char(date_trunc('day'::text, ctp.created_at), 'YYYY-MM-DD'::text))))
          GROUP BY d.date
          ORDER BY d.date DESC) t;


ALTER TABLE public.trades_completed OWNER TO postgres;

--
-- Name: transactions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.transactions (
    id bigint NOT NULL,
    user_account_id integer,
    type character varying(50) NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    crypto_amount bigint NOT NULL,
    crypto_miner_fee bigint DEFAULT 0 NOT NULL,
    crypto_add_to_balance bigint NOT NULL,
    fiat_crypto_ratio integer DEFAULT 0,
    crypto_current_rate_usd numeric(15,2) NOT NULL,
    fiat_amount numeric(25,2) NOT NULL,
    fiat_currency_id integer NOT NULL,
    fiat_current_rate numeric(25,2) NOT NULL,
    fiat_add_to_balance numeric(25,2) NOT NULL,
    sent_to_address character varying(100),
    transaction_hash character varying(255),
    forward_bitcoins boolean NOT NULL,
    forward_address character varying(48),
    forward_percentage integer,
    forward_tx_id character varying(100),
    confirms integer NOT NULL,
    label text,
    order_id bigint,
    order_code character varying(45),
    ip_address character varying(45),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    forward_amount bigint,
    host character varying(50),
    new_crypto_balance bigint DEFAULT (0)::bigint NOT NULL,
    new_fiat_balance numeric(25,2) DEFAULT (0)::numeric NOT NULL,
    priority character varying(64)
);


ALTER TABLE public.transactions OWNER TO postgres;

--
-- Name: transactions_failed; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.transactions_failed (
    id integer NOT NULL,
    transaction_hash character varying(255),
    reason text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.transactions_failed OWNER TO postgres;

--
-- Name: transactions_failed_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.transactions_failed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transactions_failed_id_seq OWNER TO postgres;

--
-- Name: transactions_failed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.transactions_failed_id_seq OWNED BY public.transactions_failed.id;


--
-- Name: transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transactions_id_seq OWNER TO postgres;

--
-- Name: transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.transactions_id_seq OWNED BY public.transactions.id;


--
-- Name: transactions_prepared; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.transactions_prepared (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    type character varying(50) NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    crypto_amount bigint NOT NULL,
    crypto_amount_no_fee bigint,
    crypto_add_to_balance bigint NOT NULL,
    new_crypto_balance bigint,
    sent_to_address character varying(100),
    transaction_hash character varying(255),
    confirms integer DEFAULT 0 NOT NULL,
    note text,
    crypto_current_rate_usd numeric(25,2) NOT NULL,
    host character varying(50),
    fiat_value numeric(25,2),
    trade_id bigint,
    merchant_id integer,
    processed boolean DEFAULT false NOT NULL,
    failed boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    source character varying(64),
    priority character varying(64),
    autosend_type character varying(64),
    batch_id bigint,
    track_id character varying(255)
);


ALTER TABLE public.transactions_prepared OWNER TO postgres;

--
-- Name: transactions_prepared_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.transactions_prepared_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transactions_prepared_id_seq OWNER TO postgres;

--
-- Name: transactions_prepared_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.transactions_prepared_id_seq OWNED BY public.transactions_prepared.id;


--
-- Name: two_fa_reset_tokens; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.two_fa_reset_tokens (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    created_by bigint NOT NULL,
    token character varying(255) NOT NULL,
    succeeded_at timestamp(0) without time zone,
    failed_at timestamp(0) without time zone,
    attempt_count smallint DEFAULT '0'::smallint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.two_fa_reset_tokens OWNER TO postgres;

--
-- Name: two_fa_reset_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.two_fa_reset_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.two_fa_reset_tokens_id_seq OWNER TO postgres;

--
-- Name: two_fa_reset_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.two_fa_reset_tokens_id_seq OWNED BY public.two_fa_reset_tokens.id;


--
-- Name: unconfirmed_transactions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.unconfirmed_transactions (
    id bigint NOT NULL,
    user_account_id bigint,
    type character varying(50) NOT NULL,
    receiving_address character varying(100),
    transaction_hash character varying(255),
    confirms integer DEFAULT 0 NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    crypto_current_rate_usd numeric(15,2),
    crypto_amount bigint NOT NULL,
    crypto_add_to_balance bigint NOT NULL,
    fiat_crypto_ratio integer DEFAULT 0 NOT NULL,
    fiat_amount numeric(25,2),
    fiat_currency_id integer,
    fiat_current_rate numeric(25,2),
    fiat_add_to_balance numeric(25,2),
    new_crypto_balance bigint DEFAULT (0)::bigint NOT NULL,
    new_fiat_balance numeric(25,2) DEFAULT (0)::numeric NOT NULL,
    forward_bitcoins boolean,
    forward_address character varying(48),
    forward_percentage integer,
    forward_amount bigint,
    label text,
    order_id bigint,
    order_code character varying(45),
    ip_address character varying(45),
    host character varying(50),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    source character varying(64),
    priority character varying(64),
    "position" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.unconfirmed_transactions OWNER TO postgres;

--
-- Name: unconfirmed_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.unconfirmed_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unconfirmed_transactions_id_seq OWNER TO postgres;

--
-- Name: unconfirmed_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.unconfirmed_transactions_id_seq OWNED BY public.unconfirmed_transactions.id;


--
-- Name: user_account_delete_reasons; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_account_delete_reasons (
    id integer NOT NULL,
    reasons character varying(255) NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.user_account_delete_reasons OWNER TO postgres;

--
-- Name: user_account_delete_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_account_delete_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_account_delete_reasons_id_seq OWNER TO postgres;

--
-- Name: user_account_delete_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_account_delete_reasons_id_seq OWNED BY public.user_account_delete_reasons.id;


--
-- Name: user_account_delete_requests; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_account_delete_requests (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    reason character varying(255) NOT NULL,
    status smallint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.user_account_delete_requests OWNER TO postgres;

--
-- Name: user_account_delete_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_account_delete_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_account_delete_requests_id_seq OWNER TO postgres;

--
-- Name: user_account_delete_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_account_delete_requests_id_seq OWNED BY public.user_account_delete_requests.id;


--
-- Name: user_accounts_deleted; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_accounts_deleted (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    username character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.user_accounts_deleted OWNER TO postgres;

--
-- Name: user_accounts_deleted_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_accounts_deleted_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_accounts_deleted_id_seq OWNER TO postgres;

--
-- Name: user_accounts_deleted_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_accounts_deleted_id_seq OWNED BY public.user_accounts_deleted.id;


--
-- Name: user_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_accounts_id_seq OWNER TO postgres;

--
-- Name: user_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_accounts_id_seq OWNED BY public.user_accounts.id;


--
-- Name: user_addresses; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_addresses (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    country_id integer,
    region_id integer,
    region_name character varying(255),
    city_id integer,
    city_name character varying(255),
    address_line_1 character varying(255),
    address_line_2 character varying(255),
    zip_code character varying(128),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.user_addresses OWNER TO postgres;

--
-- Name: user_addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_addresses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_addresses_id_seq OWNER TO postgres;

--
-- Name: user_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_addresses_id_seq OWNED BY public.user_addresses.id;


--
-- Name: user_api_permissions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_api_permissions (
    id integer NOT NULL,
    user_api_id bigint NOT NULL,
    endpoint character varying(255) NOT NULL,
    locked boolean NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.user_api_permissions OWNER TO postgres;

--
-- Name: user_api_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_api_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_api_permissions_id_seq OWNER TO postgres;

--
-- Name: user_api_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_api_permissions_id_seq OWNED BY public.user_api_permissions.id;


--
-- Name: user_apis; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_apis (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    key character varying(255) NOT NULL,
    secret text NOT NULL,
    rate_limit integer DEFAULT 1000 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    whitelist jsonb
);


ALTER TABLE public.user_apis OWNER TO postgres;

--
-- Name: user_apis_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_apis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_apis_id_seq OWNER TO postgres;

--
-- Name: user_apis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_apis_id_seq OWNED BY public.user_apis.id;


--
-- Name: user_badges; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_badges (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_badge_type_id integer
);


ALTER TABLE public.user_badges OWNER TO postgres;

--
-- Name: user_badges_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_badges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_badges_id_seq OWNER TO postgres;

--
-- Name: user_badges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_badges_id_seq OWNED BY public.user_badges.id;


--
-- Name: user_compliance; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_compliance (
    user_account_id bigint NOT NULL,
    id_verified_at timestamp(0) without time zone,
    id_verification_status smallint,
    id_verification_id integer,
    document_verified_at timestamp(0) without time zone,
    document_verification_status smallint,
    document_verification_id integer,
    complyadvantage_fuzziness double precision,
    blocked_kyc_actions jsonb,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    compliance_notes jsonb,
    subpoena_numbers text,
    single_trade_limit integer,
    id_verification_required_reason character varying(255),
    document_verification_required_reason character varying(255)
);


ALTER TABLE public.user_compliance OWNER TO postgres;

--
-- Name: user_crypto_addresses; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_crypto_addresses (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    address character varying(48),
    label text,
    crypto_total_received bigint DEFAULT (0)::bigint NOT NULL,
    crypto_previous_balance bigint DEFAULT (0)::bigint NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    qr_code_path character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    crypto_balance bigint DEFAULT (0)::bigint NOT NULL,
    address_type smallint DEFAULT (1)::smallint NOT NULL
);


ALTER TABLE public.user_crypto_addresses OWNER TO postgres;

--
-- Name: user_crypto_addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_crypto_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_crypto_addresses_id_seq OWNER TO postgres;

--
-- Name: user_crypto_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_crypto_addresses_id_seq OWNED BY public.user_crypto_addresses.id;


--
-- Name: user_crypto_balances_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_crypto_balances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_crypto_balances_id_seq OWNER TO postgres;

--
-- Name: user_crypto_balances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_crypto_balances_id_seq OWNED BY public.user_crypto_balances.id;


--
-- Name: user_devices; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_devices (
    id integer NOT NULL,
    uuid character varying(255) NOT NULL,
    collision_uuid character varying(255),
    user_agent text NOT NULL,
    user_account_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.user_devices OWNER TO postgres;

--
-- Name: user_devices_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_devices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_devices_id_seq OWNER TO postgres;

--
-- Name: user_devices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_devices_id_seq OWNED BY public.user_devices.id;


--
-- Name: user_emails_blacklist; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_emails_blacklist (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    reason character varying(255) NOT NULL,
    admin_account_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.user_emails_blacklist OWNER TO postgres;

--
-- Name: user_emails_blacklist_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_emails_blacklist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_emails_blacklist_id_seq OWNER TO postgres;

--
-- Name: user_emails_blacklist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_emails_blacklist_id_seq OWNED BY public.user_emails_blacklist.id;


--
-- Name: user_feature_flags; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_feature_flags (
    id integer NOT NULL,
    user_account_id integer NOT NULL,
    key character varying(255) NOT NULL,
    value boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.user_feature_flags OWNER TO postgres;

--
-- Name: user_feature_flags_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_feature_flags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_feature_flags_id_seq OWNER TO postgres;

--
-- Name: user_feature_flags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_feature_flags_id_seq OWNED BY public.user_feature_flags.id;


--
-- Name: user_freeway_histories; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_freeway_histories (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    affiliate_id bigint NOT NULL,
    template character varying(255) NOT NULL,
    step character varying(255) NOT NULL,
    is_protected boolean DEFAULT false NOT NULL,
    is_exact_amount boolean DEFAULT false NOT NULL,
    fiat_currency_id integer NOT NULL,
    fiat_amount numeric(25,2),
    crypto_amount bigint,
    payment_method_group_id integer,
    payment_method_id integer,
    offer_id integer,
    offer_owner_id integer,
    affiliate_widget_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    trade_id bigint,
    external_crypto_address character varying(48),
    external_description character varying(100),
    track_id character varying(64),
    CONSTRAINT user_freeway_histories_template_check CHECK (((template)::text = ANY (ARRAY[('default'::character varying)::text, ('trusted_debit'::character varying)::text])))
);


ALTER TABLE public.user_freeway_histories OWNER TO postgres;

--
-- Name: user_freeway_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_freeway_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_freeway_histories_id_seq OWNER TO postgres;

--
-- Name: user_freeway_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_freeway_histories_id_seq OWNED BY public.user_freeway_histories.id;


--
-- Name: user_ip_history; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_ip_history (
    user_account_id integer NOT NULL,
    ip character varying(255) NOT NULL,
    user_agent text,
    created_at timestamp without time zone NOT NULL,
    country_code character varying(2),
    country_name character varying(255),
    city character varying(255),
    postal_code character varying(255),
    latitude double precision,
    longitude double precision,
    trusted boolean DEFAULT false NOT NULL,
    is_anonymous smallint,
    country_id integer,
    updated_at timestamp(0) without time zone,
    state_code character varying(255)
);


ALTER TABLE public.user_ip_history OWNER TO postgres;

--
-- Name: user_issues; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_issues (
    user_account_id bigint NOT NULL,
    email_invalid boolean,
    email_disposable boolean,
    email_honeypot boolean,
    email_recent_abuse boolean,
    email_generic boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.user_issues OWNER TO postgres;

--
-- Name: user_languages; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_languages (
    user_account_id bigint NOT NULL,
    language_code character varying(5) NOT NULL
);


ALTER TABLE public.user_languages OWNER TO postgres;

--
-- Name: user_notifications_settings; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_notifications_settings (
    user_account_id bigint NOT NULL,
    mobile jsonb NOT NULL
);


ALTER TABLE public.user_notifications_settings OWNER TO postgres;

--
-- Name: user_pdf_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_pdf_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_pdf_reports_id_seq OWNER TO postgres;

--
-- Name: user_pdf_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_pdf_reports_id_seq OWNED BY public.export_reports.id;


--
-- Name: user_relations; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_relations (
    id bigint NOT NULL,
    initiator_user_id bigint NOT NULL,
    target_user_id bigint NOT NULL,
    relation character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    CONSTRAINT user_relations_relation_check CHECK (((relation)::text = ANY (ARRAY[('trusted'::character varying)::text, ('untrusted'::character varying)::text, ('blocked'::character varying)::text, ('unblocked'::character varying)::text])))
);


ALTER TABLE public.user_relations OWNER TO postgres;

--
-- Name: user_relations_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_relations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_relations_id_seq OWNER TO postgres;

--
-- Name: user_relations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_relations_id_seq OWNED BY public.user_relations.id;


--
-- Name: user_reports; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_reports (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    target_trade_id integer,
    target_user_id integer,
    user_account_id integer NOT NULL,
    content text NOT NULL,
    closed_at timestamp without time zone,
    closed_by integer,
    status boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    target_offer_id integer,
    archived boolean DEFAULT false NOT NULL
);


ALTER TABLE public.user_reports OWNER TO postgres;

--
-- Name: user_reports_attachments; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_reports_attachments (
    id integer NOT NULL,
    report_id integer NOT NULL,
    path character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.user_reports_attachments OWNER TO postgres;

--
-- Name: user_reports_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_reports_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_reports_attachments_id_seq OWNER TO postgres;

--
-- Name: user_reports_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_reports_attachments_id_seq OWNED BY public.user_reports_attachments.id;


--
-- Name: user_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_reports_id_seq OWNER TO postgres;

--
-- Name: user_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_reports_id_seq OWNED BY public.user_reports.id;


--
-- Name: user_security_questions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_security_questions (
    user_account_id bigint NOT NULL,
    security_question_id bigint NOT NULL,
    answer text NOT NULL
);


ALTER TABLE public.user_security_questions OWNER TO postgres;

--
-- Name: user_segments; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_segments (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    segment_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone
);


ALTER TABLE public.user_segments OWNER TO postgres;

--
-- Name: user_segments_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_segments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_segments_id_seq OWNER TO postgres;

--
-- Name: user_segments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_segments_id_seq OWNED BY public.user_segments.id;


--
-- Name: user_sessions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_sessions (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    user_agent text,
    ip character varying(255),
    session_encrypted text,
    location_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    browser character varying(255)
);


ALTER TABLE public.user_sessions OWNER TO postgres;

--
-- Name: user_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_sessions_id_seq OWNER TO postgres;

--
-- Name: user_sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_sessions_id_seq OWNED BY public.user_sessions.id;


--
-- Name: user_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_settings_id_seq OWNER TO postgres;

--
-- Name: user_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_settings_id_seq OWNED BY public.user_settings.id;


--
-- Name: user_signups_by_day; Type: VIEW; Schema: public; Owner: webadmin
--

CREATE VIEW public.user_signups_by_day AS
 SELECT t.date AS date_registered,
    t.registered_users,
    t.email_activated,
        CASE
            WHEN (t.registered_users = 0) THEN ((t.registered_users / NULL::bigint))::text
            ELSE ((round((((t.email_activated * 100))::numeric / (t.registered_users)::numeric), 0))::text || '%'::text)
        END AS email_activation_percent,
    t.phone_activated,
        CASE
            WHEN (t.registered_users = 0) THEN ((t.registered_users / NULL::bigint))::text
            ELSE ((round((((t.phone_activated * 100))::numeric / (t.registered_users)::numeric), 0))::text || '%'::text)
        END AS phone_activation_percent
   FROM ( SELECT d.date,
            count(ua.id) AS registered_users,
            count(
                CASE
                    WHEN ua.email_active THEN 1
                    ELSE NULL::integer
                END) AS email_activated,
            count(
                CASE
                    WHEN us.phone_active THEN 1
                    ELSE NULL::integer
                END) AS phone_activated
           FROM ((( SELECT to_char(date_trunc('day'::text, ((('now'::text)::date - offs.offs))::timestamp with time zone), 'YYYY-MM-DD'::text) AS date
                   FROM generate_series(0, 365, 1) offs(offs)) d
             LEFT JOIN public.user_accounts ua ON ((d.date = to_char(date_trunc('day'::text, ua.created_at), 'YYYY-MM-DD'::text))))
             LEFT JOIN public.user_settings us ON ((us.user_account_id = ua.id)))
          GROUP BY d.date
          ORDER BY d.date DESC) t;


ALTER TABLE public.user_signups_by_day OWNER TO postgres;

--
-- Name: user_site_messages; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_site_messages (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    reason_key character varying(255),
    additional_details text,
    footprint jsonb,
    created_by bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    opened_at timestamp(0) without time zone,
    confirmed_at timestamp(0) without time zone
);


ALTER TABLE public.user_site_messages OWNER TO postgres;

--
-- Name: user_site_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_site_messages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_site_messages_id_seq OWNER TO postgres;

--
-- Name: user_site_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_site_messages_id_seq OWNED BY public.user_site_messages.id;


--
-- Name: user_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_stats_id_seq OWNER TO postgres;

--
-- Name: user_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_stats_id_seq OWNED BY public.user_stats.id;


--
-- Name: user_status_actions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_status_actions (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    status character varying(50) NOT NULL,
    reason_key character varying(50),
    reason text,
    custom_reason text,
    next_status character varying(50),
    next_status_date timestamp(0) without time zone,
    archived boolean DEFAULT false NOT NULL,
    created_by bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.user_status_actions OWNER TO postgres;

--
-- Name: user_status_actions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_status_actions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_status_actions_id_seq OWNER TO postgres;

--
-- Name: user_status_actions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_status_actions_id_seq OWNED BY public.user_status_actions.id;


--
-- Name: user_transactions; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_transactions (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    type character varying(50) NOT NULL,
    crypto_currency_id integer DEFAULT 1 NOT NULL,
    crypto_amount bigint NOT NULL,
    crypto_add_to_balance bigint NOT NULL,
    new_crypto_balance bigint,
    sent_to_address character varying(100),
    transaction_hash character varying(255),
    confirms integer DEFAULT 0 NOT NULL,
    note text,
    crypto_current_rate_usd numeric(25,2) NOT NULL,
    host character varying(50),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fiat_value numeric(25,2),
    trade_id bigint,
    merchant_id integer,
    crypto_amount_no_fee bigint,
    source character varying(64),
    priority character varying(64),
    autosend_type character varying(64),
    "position" smallint DEFAULT 0,
    linked_user_transaction_id bigint,
    crypto_current_rate_fiat numeric(25,2),
    new_crypto_balance_old bigint,
    user_note character varying(255),
    merchant_name character varying(64),
    track_id character varying(64)
);


ALTER TABLE public.user_transactions OWNER TO postgres;

--
-- Name: user_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_transactions_id_seq OWNER TO postgres;

--
-- Name: user_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_transactions_id_seq OWNED BY public.user_transactions.id;


--
-- Name: user_verifications; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.user_verifications (
    id integer NOT NULL,
    user_account_id bigint NOT NULL,
    video_status integer DEFAULT 0 NOT NULL,
    video_url character varying(255),
    selfie_status integer DEFAULT 0 NOT NULL,
    selfie_url character varying(255),
    doc_status integer DEFAULT 0 NOT NULL,
    doc_url character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.user_verifications OWNER TO postgres;

--
-- Name: user_verifications_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.user_verifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_verifications_id_seq OWNER TO postgres;

--
-- Name: user_verifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.user_verifications_id_seq OWNED BY public.user_verifications.id;


--
-- Name: vendor_balances_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.vendor_balances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vendor_balances_id_seq OWNER TO postgres;

--
-- Name: vendor_balances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.vendor_balances_id_seq OWNED BY public.referral_balances.id;


--
-- Name: vendor_referrals_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.vendor_referrals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vendor_referrals_id_seq OWNER TO postgres;

--
-- Name: vendor_referrals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.vendor_referrals_id_seq OWNED BY public.referrals.id;


--
-- Name: vendor_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.vendor_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vendor_transactions_id_seq OWNER TO postgres;

--
-- Name: vendor_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.vendor_transactions_id_seq OWNED BY public.referral_transactions.id;


--
-- Name: vendors; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.vendors (
    id bigint NOT NULL,
    user_account_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone,
    referral_fee_percentage numeric(5,2) DEFAULT (20)::numeric NOT NULL
);


ALTER TABLE public.vendors OWNER TO postgres;

--
-- Name: vendors_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.vendors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vendors_id_seq OWNER TO postgres;

--
-- Name: vendors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.vendors_id_seq OWNED BY public.vendors.id;


--
-- Name: visits; Type: TABLE; Schema: public; Owner: webadmin
--

CREATE TABLE public.visits (
    id integer NOT NULL,
    user_account_id integer,
    user_relation_type character varying(30),
    cookie_token character varying(60) NOT NULL,
    landing_page character varying(255) NOT NULL,
    referrer_domain character varying(100),
    referrer_url character varying(255),
    referrer character varying(100),
    utm_source character varying(100),
    utm_campaign character varying(100),
    utm_medium character varying(100),
    utm_term character varying(1000),
    utm_content character varying(100),
    referral character varying(100),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    uri character varying(2000)
);


ALTER TABLE public.visits OWNER TO postgres;

--
-- Name: visits_id_seq; Type: SEQUENCE; Schema: public; Owner: webadmin
--

CREATE SEQUENCE public.visits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.visits_id_seq OWNER TO postgres;

--
-- Name: visits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webadmin
--

ALTER SEQUENCE public.visits_id_seq OWNED BY public.visits.id;


--
-- Name: admin_logs id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.admin_logs ALTER COLUMN id SET DEFAULT nextval('public.admin_logs_id_seq'::regclass);


--
-- Name: affiliate_balances id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_balances ALTER COLUMN id SET DEFAULT nextval('public.affiliate_balances_id_seq'::regclass);


--
-- Name: affiliate_skins id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_skins ALTER COLUMN id SET DEFAULT nextval('public.affiliate_skins_id_seq'::regclass);


--
-- Name: affiliate_transactions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_transactions ALTER COLUMN id SET DEFAULT nextval('public.affiliate_transactions_id_seq'::regclass);


--
-- Name: affiliate_widgets id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_widgets ALTER COLUMN id SET DEFAULT nextval('public.affiliate_widgets_id_seq'::regclass);


--
-- Name: affiliates id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliates ALTER COLUMN id SET DEFAULT nextval('public.affiliates_id_seq'::regclass);


--
-- Name: auth_logs id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.auth_logs ALTER COLUMN id SET DEFAULT nextval('public.auth_logs_id_seq'::regclass);


--
-- Name: bank_account_bank_networks id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_account_bank_networks ALTER COLUMN id SET DEFAULT nextval('public.bank_account_bank_networks_id_seq'::regclass);


--
-- Name: bank_accounts id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_accounts ALTER COLUMN id SET DEFAULT nextval('public.bank_accounts_id_seq'::regclass);


--
-- Name: bank_networks id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_networks ALTER COLUMN id SET DEFAULT nextval('public.bank_networks_id_seq'::regclass);


--
-- Name: banks id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.banks ALTER COLUMN id SET DEFAULT nextval('public.banks_id_seq'::regclass);


--
-- Name: callbacks id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.callbacks ALTER COLUMN id SET DEFAULT nextval('public.callbacks_id_seq'::regclass);


--
-- Name: chainalysis_requests id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.chainalysis_requests ALTER COLUMN id SET DEFAULT nextval('public.chainalysis_requests_id_seq'::regclass);


--
-- Name: cities id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.cities ALTER COLUMN id SET DEFAULT nextval('public.cities_id_seq'::regclass);


--
-- Name: complyadvantage_searches id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.complyadvantage_searches ALTER COLUMN id SET DEFAULT nextval('public.complyadvantage_searches_id_seq'::regclass);


--
-- Name: complyadvantage_tm_requests id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.complyadvantage_tm_requests ALTER COLUMN id SET DEFAULT nextval('public.complyadvantage_tm_requests_id_seq'::regclass);


--
-- Name: country_city_landings_offers id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.country_city_landings_offers ALTER COLUMN id SET DEFAULT nextval('public.country_city_landings_offers_id_seq'::regclass);


--
-- Name: country_landings id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.country_landings ALTER COLUMN id SET DEFAULT nextval('public.country_landings_id_seq'::regclass);


--
-- Name: cron_job id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.cron_job ALTER COLUMN id SET DEFAULT nextval('public.cron_job_id_seq'::regclass);


--
-- Name: cron_manager id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.cron_manager ALTER COLUMN id SET DEFAULT nextval('public.cron_manager_id_seq'::regclass);


--
-- Name: crypto_currencies id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currencies ALTER COLUMN id SET DEFAULT nextval('public.crypto_currencies_id_seq'::regclass);


--
-- Name: crypto_currency_forks id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks ALTER COLUMN id SET DEFAULT nextval('public.crypto_currency_forks_id_seq'::regclass);


--
-- Name: crypto_currency_forks_user_payouts id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks_user_payouts ALTER COLUMN id SET DEFAULT nextval('public.crypto_currency_forks_user_payouts_id_seq'::regclass);


--
-- Name: crypto_trade_notes id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_notes ALTER COLUMN id SET DEFAULT nextval('public.crypto_trade_notes_id_seq'::regclass);


--
-- Name: crypto_trade_relations id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_relations ALTER COLUMN id SET DEFAULT nextval('public.crypto_trade_relations_id_seq'::regclass);


--
-- Name: crypto_trades_active id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active ALTER COLUMN id SET DEFAULT nextval('public.crypto_trades_active_id_seq'::regclass);


--
-- Name: crypto_trades_details id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_details ALTER COLUMN id SET DEFAULT nextval('public.crypto_trades_details_id_seq'::regclass);


--
-- Name: crypto_trades_disputed id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_disputed ALTER COLUMN id SET DEFAULT nextval('public.crypto_trades_disputed_id_seq'::regclass);


--
-- Name: dispute_award_reasons id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_award_reasons ALTER COLUMN id SET DEFAULT nextval('public.dispute_award_reasons_id_seq'::regclass);


--
-- Name: dispute_logs id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_logs ALTER COLUMN id SET DEFAULT nextval('public.dispute_logs_id_seq'::regclass);


--
-- Name: dispute_reasons id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_reasons ALTER COLUMN id SET DEFAULT nextval('public.dispute_reasons_id_seq'::regclass);


--
-- Name: export_reports id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.export_reports ALTER COLUMN id SET DEFAULT nextval('public.user_pdf_reports_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: feedback id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.feedback ALTER COLUMN id SET DEFAULT nextval('public.feedback_id_seq'::regclass);


--
-- Name: fiat_currencies id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.fiat_currencies ALTER COLUMN id SET DEFAULT nextval('public.fiat_currencies_id_seq'::regclass);


--
-- Name: invoices id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.invoices ALTER COLUMN id SET DEFAULT nextval('public.invoices_id_seq'::regclass);


--
-- Name: jumio_verifications id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.jumio_verifications ALTER COLUMN id SET DEFAULT nextval('public.jumio_verifications_id_seq'::regclass);


--
-- Name: kyc_verifications id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.kyc_verifications ALTER COLUMN id SET DEFAULT nextval('public.kyc_verifications_id_seq'::regclass);


--
-- Name: merchant_deposit_address id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.merchant_deposit_address ALTER COLUMN id SET DEFAULT nextval('public.merchant_deposit_address_id_seq'::regclass);


--
-- Name: merchants id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.merchants ALTER COLUMN id SET DEFAULT nextval('public.merchants_id_seq'::regclass);


--
-- Name: moderator_checklist_items id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.moderator_checklist_items ALTER COLUMN id SET DEFAULT nextval('public.moderator_checklist_item_id_seq'::regclass);


--
-- Name: moderator_checklist_logs id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.moderator_checklist_logs ALTER COLUMN id SET DEFAULT nextval('public.moderator_checklist_log_id_seq'::regclass);


--
-- Name: monthly_reports id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.monthly_reports ALTER COLUMN id SET DEFAULT nextval('public.monthly_reports_id_seq'::regclass);


--
-- Name: offers id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offers ALTER COLUMN id SET DEFAULT nextval('public.offers_id_seq'::regclass);


--
-- Name: payment_method_groups id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.payment_method_groups ALTER COLUMN id SET DEFAULT nextval('public.payment_method_groups_id_seq'::regclass);


--
-- Name: payment_method_stats id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.payment_method_stats ALTER COLUMN id SET DEFAULT nextval('public.payment_method_stats_id_seq'::regclass);


--
-- Name: payment_methods id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.payment_methods ALTER COLUMN id SET DEFAULT nextval('public.payment_methods_id_seq'::regclass);


--
-- Name: popular_payment_methods id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.popular_payment_methods ALTER COLUMN id SET DEFAULT nextval('public.popular_payment_methods_id_seq'::regclass);


--
-- Name: price_equations id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.price_equations ALTER COLUMN id SET DEFAULT nextval('public.price_equations_id_seq'::regclass);


--
-- Name: referral_balances id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_balances ALTER COLUMN id SET DEFAULT nextval('public.vendor_balances_id_seq'::regclass);


--
-- Name: referral_payouts id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_payouts ALTER COLUMN id SET DEFAULT nextval('public.referral_payouts_id_seq'::regclass);


--
-- Name: referral_transactions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_transactions ALTER COLUMN id SET DEFAULT nextval('public.vendor_transactions_id_seq'::regclass);


--
-- Name: referrals id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referrals ALTER COLUMN id SET DEFAULT nextval('public.vendor_referrals_id_seq'::regclass);


--
-- Name: salesman_lead_connections id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.salesman_lead_connections ALTER COLUMN id SET DEFAULT nextval('public.salesman_lead_connections_id_seq'::regclass);


--
-- Name: sds_perfomance id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.sds_perfomance ALTER COLUMN id SET DEFAULT nextval('public.sds_perfomance_id_seq'::regclass);


--
-- Name: security_questions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.security_questions ALTER COLUMN id SET DEFAULT nextval('public.security_questions_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: slow_query_reports id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.slow_query_reports ALTER COLUMN id SET DEFAULT nextval('public.slow_query_reports_id_seq'::regclass);


--
-- Name: survey_questions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.survey_questions ALTER COLUMN id SET DEFAULT nextval('public.survey_questions_id_seq'::regclass);


--
-- Name: tags id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.tags ALTER COLUMN id SET DEFAULT nextval('public.tags_id_seq'::regclass);


--
-- Name: trade_fees id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.trade_fees ALTER COLUMN id SET DEFAULT nextval('public.trade_fees_id_seq'::regclass);


--
-- Name: transactions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.transactions ALTER COLUMN id SET DEFAULT nextval('public.transactions_id_seq'::regclass);


--
-- Name: transactions_failed id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.transactions_failed ALTER COLUMN id SET DEFAULT nextval('public.transactions_failed_id_seq'::regclass);


--
-- Name: transactions_prepared id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.transactions_prepared ALTER COLUMN id SET DEFAULT nextval('public.transactions_prepared_id_seq'::regclass);


--
-- Name: two_fa_reset_tokens id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.two_fa_reset_tokens ALTER COLUMN id SET DEFAULT nextval('public.two_fa_reset_tokens_id_seq'::regclass);


--
-- Name: unconfirmed_transactions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.unconfirmed_transactions ALTER COLUMN id SET DEFAULT nextval('public.unconfirmed_transactions_id_seq'::regclass);


--
-- Name: user_account_delete_reasons id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_account_delete_reasons ALTER COLUMN id SET DEFAULT nextval('public.user_account_delete_reasons_id_seq'::regclass);


--
-- Name: user_account_delete_requests id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_account_delete_requests ALTER COLUMN id SET DEFAULT nextval('public.user_account_delete_requests_id_seq'::regclass);


--
-- Name: user_accounts id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_accounts ALTER COLUMN id SET DEFAULT nextval('public.user_accounts_id_seq'::regclass);


--
-- Name: user_accounts_deleted id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_accounts_deleted ALTER COLUMN id SET DEFAULT nextval('public.user_accounts_deleted_id_seq'::regclass);


--
-- Name: user_addresses id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_addresses ALTER COLUMN id SET DEFAULT nextval('public.user_addresses_id_seq'::regclass);


--
-- Name: user_api_permissions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_api_permissions ALTER COLUMN id SET DEFAULT nextval('public.user_api_permissions_id_seq'::regclass);


--
-- Name: user_apis id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_apis ALTER COLUMN id SET DEFAULT nextval('public.user_apis_id_seq'::regclass);


--
-- Name: user_badges id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_badges ALTER COLUMN id SET DEFAULT nextval('public.user_badges_id_seq'::regclass);


--
-- Name: user_crypto_addresses id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_crypto_addresses ALTER COLUMN id SET DEFAULT nextval('public.user_crypto_addresses_id_seq'::regclass);


--
-- Name: user_crypto_balances id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_crypto_balances ALTER COLUMN id SET DEFAULT nextval('public.user_crypto_balances_id_seq'::regclass);


--
-- Name: user_devices id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_devices ALTER COLUMN id SET DEFAULT nextval('public.user_devices_id_seq'::regclass);


--
-- Name: user_emails_blacklist id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_emails_blacklist ALTER COLUMN id SET DEFAULT nextval('public.user_emails_blacklist_id_seq'::regclass);


--
-- Name: user_feature_flags id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_feature_flags ALTER COLUMN id SET DEFAULT nextval('public.user_feature_flags_id_seq'::regclass);


--
-- Name: user_freeway_histories id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories ALTER COLUMN id SET DEFAULT nextval('public.user_freeway_histories_id_seq'::regclass);


--
-- Name: user_relations id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_relations ALTER COLUMN id SET DEFAULT nextval('public.user_relations_id_seq'::regclass);


--
-- Name: user_reports id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_reports ALTER COLUMN id SET DEFAULT nextval('public.user_reports_id_seq'::regclass);


--
-- Name: user_reports_attachments id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_reports_attachments ALTER COLUMN id SET DEFAULT nextval('public.user_reports_attachments_id_seq'::regclass);


--
-- Name: user_segments id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_segments ALTER COLUMN id SET DEFAULT nextval('public.user_segments_id_seq'::regclass);


--
-- Name: user_sessions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_sessions ALTER COLUMN id SET DEFAULT nextval('public.user_sessions_id_seq'::regclass);


--
-- Name: user_settings id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_settings ALTER COLUMN id SET DEFAULT nextval('public.user_settings_id_seq'::regclass);


--
-- Name: user_site_messages id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_site_messages ALTER COLUMN id SET DEFAULT nextval('public.user_site_messages_id_seq'::regclass);


--
-- Name: user_stats id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_stats ALTER COLUMN id SET DEFAULT nextval('public.user_stats_id_seq'::regclass);


--
-- Name: user_status_actions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_status_actions ALTER COLUMN id SET DEFAULT nextval('public.user_status_actions_id_seq'::regclass);


--
-- Name: user_transactions id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_transactions ALTER COLUMN id SET DEFAULT nextval('public.user_transactions_id_seq'::regclass);


--
-- Name: user_verifications id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_verifications ALTER COLUMN id SET DEFAULT nextval('public.user_verifications_id_seq'::regclass);


--
-- Name: vendors id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.vendors ALTER COLUMN id SET DEFAULT nextval('public.vendors_id_seq'::regclass);


--
-- Name: visits id; Type: DEFAULT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.visits ALTER COLUMN id SET DEFAULT nextval('public.visits_id_seq'::regclass);


--
-- Name: admin_logs admin_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.admin_logs
    ADD CONSTRAINT admin_logs_pkey PRIMARY KEY (id);


--
-- Name: affiliate_balances affiliate_balances_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_balances
    ADD CONSTRAINT affiliate_balances_pkey PRIMARY KEY (id);


--
-- Name: affiliate_skins affiliate_skins_affiliate_id_template_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_skins
    ADD CONSTRAINT affiliate_skins_affiliate_id_template_unique UNIQUE (affiliate_id, template);


--
-- Name: affiliate_skins affiliate_skins_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_skins
    ADD CONSTRAINT affiliate_skins_pkey PRIMARY KEY (id);


--
-- Name: affiliate_transactions affiliate_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_transactions
    ADD CONSTRAINT affiliate_transactions_pkey PRIMARY KEY (id);


--
-- Name: affiliate_transactions affiliate_transactions_trade_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_transactions
    ADD CONSTRAINT affiliate_transactions_trade_id_unique UNIQUE (trade_id);


--
-- Name: affiliate_widgets affiliate_widgets_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_widgets
    ADD CONSTRAINT affiliate_widgets_pkey PRIMARY KEY (id);


--
-- Name: affiliates affiliates_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliates
    ADD CONSTRAINT affiliates_pkey PRIMARY KEY (id);


--
-- Name: affiliates affiliates_user_account_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliates
    ADD CONSTRAINT affiliates_user_account_id_unique UNIQUE (user_account_id, is_merchant);


--
-- Name: auth_logs auth_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.auth_logs
    ADD CONSTRAINT auth_logs_pkey PRIMARY KEY (id);


--
-- Name: bank_account_bank_networks bank_account_bank_networks_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_account_bank_networks
    ADD CONSTRAINT bank_account_bank_networks_pkey PRIMARY KEY (id);


--
-- Name: bank_accounts bank_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_accounts
    ADD CONSTRAINT bank_accounts_pkey PRIMARY KEY (id);


--
-- Name: bank_networks bank_networks_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_networks
    ADD CONSTRAINT bank_networks_pkey PRIMARY KEY (id);


--
-- Name: bank_networks bank_networks_slug_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_networks
    ADD CONSTRAINT bank_networks_slug_unique UNIQUE (slug);


--
-- Name: banks banks_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.banks
    ADD CONSTRAINT banks_pkey PRIMARY KEY (id);


--
-- Name: banks banks_routing_number_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.banks
    ADD CONSTRAINT banks_routing_number_unique UNIQUE (routing_number);


--
-- Name: banks banks_swift_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.banks
    ADD CONSTRAINT banks_swift_unique UNIQUE (swift);


--
-- Name: callbacks callbacks_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.callbacks
    ADD CONSTRAINT callbacks_pkey PRIMARY KEY (id);


--
-- Name: chainalysis_requests chainalysis_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.chainalysis_requests
    ADD CONSTRAINT chainalysis_requests_pkey PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: cities cities_slug_country_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_slug_country_id_unique UNIQUE (slug, country_id);


--
-- Name: complyadvantage_searches complyadvantage_searches_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.complyadvantage_searches
    ADD CONSTRAINT complyadvantage_searches_pkey PRIMARY KEY (id);


--
-- Name: complyadvantage_tm_requests complyadvantage_tm_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.complyadvantage_tm_requests
    ADD CONSTRAINT complyadvantage_tm_requests_pkey PRIMARY KEY (id);


--
-- Name: countries countries_pkey1; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey1 PRIMARY KEY (id);


--
-- Name: countries countries_slug_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_slug_unique UNIQUE (slug);


--
-- Name: country_city_landings_offers country_city_landings_offers_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.country_city_landings_offers
    ADD CONSTRAINT country_city_landings_offers_pkey PRIMARY KEY (id);


--
-- Name: country_landings country_landings_country_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.country_landings
    ADD CONSTRAINT country_landings_country_id_unique UNIQUE (country_id);


--
-- Name: country_landings country_landings_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.country_landings
    ADD CONSTRAINT country_landings_pkey PRIMARY KEY (id);


--
-- Name: cron_job cron_job_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.cron_job
    ADD CONSTRAINT cron_job_pkey PRIMARY KEY (id);


--
-- Name: cron_manager cron_manager_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.cron_manager
    ADD CONSTRAINT cron_manager_pkey PRIMARY KEY (id);


--
-- Name: crypto_blacklist crypto_blacklist_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_blacklist
    ADD CONSTRAINT crypto_blacklist_pkey PRIMARY KEY (address);


--
-- Name: crypto_currencies crypto_currencies_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currencies
    ADD CONSTRAINT crypto_currencies_pkey PRIMARY KEY (id);


--
-- Name: crypto_currency_forks crypto_currency_forks_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks
    ADD CONSTRAINT crypto_currency_forks_pkey PRIMARY KEY (id);


--
-- Name: crypto_currency_forks_user_payouts crypto_currency_forks_user_payouts_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks_user_payouts
    ADD CONSTRAINT crypto_currency_forks_user_payouts_pkey PRIMARY KEY (id);


--
-- Name: crypto_trade_notes crypto_trade_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_notes
    ADD CONSTRAINT crypto_trade_notes_pkey PRIMARY KEY (id);


--
-- Name: crypto_trade_relations crypto_trade_relations_parent_id_child_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_relations
    ADD CONSTRAINT crypto_trade_relations_parent_id_child_id_unique UNIQUE (parent_id, child_id);


--
-- Name: crypto_trade_relations crypto_trade_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_relations
    ADD CONSTRAINT crypto_trade_relations_pkey PRIMARY KEY (id);


--
-- Name: crypto_trade_statuses crypto_trade_statuses_name_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_statuses
    ADD CONSTRAINT crypto_trade_statuses_name_unique UNIQUE (name);

ALTER TABLE ONLY public.crypto_trade_statuses REPLICA IDENTITY USING INDEX crypto_trade_statuses_name_unique;


--
-- Name: crypto_trade_surveys crypto_trade_surveys_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_surveys
    ADD CONSTRAINT crypto_trade_surveys_pkey PRIMARY KEY (trade_id);


--
-- Name: crypto_trades_active crypto_trades_active_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_pkey PRIMARY KEY (id);


--
-- Name: crypto_trades_completed crypto_trades_completed_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_pkey PRIMARY KEY (id);


--
-- Name: crypto_trades_details crypto_trades_details_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_details
    ADD CONSTRAINT crypto_trades_details_pkey PRIMARY KEY (id);


--
-- Name: crypto_trades_disputed crypto_trades_disputed_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_disputed
    ADD CONSTRAINT crypto_trades_disputed_pkey PRIMARY KEY (id);


--
-- Name: crypto_trades_escrow crypto_trades_escrow_trade_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_escrow
    ADD CONSTRAINT crypto_trades_escrow_trade_id_unique UNIQUE (trade_id);

ALTER TABLE ONLY public.crypto_trades_escrow REPLICA IDENTITY USING INDEX crypto_trades_escrow_trade_id_unique;


--
-- Name: crypto_trades_payment_details crypto_trades_payment_details_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_payment_details
    ADD CONSTRAINT crypto_trades_payment_details_pkey PRIMARY KEY (trade_id, field_key);


--
-- Name: crypto_trades_payment_requirements crypto_trades_payment_requirements_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_payment_requirements
    ADD CONSTRAINT crypto_trades_payment_requirements_pkey PRIMARY KEY (trade_id, requirement);


--
-- Name: dispute_award_reasons dispute_award_reasons_group_label_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_award_reasons
    ADD CONSTRAINT dispute_award_reasons_group_label_unique UNIQUE ("group", label);


--
-- Name: dispute_award_reasons dispute_award_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_award_reasons
    ADD CONSTRAINT dispute_award_reasons_pkey PRIMARY KEY (id);


--
-- Name: dispute_logs dispute_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_logs
    ADD CONSTRAINT dispute_logs_pkey PRIMARY KEY (id);


--
-- Name: dispute_reasons dispute_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_reasons
    ADD CONSTRAINT dispute_reasons_pkey PRIMARY KEY (id);


--
-- Name: email_actions email_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.email_actions
    ADD CONSTRAINT email_actions_pkey PRIMARY KEY (token);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: feedback feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (id);


--
-- Name: fiat_currencies fiat_currencies_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.fiat_currencies
    ADD CONSTRAINT fiat_currencies_pkey PRIMARY KEY (id);


--
-- Name: invoices invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT invoices_pkey PRIMARY KEY (id);


--
-- Name: jumio_verifications jumio_verifications_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.jumio_verifications
    ADD CONSTRAINT jumio_verifications_pkey PRIMARY KEY (id);


--
-- Name: kyc_verifications kyc_verifications_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.kyc_verifications
    ADD CONSTRAINT kyc_verifications_pkey PRIMARY KEY (id);


--
-- Name: languages languages_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (code);


--
-- Name: merchant_deposit_address merchant_deposit_address_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.merchant_deposit_address
    ADD CONSTRAINT merchant_deposit_address_pkey PRIMARY KEY (id);


--
-- Name: merchants merchants_name_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.merchants
    ADD CONSTRAINT merchants_name_unique UNIQUE (name);


--
-- Name: merchants merchants_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.merchants
    ADD CONSTRAINT merchants_pkey PRIMARY KEY (id);


--
-- Name: moderator_checklist_items moderator_checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.moderator_checklist_items
    ADD CONSTRAINT moderator_checklist_item_pkey PRIMARY KEY (id);


--
-- Name: moderator_checklist_logs moderator_checklist_log_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.moderator_checklist_logs
    ADD CONSTRAINT moderator_checklist_log_pkey PRIMARY KEY (id);


--
-- Name: monthly_reports monthly_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.monthly_reports
    ADD CONSTRAINT monthly_reports_pkey PRIMARY KEY (id);


--
-- Name: offer_payment_details offer_payment_details_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_payment_details
    ADD CONSTRAINT offer_payment_details_pkey PRIMARY KEY (offer_id, payment_method_id, field_key);


--
-- Name: offer_payment_requirements offer_payment_requirements_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_payment_requirements
    ADD CONSTRAINT offer_payment_requirements_pkey PRIMARY KEY (offer_id, requirement);


--
-- Name: offer_performance offer_performance_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_performance
    ADD CONSTRAINT offer_performance_pkey PRIMARY KEY (offer_id);


--
-- Name: offer_tags offer_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_tags
    ADD CONSTRAINT offer_tags_pkey PRIMARY KEY (offer_id, tag_id);


--
-- Name: offers offers_id_hashed_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_id_hashed_unique UNIQUE (id_hashed);


--
-- Name: offers offers_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_pkey PRIMARY KEY (id);


--
-- Name: payment_method_groups payment_method_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.payment_method_groups
    ADD CONSTRAINT payment_method_groups_pkey PRIMARY KEY (id);


--
-- Name: payment_method_stats payment_method_stats_fiat_currency_id_payment_method_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.payment_method_stats
    ADD CONSTRAINT payment_method_stats_fiat_currency_id_payment_method_id_unique UNIQUE (fiat_currency_id, payment_method_id);


--
-- Name: payment_method_stats payment_method_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.payment_method_stats
    ADD CONSTRAINT payment_method_stats_pkey PRIMARY KEY (id);


--
-- Name: payment_methods payment_methods_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.payment_methods
    ADD CONSTRAINT payment_methods_pkey PRIMARY KEY (id);


--
-- Name: phone_calls phone_calls_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.phone_calls
    ADD CONSTRAINT phone_calls_pkey PRIMARY KEY (hash_id);


--
-- Name: popular_payment_methods popular_payment_methods_payment_method_id_country_id_offer_type; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.popular_payment_methods
    ADD CONSTRAINT popular_payment_methods_payment_method_id_country_id_offer_type UNIQUE (payment_method_id, country_id, offer_type, type);


--
-- Name: popular_payment_methods popular_payment_methods_payment_method_id_payment_method_group_; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.popular_payment_methods
    ADD CONSTRAINT popular_payment_methods_payment_method_id_payment_method_group_ UNIQUE (payment_method_id, payment_method_group_id, offer_type, type);


--
-- Name: popular_payment_methods popular_payment_methods_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.popular_payment_methods
    ADD CONSTRAINT popular_payment_methods_pkey PRIMARY KEY (id);


--
-- Name: price_equations price_equations_datasource_key_datapoint_key_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.price_equations
    ADD CONSTRAINT price_equations_datasource_key_datapoint_key_unique UNIQUE (datasource_key, datapoint_key);


--
-- Name: price_equations price_equations_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.price_equations
    ADD CONSTRAINT price_equations_pkey PRIMARY KEY (id);


--
-- Name: referral_balances referral_balances_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_balances
    ADD CONSTRAINT referral_balances_pkey PRIMARY KEY (id);


--
-- Name: referral_payouts referral_payouts_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_payouts
    ADD CONSTRAINT referral_payouts_pkey PRIMARY KEY (id);


--
-- Name: referral_payouts referral_payouts_trade_id_buyer_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_payouts
    ADD CONSTRAINT referral_payouts_trade_id_buyer_id_unique UNIQUE (trade_id, buyer_id);


--
-- Name: referral_payouts referral_payouts_trade_id_seller_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_payouts
    ADD CONSTRAINT referral_payouts_trade_id_seller_id_unique UNIQUE (trade_id, seller_id);


--
-- Name: referral_transactions referral_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_transactions
    ADD CONSTRAINT referral_transactions_pkey PRIMARY KEY (id);


--
-- Name: referral_transactions referral_transactions_user_trade_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_transactions
    ADD CONSTRAINT referral_transactions_user_trade_unique UNIQUE (user_account_id, trade_id, referral_user_account_id, initial_referral_user_account_id);


--
-- Name: referrals referrals_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_pkey PRIMARY KEY (id);


--
-- Name: referrals referrals_referral_user_account_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_referral_user_account_id_unique UNIQUE (referral_user_account_id);


--
-- Name: salesman_lead_connections salesman_lead_connections_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.salesman_lead_connections
    ADD CONSTRAINT salesman_lead_connections_pkey PRIMARY KEY (id);


--
-- Name: sds_perfomance sds_perfomance_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.sds_perfomance
    ADD CONSTRAINT sds_perfomance_pkey PRIMARY KEY (id);


--
-- Name: security_questions security_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.security_questions
    ADD CONSTRAINT security_questions_pkey PRIMARY KEY (id);


--
-- Name: settings settings_key_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_key_unique UNIQUE (key);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: slow_query_reports slow_query_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.slow_query_reports
    ADD CONSTRAINT slow_query_reports_pkey PRIMARY KEY (id);


--
-- Name: survey_questions survey_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.survey_questions
    ADD CONSTRAINT survey_questions_pkey PRIMARY KEY (id);


--
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: tags tags_slug_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_slug_unique UNIQUE (slug);


--
-- Name: trade_fees trade_fees_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.trade_fees
    ADD CONSTRAINT trade_fees_pkey PRIMARY KEY (id);


--
-- Name: transactions_failed transactions_failed_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.transactions_failed
    ADD CONSTRAINT transactions_failed_pkey PRIMARY KEY (id);


--
-- Name: transactions transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);


--
-- Name: transactions_prepared transactions_prepared_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.transactions_prepared
    ADD CONSTRAINT transactions_prepared_pkey PRIMARY KEY (id);


--
-- Name: two_fa_reset_tokens two_fa_reset_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.two_fa_reset_tokens
    ADD CONSTRAINT two_fa_reset_tokens_pkey PRIMARY KEY (id);


--
-- Name: unconfirmed_transactions unconfirmed_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.unconfirmed_transactions
    ADD CONSTRAINT unconfirmed_transactions_pkey PRIMARY KEY (id);


--
-- Name: user_account_delete_reasons user_account_delete_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_account_delete_reasons
    ADD CONSTRAINT user_account_delete_reasons_pkey PRIMARY KEY (id);


--
-- Name: user_account_delete_requests user_account_delete_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_account_delete_requests
    ADD CONSTRAINT user_account_delete_requests_pkey PRIMARY KEY (id);


--
-- Name: user_segments user_account_segment_pk; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_segments
    ADD CONSTRAINT user_account_segment_pk UNIQUE (user_account_id, segment_id);


--
-- Name: user_accounts_deleted user_accounts_deleted_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_accounts_deleted
    ADD CONSTRAINT user_accounts_deleted_pkey PRIMARY KEY (id);


--
-- Name: user_accounts user_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_accounts
    ADD CONSTRAINT user_accounts_pkey PRIMARY KEY (id);


--
-- Name: user_addresses user_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_addresses
    ADD CONSTRAINT user_addresses_pkey PRIMARY KEY (id);


--
-- Name: user_api_permissions user_api_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_api_permissions
    ADD CONSTRAINT user_api_permissions_pkey PRIMARY KEY (id);


--
-- Name: user_apis user_apis_key_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_apis
    ADD CONSTRAINT user_apis_key_unique UNIQUE (key);


--
-- Name: user_apis user_apis_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_apis
    ADD CONSTRAINT user_apis_pkey PRIMARY KEY (id);


--
-- Name: user_badges user_badges_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_badges
    ADD CONSTRAINT user_badges_pkey PRIMARY KEY (id);


--
-- Name: user_compliance user_compliance_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_compliance
    ADD CONSTRAINT user_compliance_pkey PRIMARY KEY (user_account_id);


--
-- Name: user_crypto_addresses user_crypto_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_crypto_addresses
    ADD CONSTRAINT user_crypto_addresses_pkey PRIMARY KEY (id);


--
-- Name: user_crypto_balances user_crypto_balances_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_crypto_balances
    ADD CONSTRAINT user_crypto_balances_pkey PRIMARY KEY (id);


--
-- Name: user_devices user_devices_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_devices
    ADD CONSTRAINT user_devices_pkey PRIMARY KEY (id);


--
-- Name: user_emails_blacklist user_emails_blacklist_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_emails_blacklist
    ADD CONSTRAINT user_emails_blacklist_pkey PRIMARY KEY (id);


--
-- Name: user_feature_flags user_feature_flags_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_feature_flags
    ADD CONSTRAINT user_feature_flags_pkey PRIMARY KEY (id);


--
-- Name: user_feature_flags user_feature_flags_user_account_id_key_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_feature_flags
    ADD CONSTRAINT user_feature_flags_user_account_id_key_unique UNIQUE (user_account_id, key);


--
-- Name: user_freeway_histories user_freeway_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_pkey PRIMARY KEY (id);


--
-- Name: user_freeway_histories user_freeway_histories_user_account_id_unique; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_user_account_id_unique UNIQUE (user_account_id);


--
-- Name: user_ip_history user_ip_history_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_ip_history
    ADD CONSTRAINT user_ip_history_pkey PRIMARY KEY (user_account_id, ip);


--
-- Name: user_issues user_issues_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_issues
    ADD CONSTRAINT user_issues_pkey PRIMARY KEY (user_account_id);


--
-- Name: user_languages user_languages_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_languages
    ADD CONSTRAINT user_languages_pkey PRIMARY KEY (user_account_id, language_code);


--
-- Name: user_notifications_settings user_notifications_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_notifications_settings
    ADD CONSTRAINT user_notifications_settings_pkey PRIMARY KEY (user_account_id);


--
-- Name: export_reports user_pdf_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.export_reports
    ADD CONSTRAINT user_pdf_reports_pkey PRIMARY KEY (id);


--
-- Name: user_relations user_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_relations
    ADD CONSTRAINT user_relations_pkey PRIMARY KEY (id);


--
-- Name: user_reports_attachments user_reports_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_reports_attachments
    ADD CONSTRAINT user_reports_attachments_pkey PRIMARY KEY (id);


--
-- Name: user_reports user_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_reports
    ADD CONSTRAINT user_reports_pkey PRIMARY KEY (id);


--
-- Name: user_security_questions user_security_questions_user_account_id_security_question_id_un; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_security_questions
    ADD CONSTRAINT user_security_questions_user_account_id_security_question_id_un UNIQUE (user_account_id, security_question_id);


--
-- Name: user_segments user_segments_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_segments
    ADD CONSTRAINT user_segments_pkey PRIMARY KEY (id);


--
-- Name: user_sessions user_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_sessions
    ADD CONSTRAINT user_sessions_pkey PRIMARY KEY (id);


--
-- Name: user_settings user_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_settings
    ADD CONSTRAINT user_settings_pkey PRIMARY KEY (id);


--
-- Name: user_site_messages user_site_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_site_messages
    ADD CONSTRAINT user_site_messages_pkey PRIMARY KEY (id);


--
-- Name: user_stats user_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_stats
    ADD CONSTRAINT user_stats_pkey PRIMARY KEY (id);


--
-- Name: user_status_actions user_status_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_status_actions
    ADD CONSTRAINT user_status_actions_pkey PRIMARY KEY (id);


--
-- Name: user_transactions user_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_transactions
    ADD CONSTRAINT user_transactions_pkey PRIMARY KEY (id);


--
-- Name: user_verifications user_verifications_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_verifications
    ADD CONSTRAINT user_verifications_pkey PRIMARY KEY (id);


--
-- Name: vendors vendors_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.vendors
    ADD CONSTRAINT vendors_pkey PRIMARY KEY (id);


--
-- Name: visits visits_pkey; Type: CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.visits
    ADD CONSTRAINT visits_pkey PRIMARY KEY (id);


--
-- Name: admin_logs_action_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX admin_logs_action_index ON public.admin_logs USING btree (action);


--
-- Name: admin_logs_created_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX admin_logs_created_at_index ON public.admin_logs USING btree (created_at);


--
-- Name: affiliate_balances_affiliate_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_balances_affiliate_id_index ON public.affiliate_balances USING btree (affiliate_id);


--
-- Name: affiliate_balances_crypto_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_balances_crypto_currency_id_index ON public.affiliate_balances USING btree (crypto_currency_id);


--
-- Name: affiliate_skins_affiliate_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_skins_affiliate_id_index ON public.affiliate_skins USING btree (affiliate_id);


--
-- Name: affiliate_skins_template_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_skins_template_index ON public.affiliate_skins USING btree (template);


--
-- Name: affiliate_transactions_affiliate_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_transactions_affiliate_id_index ON public.affiliate_transactions USING btree (affiliate_id);


--
-- Name: affiliate_transactions_affiliate_widget_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_transactions_affiliate_widget_id_index ON public.affiliate_transactions USING btree (affiliate_widget_id);


--
-- Name: affiliate_transactions_crypto_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_transactions_crypto_currency_id_index ON public.affiliate_transactions USING btree (crypto_currency_id);


--
-- Name: affiliate_transactions_type_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_transactions_type_id_index ON public.affiliate_transactions USING btree (type_id);


--
-- Name: affiliate_widgets_affiliate_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliate_widgets_affiliate_id_index ON public.affiliate_widgets USING btree (affiliate_id);


--
-- Name: affiliates_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX affiliates_user_account_id_index ON public.affiliates USING btree (user_account_id);


--
-- Name: auth_logs_login_type; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX auth_logs_login_type ON public.auth_logs USING btree (user_account_id, ip, user_agent);


--
-- Name: auth_logs_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX auth_logs_user_account_id_index ON public.auth_logs USING btree (user_account_id);


--
-- Name: bank_account_bank_networks_bank_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX bank_account_bank_networks_bank_account_id_index ON public.bank_account_bank_networks USING btree (bank_account_id);


--
-- Name: bank_account_bank_networks_bank_network_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX bank_account_bank_networks_bank_network_id_index ON public.bank_account_bank_networks USING btree (bank_network_id);


--
-- Name: bank_accounts_bank_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX bank_accounts_bank_id_index ON public.bank_accounts USING btree (bank_id);


--
-- Name: bank_accounts_fiat_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX bank_accounts_fiat_currency_id_index ON public.bank_accounts USING btree (fiat_currency_id);


--
-- Name: bank_accounts_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX bank_accounts_user_account_id_index ON public.bank_accounts USING btree (user_account_id);


--
-- Name: banks_country_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX banks_country_id_index ON public.banks USING btree (country_id);


--
-- Name: callbacks_status_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX callbacks_status_index ON public.callbacks USING btree (status);


--
-- Name: cities_country_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX cities_country_id_index ON public.cities USING btree (country_id);


--
-- Name: cities_country_iso_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX cities_country_iso_index ON public.cities USING btree (country_iso);


--
-- Name: cities_lat_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX cities_lat_index ON public.cities USING btree (lat);


--
-- Name: cities_lon_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX cities_lon_index ON public.cities USING btree (lon);


--
-- Name: cities_name_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX cities_name_index ON public.cities USING btree (name);


--
-- Name: city_name_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX city_name_idx ON public.locations USING btree (city_name);


--
-- Name: complyadvantage_searches_search_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX complyadvantage_searches_search_id_index ON public.complyadvantage_searches USING btree (search_id);


--
-- Name: country_city_landings_offers_country_id; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX country_city_landings_offers_country_id ON public.country_city_landings_offers USING btree (country_id) WHERE (country_id IS NOT NULL);


--
-- Name: country_city_landings_offers_fiat_currency_id_payment_method_id; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX country_city_landings_offers_fiat_currency_id_payment_method_id ON public.country_city_landings_offers USING btree (fiat_currency_id, payment_method_id) WHERE ((fiat_currency_id IS NOT NULL) AND (payment_method_id IS NOT NULL));


--
-- Name: country_iso_code_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX country_iso_code_idx ON public.locations USING btree (country_iso_code);


--
-- Name: country_name_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX country_name_idx ON public.locations USING btree (country_name);


--
-- Name: cron_job_name_cron_manager_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX cron_job_name_cron_manager_id_index ON public.cron_job USING btree (name, cron_manager_id);


--
-- Name: crypto_trade_details_trg_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trade_details_trg_index ON public.crypto_trades_details USING gist (phash_original public.gist_trgm_ops);


--
-- Name: crypto_trade_notes_author_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trade_notes_author_id_index ON public.crypto_trade_notes USING btree (author_id);


--
-- Name: crypto_trade_notes_trade_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trade_notes_trade_id_index ON public.crypto_trade_notes USING btree (trade_id);


--
-- Name: crypto_trade_notes_type_trade_id_target_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trade_notes_type_trade_id_target_id_index ON public.crypto_trade_notes USING btree (type, trade_id, target_id);


--
-- Name: crypto_trade_statuses_status_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trade_statuses_status_id_index ON public.crypto_trade_statuses USING btree (status_id);


--
-- Name: crypto_trades_active_buyer_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_buyer_id_index ON public.crypto_trades_active USING btree (buyer_id);


--
-- Name: crypto_trades_active_created_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_created_at_index ON public.crypto_trades_active USING btree (created_at);


--
-- Name: crypto_trades_active_created_at_offer_owner_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_created_at_offer_owner_id_idx ON public.crypto_trades_active USING btree (created_at, offer_owner_id);


--
-- Name: crypto_trades_active_created_at_offer_owner_id_offer_respon_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_created_at_offer_owner_id_offer_respon_idx ON public.crypto_trades_active USING btree (created_at, offer_owner_id, offer_responder_id);


--
-- Name: crypto_trades_active_created_at_offer_responder_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_created_at_offer_responder_id_idx ON public.crypto_trades_active USING btree (created_at, offer_responder_id);


--
-- Name: crypto_trades_active_crypto_trade_status_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_crypto_trade_status_id_index ON public.crypto_trades_active USING btree (crypto_trade_status_id);


--
-- Name: crypto_trades_active_fiat_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_fiat_currency_id_index ON public.crypto_trades_active USING btree (fiat_currency_id);


--
-- Name: crypto_trades_active_id_status_id; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_id_status_id ON public.crypto_trades_active USING btree (id, crypto_trade_status_id);


--
-- Name: crypto_trades_active_offer_owner_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_offer_owner_id_index ON public.crypto_trades_active USING btree (offer_owner_id);


--
-- Name: crypto_trades_active_offer_responder_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_offer_responder_id_index ON public.crypto_trades_active USING btree (offer_responder_id);


--
-- Name: crypto_trades_active_offer_type_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_offer_type_index ON public.crypto_trades_active USING btree (offer_type);


--
-- Name: crypto_trades_active_payment_method_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_payment_method_id_index ON public.crypto_trades_active USING btree (payment_method_id);


--
-- Name: crypto_trades_active_responder_ip_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_responder_ip_index ON public.crypto_trades_active USING btree (responder_ip);


--
-- Name: crypto_trades_active_seller_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_active_seller_id_index ON public.crypto_trades_active USING btree (seller_id);


--
-- Name: crypto_trades_completed_affiliate_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_affiliate_id_index ON public.crypto_trades_completed USING btree (affiliate_id);


--
-- Name: crypto_trades_completed_buyer_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_buyer_id_index ON public.crypto_trades_completed USING btree (buyer_id);


--
-- Name: crypto_trades_completed_completed_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_completed_at_index ON public.crypto_trades_completed USING btree (completed_at);


--
-- Name: crypto_trades_completed_created_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_created_at_index ON public.crypto_trades_completed USING btree (created_at);


--
-- Name: crypto_trades_completed_created_at_seller_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_created_at_seller_id_idx ON public.crypto_trades_completed USING btree (created_at, seller_id);


--
-- Name: crypto_trades_completed_crypto_trade_status_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_crypto_trade_status_id_index ON public.crypto_trades_completed USING btree (crypto_trade_status_id);


--
-- Name: crypto_trades_completed_fiat_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_fiat_currency_id_index ON public.crypto_trades_completed USING btree (fiat_currency_id);


--
-- Name: crypto_trades_completed_id_created_at_completed_at_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_id_created_at_completed_at_idx ON public.crypto_trades_completed USING btree (id, created_at, completed_at);


--
-- Name: crypto_trades_completed_invoice_uuid_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_invoice_uuid_index ON public.crypto_trades_completed USING btree (invoice_uuid) WHERE (invoice_uuid IS NOT NULL);


--
-- Name: crypto_trades_completed_jj_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_jj_idx ON public.crypto_trades_completed USING btree (payment_method_id, seller_id);


--
-- Name: crypto_trades_completed_offer_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_offer_id_index ON public.crypto_trades_completed USING btree (offer_id);


--
-- Name: crypto_trades_completed_offer_owner_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_offer_owner_id_index ON public.crypto_trades_completed USING btree (offer_owner_id);


--
-- Name: crypto_trades_completed_offer_responder_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_offer_responder_id_index ON public.crypto_trades_completed USING btree (offer_responder_id);


--
-- Name: crypto_trades_completed_offer_type_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_offer_type_index ON public.crypto_trades_completed USING btree (offer_type);


--
-- Name: crypto_trades_completed_payment_method_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_payment_method_id_idx ON public.crypto_trades_completed USING btree (payment_method_id);


--
-- Name: crypto_trades_completed_seller_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_seller_id_index ON public.crypto_trades_completed USING btree (seller_id);


--
-- Name: crypto_trades_completed_seller_payment_method_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_seller_payment_method_idx ON public.crypto_trades_completed USING btree (seller_id, payment_method_id);


--
-- Name: crypto_trades_completed_source_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_source_id_index ON public.crypto_trades_completed USING btree (source_id);


--
-- Name: crypto_trades_completed_updated_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_completed_updated_at_index ON public.crypto_trades_completed USING btree (updated_at);


--
-- Name: crypto_trades_details_trade_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_details_trade_id_index ON public.crypto_trades_details USING btree (trade_id);


--
-- Name: crypto_trades_details_type_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_details_type_index ON public.crypto_trades_details USING btree (type);


--
-- Name: crypto_trades_disputed_crypto_trade_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_disputed_crypto_trade_id_index ON public.crypto_trades_disputed USING btree (crypto_trade_id);


--
-- Name: crypto_trades_disputed_dispute_started_by_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX crypto_trades_disputed_dispute_started_by_index ON public.crypto_trades_disputed USING btree (dispute_started_by);


--
-- Name: feedback_deleted_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX feedback_deleted_at_index ON public.feedback USING btree (deleted_at);


--
-- Name: feedback_feedback_leaver_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX feedback_feedback_leaver_id_index ON public.feedback USING btree (feedback_leaver_id);


--
-- Name: feedback_feedback_receiver_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX feedback_feedback_receiver_id_index ON public.feedback USING btree (feedback_receiver_id);


--
-- Name: feedback_offer_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX feedback_offer_id_idx ON public.feedback USING btree (offer_id);


--
-- Name: feedback_trade_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX feedback_trade_id_index ON public.feedback USING btree (trade_id);


--
-- Name: idx_fiat_currencies_code; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX idx_fiat_currencies_code ON public.fiat_currencies USING btree (code);


--
-- Name: idx_tree_columns; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX idx_tree_columns ON public.crypto_trades_completed USING btree (created_at, offer_type, crypto_trade_status_id);


--
-- Name: jumio_verifications_document_number_hash_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX jumio_verifications_document_number_hash_index ON public.jumio_verifications USING btree (document_number_hash);


--
-- Name: jumio_verifications_reference_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX jumio_verifications_reference_index ON public.jumio_verifications USING btree (reference);


--
-- Name: kyc_verification_status_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX kyc_verification_status_idx ON public.kyc_verifications USING btree (status);


--
-- Name: kyc_verification_type_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX kyc_verification_type_idx ON public.kyc_verifications USING btree (type);


--
-- Name: kyc_verifications_document_number_hash_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX kyc_verifications_document_number_hash_index ON public.kyc_verifications USING btree (document_number_hash);


--
-- Name: languages_supported_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX languages_supported_index ON public.languages USING btree (supported);


--
-- Name: locations_pk; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX locations_pk ON public.locations USING btree (id);


--
-- Name: merchant_deposit_address_merchant_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX merchant_deposit_address_merchant_id_index ON public.merchant_deposit_address USING btree (merchant_id);


--
-- Name: merchant_deposit_address_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX merchant_deposit_address_user_account_id_index ON public.merchant_deposit_address USING btree (user_account_id);


--
-- Name: merchants_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX merchants_user_account_id_index ON public.merchants USING btree (user_account_id);


--
-- Name: monthly_reports_end_date_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX monthly_reports_end_date_index ON public.monthly_reports USING btree (end_date);


--
-- Name: monthly_reports_start_date_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX monthly_reports_start_date_index ON public.monthly_reports USING btree (start_date);


--
-- Name: offer_tags_tag_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offer_tags_tag_id_idx ON public.offer_tags USING btree (tag_id);


--
-- Name: offers_active_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_active_index ON public.offers USING btree (active);


--
-- Name: offers_authorized_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_authorized_index ON public.offers USING btree (authorized);


--
-- Name: offers_country_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_country_id_index ON public.offers USING btree (country_id);


--
-- Name: offers_deleted_active_authorized_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_deleted_active_authorized_index ON public.offers USING btree (deleted_at, offer_type, payment_method_id) WHERE ((active = true) AND (authorized = true));


--
-- Name: offers_deleted_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_deleted_at_index ON public.offers USING btree (deleted_at);


--
-- Name: offers_fiat_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_fiat_currency_id_index ON public.offers USING btree (fiat_currency_id);


--
-- Name: offers_offer_type_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_offer_type_index ON public.offers USING btree (offer_type);


--
-- Name: offers_payment_method_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_payment_method_id_index ON public.offers USING btree (payment_method_id);


--
-- Name: offers_payment_method_id_user_account_id_fiat_currency_id_activ; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_payment_method_id_user_account_id_fiat_currency_id_activ ON public.offers USING btree (payment_method_id, user_account_id, fiat_currency_id, active);


--
-- Name: offers_payment_user_active; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_payment_user_active ON public.offers USING btree (payment_method_id, user_account_id, active);


--
-- Name: offers_price_datasource_key_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_price_datasource_key_index ON public.offers USING btree (price_datasource_key);


--
-- Name: offers_require_verified_email_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_require_verified_email_index ON public.offers USING btree (require_verified_email);


--
-- Name: offers_require_verified_phone_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_require_verified_phone_index ON public.offers USING btree (require_verified_phone);


--
-- Name: offers_show_only_trusted_user_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_show_only_trusted_user_index ON public.offers USING btree (show_only_trusted_user);


--
-- Name: offers_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX offers_user_account_id_index ON public.offers USING btree (user_account_id);


--
-- Name: password_reminders_email_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX password_reminders_email_index ON public.password_reminders USING btree (email);


--
-- Name: password_reminders_token_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX password_reminders_token_index ON public.password_reminders USING btree (token);


--
-- Name: payment_method_deleted_at_null_approved_true_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX payment_method_deleted_at_null_approved_true_idx ON public.payment_methods USING btree (deleted_at, approved) WHERE ((deleted_at IS NULL) AND (approved = true));


--
-- Name: payment_methods_deleted_approved_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX payment_methods_deleted_approved_index ON public.payment_methods USING btree (deleted_at, sell_quantity DESC, id, buy_quantity, name, slug, payment_method_group_id, release_time, is_copyrighted) WHERE (approved = true);


--
-- Name: payment_methods_group_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX payment_methods_group_id_idx ON public.payment_methods USING btree (payment_method_group_id);


--
-- Name: phone_calls_call_received_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX phone_calls_call_received_index ON public.phone_calls USING btree (call_received);


--
-- Name: phone_calls_to_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX phone_calls_to_index ON public.phone_calls USING btree ("to");


--
-- Name: promocode_transactions_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX promocode_transactions_at_index ON public.transactions USING btree (sent_to_address) WHERE ((order_code)::text = 'promo-postcard'::text);


--
-- Name: referral_balances_crypto_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX referral_balances_crypto_currency_id_index ON public.referral_balances USING btree (crypto_currency_id);


--
-- Name: referral_balances_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX referral_balances_user_account_id_index ON public.referral_balances USING btree (user_account_id);


--
-- Name: referral_transactions_crypto_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX referral_transactions_crypto_currency_id_index ON public.referral_transactions USING btree (crypto_currency_id);


--
-- Name: referral_transactions_referral_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX referral_transactions_referral_user_account_id_index ON public.referral_transactions USING btree (referral_user_account_id);


--
-- Name: referral_transactions_type_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX referral_transactions_type_id_index ON public.referral_transactions USING btree (type_id);


--
-- Name: referral_transactions_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX referral_transactions_user_account_id_index ON public.referral_transactions USING btree (user_account_id);


--
-- Name: referrals_track_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX referrals_track_index ON public.referrals USING hash (track);


--
-- Name: referrals_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX referrals_user_account_id_index ON public.referrals USING btree (user_account_id);


--
-- Name: salesman_lead_connections_lead_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX salesman_lead_connections_lead_id_index ON public.salesman_lead_connections USING btree (lead_id);


--
-- Name: salesman_lead_connections_salesman_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX salesman_lead_connections_salesman_id_index ON public.salesman_lead_connections USING btree (salesman_id);


--
-- Name: slow_query_reports_type_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX slow_query_reports_type_index ON public.slow_query_reports USING btree (type);


--
-- Name: slug_unique_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX slug_unique_index ON public.payment_methods USING btree (slug);


--
-- Name: subdivision_name_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX subdivision_name_idx ON public.locations USING btree (subdivision_name);


--
-- Name: survey_questions_active_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX survey_questions_active_index ON public.survey_questions USING btree (active);


--
-- Name: target_user_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX target_user_id_idx ON public.admin_logs USING btree (target_user_id);


--
-- Name: trade_fees_crypto_currency_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX trade_fees_crypto_currency_id_index ON public.trade_fees USING btree (crypto_currency_id);


--
-- Name: trade_fees_trade_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX trade_fees_trade_id_index ON public.trade_fees USING btree (trade_id);


--
-- Name: transactions_prepared_batch_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX transactions_prepared_batch_id_index ON public.transactions_prepared USING btree (batch_id);


--
-- Name: transactions_prepared_trade_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX transactions_prepared_trade_id_index ON public.transactions_prepared USING btree (trade_id);


--
-- Name: transactions_prepared_transaction_hash_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX transactions_prepared_transaction_hash_index ON public.transactions_prepared USING btree (transaction_hash);


--
-- Name: transactions_prepared_user_account_id; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX transactions_prepared_user_account_id ON public.transactions_prepared USING btree (user_account_id);


--
-- Name: transactions_transaction_hash_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX transactions_transaction_hash_index ON public.transactions USING btree (transaction_hash);


--
-- Name: unconfirmed_transactions_anti_race_condition_new_unique_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX unconfirmed_transactions_anti_race_condition_new_unique_index ON public.unconfirmed_transactions USING btree (receiving_address, transaction_hash, crypto_amount, "position");


--
-- Name: unconfirmed_transactions_receiving_address_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX unconfirmed_transactions_receiving_address_index ON public.unconfirmed_transactions USING btree (receiving_address);


--
-- Name: unconfirmed_transactions_transaction_hash_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX unconfirmed_transactions_transaction_hash_index ON public.unconfirmed_transactions USING btree (transaction_hash);


--
-- Name: unconfirmed_transactions_user_account_id; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX unconfirmed_transactions_user_account_id ON public.unconfirmed_transactions USING btree (user_account_id);


--
-- Name: user_account_delete_requests_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_account_delete_requests_user_account_id_index ON public.user_account_delete_requests USING btree (user_account_id);


--
-- Name: user_account_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_account_id_idx ON public.email_actions USING btree (user_account_id);


--
-- Name: user_accounts_compromised_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_accounts_compromised_index ON public.user_accounts USING btree (compromised);


--
-- Name: user_accounts_deleted_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_accounts_deleted_user_account_id_index ON public.user_accounts_deleted USING btree (user_account_id);


--
-- Name: user_accounts_email_2_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_accounts_email_2_index ON public.user_accounts USING btree (email);


--
-- Name: user_accounts_email_rormalized_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_accounts_email_rormalized_index ON public.user_accounts USING btree (lower((email_normalized)::text));


--
-- Name: user_accounts_email_rormalized_index2; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_accounts_email_rormalized_index2 ON public.user_accounts USING btree (email_normalized);


--
-- Name: user_accounts_email_unique_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX user_accounts_email_unique_idx ON public.user_accounts USING btree (lower((email)::text));


--
-- Name: user_accounts_token_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX user_accounts_token_idx ON public.user_accounts USING btree (activation_token);


--
-- Name: user_accounts_username_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX user_accounts_username_index ON public.user_accounts USING btree (lower((username)::text));


--
-- Name: user_api_permissions_user_api_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_api_permissions_user_api_id_index ON public.user_api_permissions USING btree (user_api_id);


--
-- Name: user_apis_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_apis_user_account_id_index ON public.user_apis USING btree (user_account_id);


--
-- Name: user_badges_user_account_id_type_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_badges_user_account_id_type_id_idx ON public.user_badges USING btree (user_account_id, user_badge_type_id);


--
-- Name: user_compliance_document_verification_status_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_compliance_document_verification_status_index ON public.user_compliance USING btree (document_verification_status);


--
-- Name: user_compliance_id_verification_status_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_compliance_id_verification_status_index ON public.user_compliance USING btree (id_verification_status);


--
-- Name: user_crypto_address_address_unique; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX user_crypto_address_address_unique ON public.user_crypto_addresses USING btree (crypto_currency_id, address) WHERE (address IS NOT NULL);


--
-- Name: user_crypto_addresses_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_crypto_addresses_user_account_id_index ON public.user_crypto_addresses USING btree (user_account_id);


--
-- Name: user_crypto_balances_updated_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_crypto_balances_updated_at_index ON public.user_crypto_balances USING btree (updated_at);


--
-- Name: user_crypto_balances_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_crypto_balances_user_account_id_index ON public.user_crypto_balances USING btree (user_account_id);


--
-- Name: user_devices_uuid_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_devices_uuid_idx ON public.user_devices USING btree (uuid);


--
-- Name: user_emails_blacklist_admin_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_emails_blacklist_admin_account_id_index ON public.user_emails_blacklist USING btree (admin_account_id);


--
-- Name: user_feature_flags_key_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_feature_flags_key_index ON public.user_feature_flags USING btree (key);


--
-- Name: user_feature_flags_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_feature_flags_user_account_id_index ON public.user_feature_flags USING btree (user_account_id);


--
-- Name: user_freeway_histories_affiliate_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_freeway_histories_affiliate_id_index ON public.user_freeway_histories USING btree (affiliate_id);


--
-- Name: user_freeway_histories_trade_id_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE UNIQUE INDEX user_freeway_histories_trade_id_idx ON public.user_freeway_histories USING btree (trade_id);


--
-- Name: user_ip_history_ip_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_ip_history_ip_index ON public.user_ip_history USING btree (ip);


--
-- Name: user_ip_history_updated_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_ip_history_updated_at_index ON public.user_ip_history USING btree (updated_at DESC);


--
-- Name: user_notifications_settings_mobile_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_notifications_settings_mobile_index ON public.user_notifications_settings USING gin (mobile);


--
-- Name: user_pdf_reports_admin_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_pdf_reports_admin_account_id_index ON public.export_reports USING btree (admin_account_id);


--
-- Name: user_pdf_reports_completed_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_pdf_reports_completed_at_index ON public.export_reports USING btree (completed_at);


--
-- Name: user_pdf_reports_started_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_pdf_reports_started_at_index ON public.export_reports USING btree (started_at);


--
-- Name: user_pdf_reports_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_pdf_reports_user_account_id_index ON public.export_reports USING btree (user_account_id);


--
-- Name: user_relations_initiator_user_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_relations_initiator_user_id_index ON public.user_relations USING btree (initiator_user_id);


--
-- Name: user_relations_relation_blocked_trusted_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_relations_relation_blocked_trusted_index ON public.user_relations USING btree (relation, target_user_id, initiator_user_id) WHERE ((relation)::text = ANY (ARRAY[('blocked'::character varying)::text, ('trusted'::character varying)::text]));


--
-- Name: user_relations_relation_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_relations_relation_index ON public.user_relations USING btree (relation);


--
-- Name: user_relations_target_user_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_relations_target_user_id_index ON public.user_relations USING btree (target_user_id);


--
-- Name: user_reports_target_offer_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_reports_target_offer_id_index ON public.user_reports USING btree (target_offer_id);


--
-- Name: user_reports_target_trade_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_reports_target_trade_id_index ON public.user_reports USING btree (target_trade_id);


--
-- Name: user_reports_target_user_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_reports_target_user_id_index ON public.user_reports USING btree (target_user_id);


--
-- Name: user_reports_type_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_reports_type_index ON public.user_reports USING btree (type);


--
-- Name: user_reports_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_reports_user_account_id_index ON public.user_reports USING btree (user_account_id);


--
-- Name: user_segments_segment_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_segments_segment_id_index ON public.user_segments USING btree (segment_id);


--
-- Name: user_sessions_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_sessions_user_account_id_index ON public.user_sessions USING btree (user_account_id);


--
-- Name: user_settings_document_verification_status_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_settings_document_verification_status_idx ON public.user_settings USING btree (document_verification_status);


--
-- Name: user_settings_document_verification_status_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_settings_document_verification_status_index ON public.user_settings USING btree (document_verification_status);


--
-- Name: user_settings_id_verification_status_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_settings_id_verification_status_idx ON public.user_settings USING btree (id_verification_status);


--
-- Name: user_settings_id_verification_status_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_settings_id_verification_status_index ON public.user_settings USING btree (id_verification_status);


--
-- Name: user_settings_is_document_verified_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_settings_is_document_verified_index ON public.user_settings USING btree (is_document_verified);


--
-- Name: user_settings_is_id_verified_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_settings_is_id_verified_index ON public.user_settings USING btree (is_id_verified);


--
-- Name: user_settings_user_account_id_is_verified_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_settings_user_account_id_is_verified_index ON public.user_settings USING btree (user_account_id, is_verified);


--
-- Name: user_site_messages_confirmed_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_site_messages_confirmed_at_index ON public.user_site_messages USING btree (confirmed_at);


--
-- Name: user_site_messages_created_by_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_site_messages_created_by_index ON public.user_site_messages USING btree (created_by);


--
-- Name: user_site_messages_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_site_messages_user_account_id_index ON public.user_site_messages USING btree (user_account_id);


--
-- Name: user_stats_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_stats_user_account_id_index ON public.user_stats USING btree (user_account_id);


--
-- Name: user_status_actions_archived_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_status_actions_archived_index ON public.user_status_actions USING btree (archived);


--
-- Name: user_status_actions_created_by_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_status_actions_created_by_index ON public.user_status_actions USING btree (created_by);


--
-- Name: user_status_actions_next_status_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_status_actions_next_status_index ON public.user_status_actions USING btree (next_status);


--
-- Name: user_status_actions_status_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_status_actions_status_index ON public.user_status_actions USING btree (status);


--
-- Name: user_status_actions_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_status_actions_user_account_id_index ON public.user_status_actions USING btree (user_account_id);


--
-- Name: user_transactions_jj2_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_jj2_idx ON public.user_transactions USING btree (trade_id, user_account_id);


--
-- Name: user_transactions_jj_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_jj_idx ON public.user_transactions USING btree (user_account_id, trade_id);


--
-- Name: user_transactions_linked_user_transaction_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_linked_user_transaction_id_index ON public.user_transactions USING btree (linked_user_transaction_id);


--
-- Name: user_transactions_priority_idx; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_priority_idx ON public.user_transactions USING btree (priority);


--
-- Name: user_transactions_sent_to_address_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_sent_to_address_index ON public.user_transactions USING btree (sent_to_address);


--
-- Name: user_transactions_source_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_source_index ON public.user_transactions USING btree (source);


--
-- Name: user_transactions_trade_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_trade_id_index ON public.user_transactions USING btree (trade_id);


--
-- Name: user_transactions_transaction_hash_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_transaction_hash_index ON public.user_transactions USING btree (transaction_hash);


--
-- Name: user_transactions_type; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_type ON public.user_transactions USING btree (type);


--
-- Name: user_transactions_updated_at_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_updated_at_index ON public.user_transactions USING btree (updated_at);


--
-- Name: user_transactions_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_transactions_user_account_id_index ON public.user_transactions USING btree (user_account_id);


--
-- Name: user_verifications_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX user_verifications_user_account_id_index ON public.user_verifications USING btree (user_account_id);


--
-- Name: vendors_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX vendors_user_account_id_index ON public.vendors USING btree (user_account_id);


--
-- Name: visits_cookie_token_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX visits_cookie_token_index ON public.visits USING btree (cookie_token);


--
-- Name: visits_user_account_id_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX visits_user_account_id_index ON public.visits USING btree (user_account_id);


--
-- Name: visits_utm_source_index; Type: INDEX; Schema: public; Owner: webadmin
--

CREATE INDEX visits_utm_source_index ON public.visits USING btree (utm_source);


--
-- Name: admin_logs admin_logs_moderator_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.admin_logs
    ADD CONSTRAINT admin_logs_moderator_id_foreign FOREIGN KEY (moderator_id) REFERENCES public.user_accounts(id);


--
-- Name: admin_logs admin_logs_target_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.admin_logs
    ADD CONSTRAINT admin_logs_target_user_id_foreign FOREIGN KEY (target_user_id) REFERENCES public.user_accounts(id);


--
-- Name: affiliate_balances affiliate_balances_affiliate_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_balances
    ADD CONSTRAINT affiliate_balances_affiliate_id_foreign FOREIGN KEY (affiliate_id) REFERENCES public.affiliates(id);


--
-- Name: affiliate_balances affiliate_balances_crypto_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_balances
    ADD CONSTRAINT affiliate_balances_crypto_currency_id_foreign FOREIGN KEY (crypto_currency_id) REFERENCES public.crypto_currencies(id);


--
-- Name: affiliate_transactions affiliate_transactions_affiliate_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_transactions
    ADD CONSTRAINT affiliate_transactions_affiliate_id_foreign FOREIGN KEY (affiliate_id) REFERENCES public.affiliates(id);


--
-- Name: affiliate_transactions affiliate_transactions_crypto_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_transactions
    ADD CONSTRAINT affiliate_transactions_crypto_currency_id_foreign FOREIGN KEY (crypto_currency_id) REFERENCES public.crypto_currencies(id);


--
-- Name: affiliate_transactions affiliate_transactions_fiat_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_transactions
    ADD CONSTRAINT affiliate_transactions_fiat_currency_id_foreign FOREIGN KEY (fiat_currency_id) REFERENCES public.fiat_currencies(id);


--
-- Name: affiliate_widgets affiliate_widgets_affiliate_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliate_widgets
    ADD CONSTRAINT affiliate_widgets_affiliate_id_foreign FOREIGN KEY (affiliate_id) REFERENCES public.affiliates(id);


--
-- Name: affiliates affiliates_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.affiliates
    ADD CONSTRAINT affiliates_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: auth_logs auth_logs_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.auth_logs
    ADD CONSTRAINT auth_logs_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: bank_account_bank_networks bank_account_bank_networks_bank_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_account_bank_networks
    ADD CONSTRAINT bank_account_bank_networks_bank_account_id_foreign FOREIGN KEY (bank_account_id) REFERENCES public.bank_accounts(id);


--
-- Name: bank_account_bank_networks bank_account_bank_networks_bank_network_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_account_bank_networks
    ADD CONSTRAINT bank_account_bank_networks_bank_network_id_foreign FOREIGN KEY (bank_network_id) REFERENCES public.bank_networks(id);


--
-- Name: bank_accounts bank_accounts_bank_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_accounts
    ADD CONSTRAINT bank_accounts_bank_id_foreign FOREIGN KEY (bank_id) REFERENCES public.banks(id);


--
-- Name: bank_accounts bank_accounts_fiat_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_accounts
    ADD CONSTRAINT bank_accounts_fiat_currency_id_foreign FOREIGN KEY (fiat_currency_id) REFERENCES public.fiat_currencies(id);


--
-- Name: bank_accounts bank_accounts_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.bank_accounts
    ADD CONSTRAINT bank_accounts_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: banks banks_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.banks
    ADD CONSTRAINT banks_country_id_foreign FOREIGN KEY (country_id) REFERENCES public.countries(id);


--
-- Name: chainalysis_requests chainalysis_requests_transaction_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.chainalysis_requests
    ADD CONSTRAINT chainalysis_requests_transaction_id_foreign FOREIGN KEY (transaction_id) REFERENCES public.user_transactions(id);


--
-- Name: complyadvantage_searches complyadvantage_searches_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.complyadvantage_searches
    ADD CONSTRAINT complyadvantage_searches_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: complyadvantage_tm_requests complyadvantage_tm_requests_transaction_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.complyadvantage_tm_requests
    ADD CONSTRAINT complyadvantage_tm_requests_transaction_id_foreign FOREIGN KEY (transaction_id) REFERENCES public.user_transactions(id);


--
-- Name: locations country_location_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT country_location_id_foreign FOREIGN KEY (country_location_id) REFERENCES public.locations(id);


--
-- Name: crypto_currency_forks crypto_currency_forks_approved_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks
    ADD CONSTRAINT crypto_currency_forks_approved_by_foreign FOREIGN KEY (approved_by) REFERENCES public.user_accounts(id);


--
-- Name: crypto_currency_forks crypto_currency_forks_confirmation_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks
    ADD CONSTRAINT crypto_currency_forks_confirmation_by_foreign FOREIGN KEY (confirmation_by) REFERENCES public.user_accounts(id);


--
-- Name: crypto_currency_forks crypto_currency_forks_forked_crypto_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks
    ADD CONSTRAINT crypto_currency_forks_forked_crypto_currency_id_foreign FOREIGN KEY (forked_crypto_currency_id) REFERENCES public.crypto_currencies(id);


--
-- Name: crypto_currency_forks crypto_currency_forks_original_crypto_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks
    ADD CONSTRAINT crypto_currency_forks_original_crypto_currency_id_foreign FOREIGN KEY (original_crypto_currency_id) REFERENCES public.crypto_currencies(id);


--
-- Name: crypto_currency_forks crypto_currency_forks_payout_crypto_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks
    ADD CONSTRAINT crypto_currency_forks_payout_crypto_currency_id_foreign FOREIGN KEY (payout_crypto_currency_id) REFERENCES public.crypto_currencies(id);


--
-- Name: crypto_currency_forks_user_payouts crypto_currency_forks_user_payouts_crypto_currency_fork_id_fore; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks_user_payouts
    ADD CONSTRAINT crypto_currency_forks_user_payouts_crypto_currency_fork_id_fore FOREIGN KEY (crypto_currency_fork_id) REFERENCES public.crypto_currency_forks(id);


--
-- Name: crypto_currency_forks_user_payouts crypto_currency_forks_user_payouts_last_user_transaction_id_for; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks_user_payouts
    ADD CONSTRAINT crypto_currency_forks_user_payouts_last_user_transaction_id_for FOREIGN KEY (last_user_transaction_id) REFERENCES public.user_transactions(id);


--
-- Name: crypto_currency_forks_user_payouts crypto_currency_forks_user_payouts_payout_user_transaction_id_f; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks_user_payouts
    ADD CONSTRAINT crypto_currency_forks_user_payouts_payout_user_transaction_id_f FOREIGN KEY (payout_user_transaction_id) REFERENCES public.user_transactions(id);


--
-- Name: crypto_currency_forks_user_payouts crypto_currency_forks_user_payouts_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_currency_forks_user_payouts
    ADD CONSTRAINT crypto_currency_forks_user_payouts_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trade_notes crypto_trade_notes_author_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_notes
    ADD CONSTRAINT crypto_trade_notes_author_id_foreign FOREIGN KEY (author_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trade_surveys crypto_trade_surveys_offer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_surveys
    ADD CONSTRAINT crypto_trade_surveys_offer_id_foreign FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: crypto_trade_surveys crypto_trade_surveys_question_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_surveys
    ADD CONSTRAINT crypto_trade_surveys_question_id_foreign FOREIGN KEY (question_id) REFERENCES public.survey_questions(id);


--
-- Name: crypto_trade_surveys crypto_trade_surveys_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trade_surveys
    ADD CONSTRAINT crypto_trade_surveys_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_active crypto_trades_active_affiliate_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_affiliate_id_foreign FOREIGN KEY (affiliate_id) REFERENCES public.affiliates(id);


--
-- Name: crypto_trades_active crypto_trades_active_buyer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_buyer_id_foreign FOREIGN KEY (buyer_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_active crypto_trades_active_offer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_offer_id_foreign FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: crypto_trades_active crypto_trades_active_offer_owner_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_offer_owner_id_foreign FOREIGN KEY (offer_owner_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_active crypto_trades_active_offer_responder_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_offer_responder_id_foreign FOREIGN KEY (offer_responder_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_active crypto_trades_active_owner_country_id_logged_in_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_owner_country_id_logged_in_foreign FOREIGN KEY (owner_country_id_logged_in) REFERENCES public.countries(id);


--
-- Name: crypto_trades_active crypto_trades_active_payment_method_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_payment_method_id_foreign FOREIGN KEY (payment_method_id) REFERENCES public.payment_methods(id);


--
-- Name: crypto_trades_active crypto_trades_active_responder_country_id_logged_in_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_responder_country_id_logged_in_foreign FOREIGN KEY (responder_country_id_logged_in) REFERENCES public.countries(id);


--
-- Name: crypto_trades_active crypto_trades_active_seller_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_active
    ADD CONSTRAINT crypto_trades_active_seller_id_foreign FOREIGN KEY (seller_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_affiliate_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_affiliate_id_foreign FOREIGN KEY (affiliate_id) REFERENCES public.affiliates(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_buyer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_buyer_id_foreign FOREIGN KEY (buyer_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_offer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_offer_id_foreign FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_offer_owner_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_offer_owner_id_foreign FOREIGN KEY (offer_owner_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_offer_responder_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_offer_responder_id_foreign FOREIGN KEY (offer_responder_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_owner_country_id_logged_in_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_owner_country_id_logged_in_foreign FOREIGN KEY (owner_country_id_logged_in) REFERENCES public.countries(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_payment_method_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_payment_method_id_foreign FOREIGN KEY (payment_method_id) REFERENCES public.payment_methods(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_responder_country_id_logged_in_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_responder_country_id_logged_in_foreign FOREIGN KEY (responder_country_id_logged_in) REFERENCES public.countries(id);


--
-- Name: crypto_trades_completed crypto_trades_completed_seller_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_completed
    ADD CONSTRAINT crypto_trades_completed_seller_id_foreign FOREIGN KEY (seller_id) REFERENCES public.user_accounts(id);


--
-- Name: crypto_trades_disputed crypto_trades_disputed_dispute_award_reason_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_disputed
    ADD CONSTRAINT crypto_trades_disputed_dispute_award_reason_id_foreign FOREIGN KEY (dispute_award_reason_id) REFERENCES public.dispute_award_reasons(id) ON DELETE SET NULL;


--
-- Name: dispute_logs dispute_logs_awarded_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_logs
    ADD CONSTRAINT dispute_logs_awarded_user_id_foreign FOREIGN KEY (awarded_user_id) REFERENCES public.user_accounts(id);


--
-- Name: dispute_logs dispute_logs_moderator_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_logs
    ADD CONSTRAINT dispute_logs_moderator_id_foreign FOREIGN KEY (moderator_id) REFERENCES public.user_accounts(id);


--
-- Name: dispute_logs dispute_logs_offer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.dispute_logs
    ADD CONSTRAINT dispute_logs_offer_id_foreign FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: feedback feedback_feedback_leaver_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_feedback_leaver_id_foreign FOREIGN KEY (feedback_leaver_id) REFERENCES public.user_accounts(id);


--
-- Name: feedback feedback_feedback_receiver_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_feedback_receiver_id_foreign FOREIGN KEY (feedback_receiver_id) REFERENCES public.user_accounts(id);


--
-- Name: feedback feedback_offer_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_offer_id_fk FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: crypto_trades_disputed fk_dispute_reason_id; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.crypto_trades_disputed
    ADD CONSTRAINT fk_dispute_reason_id FOREIGN KEY (dispute_reason_id) REFERENCES public.dispute_reasons(id);


--
-- Name: offer_tags fk_offer_tags_offer_id; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_tags
    ADD CONSTRAINT fk_offer_tags_offer_id FOREIGN KEY (offer_id) REFERENCES public.offers(id) ON DELETE CASCADE;


--
-- Name: offer_tags fk_offer_tags_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_tags
    ADD CONSTRAINT fk_offer_tags_tag_id FOREIGN KEY (tag_id) REFERENCES public.tags(id) ON DELETE CASCADE;


--
-- Name: user_reports_attachments fk_report_id; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_reports_attachments
    ADD CONSTRAINT fk_report_id FOREIGN KEY (report_id) REFERENCES public.user_reports(id);


--
-- Name: user_ip_history fk_user_account_id; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_ip_history
    ADD CONSTRAINT fk_user_account_id FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: email_actions fk_user_account_id; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.email_actions
    ADD CONSTRAINT fk_user_account_id FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: jumio_verifications jumio_verifications_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.jumio_verifications
    ADD CONSTRAINT jumio_verifications_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: jumio_verifications jumio_verifications_verification_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.jumio_verifications
    ADD CONSTRAINT jumio_verifications_verification_id_foreign FOREIGN KEY (verification_id) REFERENCES public.kyc_verifications(id);


--
-- Name: kyc_verifications kyc_verifications_admin_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.kyc_verifications
    ADD CONSTRAINT kyc_verifications_admin_id_foreign FOREIGN KEY (admin_id) REFERENCES public.user_accounts(id);


--
-- Name: kyc_verifications kyc_verifications_city_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.kyc_verifications
    ADD CONSTRAINT kyc_verifications_city_id_foreign FOREIGN KEY (city_id) REFERENCES public.cities(id);


--
-- Name: kyc_verifications kyc_verifications_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.kyc_verifications
    ADD CONSTRAINT kyc_verifications_country_id_foreign FOREIGN KEY (country_id) REFERENCES public.countries(id);


--
-- Name: kyc_verifications kyc_verifications_issue_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.kyc_verifications
    ADD CONSTRAINT kyc_verifications_issue_country_id_foreign FOREIGN KEY (issue_country_id) REFERENCES public.countries(id);


--
-- Name: kyc_verifications kyc_verifications_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.kyc_verifications
    ADD CONSTRAINT kyc_verifications_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: merchant_deposit_address merchant_deposit_address_merchant_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.merchant_deposit_address
    ADD CONSTRAINT merchant_deposit_address_merchant_id_foreign FOREIGN KEY (merchant_id) REFERENCES public.merchants(id);


--
-- Name: merchant_deposit_address merchant_deposit_address_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.merchant_deposit_address
    ADD CONSTRAINT merchant_deposit_address_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: moderator_checklist_logs moderator_checklist_log_crypto_trades_disputed_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.moderator_checklist_logs
    ADD CONSTRAINT moderator_checklist_log_crypto_trades_disputed_id_foreign FOREIGN KEY (crypto_trades_disputed_id) REFERENCES public.crypto_trades_disputed(id);


--
-- Name: moderator_checklist_logs moderator_checklist_log_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.moderator_checklist_logs
    ADD CONSTRAINT moderator_checklist_log_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: moderator_checklist_logs moderator_checklist_logs_moderator_checklist_item_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.moderator_checklist_logs
    ADD CONSTRAINT moderator_checklist_logs_moderator_checklist_item_id_foreign FOREIGN KEY (moderator_checklist_item_id) REFERENCES public.moderator_checklist_items(id);


--
-- Name: offer_payment_details offer_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_payment_details
    ADD CONSTRAINT offer_id_fk FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: offer_performance offer_performance_offer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_performance
    ADD CONSTRAINT offer_performance_offer_id_foreign FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: offers offers_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_country_id_foreign FOREIGN KEY (country_id) REFERENCES public.countries(id);


--
-- Name: offer_payment_requirements offers_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_payment_requirements
    ADD CONSTRAINT offers_id_fk FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: offers offers_payment_method_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_payment_method_country_id_foreign FOREIGN KEY (payment_method_country_id) REFERENCES public.countries(id);


--
-- Name: offers offers_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: offer_payment_details payment_method_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.offer_payment_details
    ADD CONSTRAINT payment_method_id_fk FOREIGN KEY (payment_method_id) REFERENCES public.payment_methods(id);


--
-- Name: payment_methods payment_methods_payment_method_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.payment_methods
    ADD CONSTRAINT payment_methods_payment_method_group_id_foreign FOREIGN KEY (payment_method_group_id) REFERENCES public.payment_method_groups(id);


--
-- Name: popular_payment_methods popular_payment_methods_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.popular_payment_methods
    ADD CONSTRAINT popular_payment_methods_country_id_foreign FOREIGN KEY (country_id) REFERENCES public.countries(id);


--
-- Name: popular_payment_methods popular_payment_methods_payment_method_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.popular_payment_methods
    ADD CONSTRAINT popular_payment_methods_payment_method_group_id_foreign FOREIGN KEY (payment_method_group_id) REFERENCES public.payment_method_groups(id);


--
-- Name: popular_payment_methods popular_payment_methods_payment_method_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.popular_payment_methods
    ADD CONSTRAINT popular_payment_methods_payment_method_id_foreign FOREIGN KEY (payment_method_id) REFERENCES public.payment_methods(id);


--
-- Name: referral_balances referral_balances_crypto_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_balances
    ADD CONSTRAINT referral_balances_crypto_currency_id_foreign FOREIGN KEY (crypto_currency_id) REFERENCES public.crypto_currencies(id);


--
-- Name: referral_balances referral_balances_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_balances
    ADD CONSTRAINT referral_balances_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: referral_transactions referral_transactions_crypto_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_transactions
    ADD CONSTRAINT referral_transactions_crypto_currency_id_foreign FOREIGN KEY (crypto_currency_id) REFERENCES public.crypto_currencies(id);


--
-- Name: referral_transactions referral_transactions_fiat_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_transactions
    ADD CONSTRAINT referral_transactions_fiat_currency_id_foreign FOREIGN KEY (fiat_currency_id) REFERENCES public.fiat_currencies(id);


--
-- Name: referral_transactions referral_transactions_referral_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_transactions
    ADD CONSTRAINT referral_transactions_referral_user_account_id_foreign FOREIGN KEY (referral_user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: referral_transactions referral_transactions_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referral_transactions
    ADD CONSTRAINT referral_transactions_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: referrals referrals_referral_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_referral_user_account_id_foreign FOREIGN KEY (referral_user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: referrals referrals_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: salesman_lead_connections salesman_lead_connections_lead_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.salesman_lead_connections
    ADD CONSTRAINT salesman_lead_connections_lead_id_foreign FOREIGN KEY (lead_id) REFERENCES public.user_accounts(id);


--
-- Name: salesman_lead_connections salesman_lead_connections_salesman_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.salesman_lead_connections
    ADD CONSTRAINT salesman_lead_connections_salesman_id_foreign FOREIGN KEY (salesman_id) REFERENCES public.user_accounts(id);


--
-- Name: locations subdivision_location_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT subdivision_location_id_foreign FOREIGN KEY (subdivision_location_id) REFERENCES public.locations(id);


--
-- Name: trade_fees trade_fees_crypto_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.trade_fees
    ADD CONSTRAINT trade_fees_crypto_currency_id_foreign FOREIGN KEY (crypto_currency_id) REFERENCES public.crypto_currencies(id);


--
-- Name: transactions_prepared transactions_prepared_merchant_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.transactions_prepared
    ADD CONSTRAINT transactions_prepared_merchant_id_foreign FOREIGN KEY (merchant_id) REFERENCES public.merchants(id);


--
-- Name: transactions_prepared transactions_prepared_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.transactions_prepared
    ADD CONSTRAINT transactions_prepared_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: two_fa_reset_tokens two_fa_reset_tokens_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.two_fa_reset_tokens
    ADD CONSTRAINT two_fa_reset_tokens_created_by_foreign FOREIGN KEY (created_by) REFERENCES public.user_accounts(id);


--
-- Name: two_fa_reset_tokens two_fa_reset_tokens_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.two_fa_reset_tokens
    ADD CONSTRAINT two_fa_reset_tokens_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_account_delete_requests user_account_delete_requests_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_account_delete_requests
    ADD CONSTRAINT user_account_delete_requests_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_accounts_deleted user_accounts_deleted_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_accounts_deleted
    ADD CONSTRAINT user_accounts_deleted_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_accounts user_accounts_document_verification_required_admin_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_accounts
    ADD CONSTRAINT user_accounts_document_verification_required_admin_id_foreign FOREIGN KEY (document_verification_required_admin_id) REFERENCES public.user_accounts(id);


--
-- Name: user_accounts user_accounts_id_verification_required_admin_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_accounts
    ADD CONSTRAINT user_accounts_id_verification_required_admin_id_foreign FOREIGN KEY (id_verification_required_admin_id) REFERENCES public.user_accounts(id);


--
-- Name: user_addresses user_addresses_city_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_addresses
    ADD CONSTRAINT user_addresses_city_id_foreign FOREIGN KEY (city_id) REFERENCES public.cities(id);


--
-- Name: user_addresses user_addresses_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_addresses
    ADD CONSTRAINT user_addresses_country_id_foreign FOREIGN KEY (country_id) REFERENCES public.countries(id);


--
-- Name: user_addresses user_addresses_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_addresses
    ADD CONSTRAINT user_addresses_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_api_permissions user_api_permissions_user_api_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_api_permissions
    ADD CONSTRAINT user_api_permissions_user_api_id_foreign FOREIGN KEY (user_api_id) REFERENCES public.user_apis(id) ON DELETE CASCADE;


--
-- Name: user_apis user_apis_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_apis
    ADD CONSTRAINT user_apis_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_badges user_badges_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_badges
    ADD CONSTRAINT user_badges_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_compliance user_compliance_document_verification_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_compliance
    ADD CONSTRAINT user_compliance_document_verification_id_foreign FOREIGN KEY (document_verification_id) REFERENCES public.kyc_verifications(id);


--
-- Name: user_compliance user_compliance_id_verification_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_compliance
    ADD CONSTRAINT user_compliance_id_verification_id_foreign FOREIGN KEY (id_verification_id) REFERENCES public.kyc_verifications(id);


--
-- Name: user_compliance user_compliance_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_compliance
    ADD CONSTRAINT user_compliance_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_crypto_addresses user_crypto_addresses_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_crypto_addresses
    ADD CONSTRAINT user_crypto_addresses_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_crypto_balances user_crypto_balances_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_crypto_balances
    ADD CONSTRAINT user_crypto_balances_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_emails_blacklist user_emails_blacklist_admin_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_emails_blacklist
    ADD CONSTRAINT user_emails_blacklist_admin_account_id_foreign FOREIGN KEY (admin_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_feature_flags user_feature_flags_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_feature_flags
    ADD CONSTRAINT user_feature_flags_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_freeway_histories user_freeway_histories_affiliate_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_affiliate_id_foreign FOREIGN KEY (affiliate_id) REFERENCES public.affiliates(id) ON DELETE CASCADE;


--
-- Name: user_freeway_histories user_freeway_histories_affiliate_widget_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_affiliate_widget_id_foreign FOREIGN KEY (affiliate_widget_id) REFERENCES public.affiliate_widgets(id);


--
-- Name: user_freeway_histories user_freeway_histories_fiat_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_fiat_currency_id_foreign FOREIGN KEY (fiat_currency_id) REFERENCES public.fiat_currencies(id);


--
-- Name: user_freeway_histories user_freeway_histories_offer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_offer_id_foreign FOREIGN KEY (offer_id) REFERENCES public.offers(id);


--
-- Name: user_freeway_histories user_freeway_histories_offer_owner_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_offer_owner_id_foreign FOREIGN KEY (offer_owner_id) REFERENCES public.user_accounts(id);


--
-- Name: user_freeway_histories user_freeway_histories_payment_method_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_payment_method_group_id_foreign FOREIGN KEY (payment_method_group_id) REFERENCES public.payment_method_groups(id);


--
-- Name: user_freeway_histories user_freeway_histories_payment_method_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_payment_method_id_foreign FOREIGN KEY (payment_method_id) REFERENCES public.payment_methods(id);


--
-- Name: user_freeway_histories user_freeway_histories_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_freeway_histories
    ADD CONSTRAINT user_freeway_histories_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id) ON DELETE CASCADE;


--
-- Name: user_ip_history user_ip_history_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_ip_history
    ADD CONSTRAINT user_ip_history_country_id_foreign FOREIGN KEY (country_id) REFERENCES public.countries(id);


--
-- Name: user_issues user_issues_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_issues
    ADD CONSTRAINT user_issues_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_languages user_languages_language_code_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_languages
    ADD CONSTRAINT user_languages_language_code_foreign FOREIGN KEY (language_code) REFERENCES public.languages(code);


--
-- Name: user_languages user_languages_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_languages
    ADD CONSTRAINT user_languages_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_notifications_settings user_notifications_settings_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_notifications_settings
    ADD CONSTRAINT user_notifications_settings_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: export_reports user_pdf_reports_admin_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.export_reports
    ADD CONSTRAINT user_pdf_reports_admin_account_id_foreign FOREIGN KEY (admin_account_id) REFERENCES public.user_accounts(id);


--
-- Name: export_reports user_pdf_reports_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.export_reports
    ADD CONSTRAINT user_pdf_reports_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_relations user_relations_initiator_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_relations
    ADD CONSTRAINT user_relations_initiator_user_id_foreign FOREIGN KEY (initiator_user_id) REFERENCES public.user_accounts(id) ON DELETE CASCADE;


--
-- Name: user_relations user_relations_target_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_relations
    ADD CONSTRAINT user_relations_target_user_id_foreign FOREIGN KEY (target_user_id) REFERENCES public.user_accounts(id) ON DELETE CASCADE;


--
-- Name: user_reports user_reports_target_offer_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_reports
    ADD CONSTRAINT user_reports_target_offer_id_foreign FOREIGN KEY (target_offer_id) REFERENCES public.offers(id);


--
-- Name: user_security_questions user_security_questions_security_question_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_security_questions
    ADD CONSTRAINT user_security_questions_security_question_id_foreign FOREIGN KEY (security_question_id) REFERENCES public.security_questions(id);


--
-- Name: user_security_questions user_security_questions_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_security_questions
    ADD CONSTRAINT user_security_questions_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_sessions user_sessions_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_sessions
    ADD CONSTRAINT user_sessions_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_settings user_settings_document_verification_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_settings
    ADD CONSTRAINT user_settings_document_verification_id_foreign FOREIGN KEY (document_verification_id) REFERENCES public.kyc_verifications(id);


--
-- Name: user_settings user_settings_fiat_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_settings
    ADD CONSTRAINT user_settings_fiat_currency_id_foreign FOREIGN KEY (fiat_currency_id) REFERENCES public.fiat_currencies(id);


--
-- Name: user_settings user_settings_id_verification_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_settings
    ADD CONSTRAINT user_settings_id_verification_id_foreign FOREIGN KEY (id_verification_id) REFERENCES public.kyc_verifications(id);


--
-- Name: user_settings user_settings_phone_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_settings
    ADD CONSTRAINT user_settings_phone_country_id_foreign FOREIGN KEY (phone_country_id) REFERENCES public.countries(id);


--
-- Name: user_settings user_settings_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_settings
    ADD CONSTRAINT user_settings_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_site_messages user_site_messages_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_site_messages
    ADD CONSTRAINT user_site_messages_created_by_foreign FOREIGN KEY (created_by) REFERENCES public.user_accounts(id);


--
-- Name: user_site_messages user_site_messages_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_site_messages
    ADD CONSTRAINT user_site_messages_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_stats user_stats_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_stats
    ADD CONSTRAINT user_stats_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_status_actions user_status_actions_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_status_actions
    ADD CONSTRAINT user_status_actions_created_by_foreign FOREIGN KEY (created_by) REFERENCES public.user_accounts(id);


--
-- Name: user_status_actions user_status_actions_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_status_actions
    ADD CONSTRAINT user_status_actions_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_transactions user_transactions_merchant_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_transactions
    ADD CONSTRAINT user_transactions_merchant_id_foreign FOREIGN KEY (merchant_id) REFERENCES public.merchants(id);


--
-- Name: user_transactions user_transactions_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_transactions
    ADD CONSTRAINT user_transactions_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: user_verifications user_verifications_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.user_verifications
    ADD CONSTRAINT user_verifications_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: vendors vendors_user_account_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: webadmin
--

ALTER TABLE ONLY public.vendors
    ADD CONSTRAINT vendors_user_account_id_foreign FOREIGN KEY (user_account_id) REFERENCES public.user_accounts(id);


--
-- Name: SCHEMA okmeter; Type: ACL; Schema: -; Owner: webadmin
--

